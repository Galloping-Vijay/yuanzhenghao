<!DOCTYPE html>
<!-- saved from url=(0051)http://www.yzhpps.com/?page_id=11626 -->
<html class="bit-html mobile-false js_active  vc_desktop  vc_transform  vc_transform  no-touch" dir="ltr" lang="zh-CN">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- for 360 -->
    <meta name="renderer" content="webkit">
    <meta name="applicable-device" content="pc,mobile"> <!-- for baidu -->
    <meta http-equiv="Cache-Control" content="no-transform"> <!-- for baidu -->
    <meta name="MobileOptimized" content="width"><!-- for baidu -->
    <meta name="HandheldFriendly" content="true"><!-- for baidu -->
    <!-- start of customer header -->
    <!-- end of customer header -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{ $configs['seo_title'] }} | {{ $title??$configs['seo_title'] }}</title>
    <meta name="keywords" content="{{ $configs['site_seo_keywords'] }}">
    <meta name="description" content="{{ $configs['site_seo_description'] }}">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="http://www.yzhpps.com/xmlrpc.php">
    <!--[if lt IE 9]>
    <script src="http://www.yzhpps.com/FeiEditor/bitSite/js/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://www.yzhpps.com/FeiEditor/bitSite/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="static-stylesheet"></style>
    <link rel="stylesheet" href="{{ asset('css/yzh/yzh.css') }}" type="text/css" media="all">
    <!-- Cache! -->
    <style type="text/css">
        #header .wf-wrap {

            max-width: 1520px;
            margin: 0 auto;
        }

    </style>
    @yield('header')
    <script type="text/javascript" async="" defer="" src="{{ asset('js/yzh/matomo.js') }}"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        /* dt-plugins */
        var dtLocal = {
            "passText": "\u67e5\u770b\u8fd9\u4e2a\u52a0\u5bc6\u8d44\u8baf\uff0c\u8bf7\u5728\u4e0b\u9762\u8f93\u5165\u5bc6\u7801\uff1a",
            "moreButtonAllLoadedText": "\u5168\u90e8\u5df2\u52a0\u8f7d",
            "postID": "11626",
            "ajaxurl": "http:\/\/5d1c71c9850fc.t73.qifeiye.com\/admin\/admin-ajax.php",
            "contactNonce": "5f9425247e",
            "ajaxNonce": "8c35441399",
            "pageData": {"type": "page", "template": "page", "layout": null},
            "themeSettings": {"smoothScroll": "on"}
        };
        /* thickbox */
        var thickboxL10n = {
            "next": "\u4e0b\u4e00\u5f20 >",
            "prev": "< \u4e0a\u4e00\u5f20",
            "image": "\u56fe\u7247",
            "of": "\/",
            "close": "\u5173\u95ed",
            "noiframes": "This feature requires inline frames. You have iframes disabled or your browser does not support them.",
            "loadingAnimation": "\/\/fast.qifeiye.com\/FeiEditor\/bitSite\/images\/preloader.gif"
        };

        /* ]]> */
    </script>
    <script type="text/javascript" src="{{ asset('js/yzh/yzh.js') }}">/*Cache!*/</script>
    <script>var geURL = 'http://www.yzhpps.com/ge/index.html?embed=1&ui=embed&spin=1&modified=unsavedChanges&proto=json&lang=zh';</script>
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="http://www.yzhpps.com/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="http://www.yzhpps.com/qfy-includes/wlwmanifest.xml">

    <link rel="canonical" href="http://www.yzhpps.com/?page_id=11626">
    <link rel="shortlink" href="http://www.yzhpps.com/?p=11626">
    <style class="style_0">.bitRightSider .widget-title, .bitLeftSider .widget-title {
            padding: 0 0 0 10px;
            height: 28px;
            line-height: 28px;
            background-color: #024886;
            margin: 0px;
            font-family:;
            font-size: px;
            font-weight: normal;
            font-style: normal;
            text-decoration: none;
            color: #fff;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            background-image: url(//fast.qifeiye.com/qfy-content/plugins//bit-plugin/assets/frame/header_bg/1/bg.png);
            background-repeat: repeat;
            -webkit-border-top-left-radius: 0;
            -webkit-border-top-right-radius: 0;
            -moz-border-radius-topleft: 0;
            -moz-border-radius-topright: 0;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .bitRightSider .bitWidgetFrame, .bitLeftSider .bitWidgetFrame {
            border-top: 0;
            border-bottom: 1px solid #bababa;
            border-left: 1px solid #bababa;
            border-right: 1px solid #bababa;
            padding: 4px 10px 4px 10px;
            -webkit-border-bottom-left-radius: 0;
            -webkit-border-bottom-right-radius: 0;
            -moz-border-radius-bottomleft: 0;
            -moz-border-radius-bottomright: 0;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .bitRightSider .site_tooler, .bitLeftSider .site_tooler {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .bitRightLeftSiderWidget.site_tooler .bitWidgetFrame, .bitRightLeftSiderWidget.site_tooler .bitWidgetFrame {
            background-color: transparent;
            background-image: none;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }</style>
    <style class="style_0">.footer .widget-title {
            padding: 0 0 0 10px;
            height: 28px;
            line-height: 28px;
            background-color: #024886;
            margin: 0px;
            font-family:;
            font-size: px;
            font-weight: normal;
            font-style: normal;
            text-decoration: none;
            color: #fff;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            background-image: none;
            -webkit-border-top-left-radius: 4px;
            -webkit-border-top-right-radius: 4px;
            -moz-border-radius-topleft: 4px;
            -moz-border-radius-topright: 4px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
        }

        .footer .widget-title {
            border-top: 0;
            border-left: 0;
            border-right: 0;
        }

        .footer .bitWidgetFrame {
            border-bottom: 0;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            padding: 4px 10px 4px 10px;
        }

        .footer .site_tooler {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .footer .site_tooler .bitWidgetFrame {
            background-color: transparent;
            background-image: none;
            -webkit-border-bottom-left-radius: 4px;
            border-bottom-left-radius: 4px;
            -webkit-border-bottom-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }</style>
    <script type="text/javascript">
        var qfyuser_ajax_url = 'http://www.yzhpps.com/admin/admin-ajax.php';
    </script>
    <script type="text/javascript">
        var qfyuser_upload_url = 'http://www.yzhpps.com/qfy-content/plugins/qfyuser/lib/fileupload/fileupload.php?upload_nonce=a0522fba95';
    </script>
    <meta name="robots" content="index, follow">
    <style type="text/css">
    </style>
    <script type="text/javascript">
        dtGlobals.logoEnabled = 1;
        dtGlobals.curr_id = '11626';
        dtGlobals.logoURL = "{{ asset('images/yzh/logo.png') }}";
        dtGlobals.logoW = '299';
        dtGlobals.logoH = '88';
        dtGlobals.qfyname = '起飞页';
        dtGlobals.id = '5d1c71c9850fc';
        dtGlobals.language = '';
        smartMenu = 1;
        document.cookie = 'resolution=' + Math.max(screen.width, screen.height) + '; path=/';
        dtGlobals.gallery_bgcolor = 'rgba(51,51,51,1)';
        dtGlobals.gallery_showthumbs = '0';
        dtGlobals.gallery_style = '';
        dtGlobals.gallery_autoplay = '0';
        dtGlobals.gallery_playspeed = '3';
        dtGlobals.gallery_imagesize = '100';
        dtGlobals.gallery_imageheight = '100';
        dtGlobals.gallery_stopbutton = '';
        dtGlobals.gallery_thumbsposition = '';
        dtGlobals.gallery_tcolor = '#fff';
        dtGlobals.gallery_tsize = '16';
        dtGlobals.gallery_dcolor = '#fff';
        dtGlobals.gallery_dsize = '14';
        dtGlobals.gallery_tfamily = '';
        dtGlobals.gallery_dfamily = '';
        dtGlobals.gallery_blankclose = '0';
        dtGlobals.gallery_arrowstyle = '0';
        dtGlobals.fm_showstyle = '';
        dtGlobals.fm_showspeed = '';
        dtGlobals.cdn_url = 'https://ccdn.goodq.top';
        dtGlobals.qfymodel = "";
        var socail_back_url = '';

    </script>

    {{--<script src="/images/yzh/configs.php" async="" defer=""></script>--}}
</head>


<body class="page page-id-11626 page-template-default image-blur widefull_header3 widefull_topbar3 btn-flat content-fullwidth qfe-js-composer js-comp-ver-4.0.1 vc_responsive is-webkit mobilefloatmenu"
      data-pid="11626" data-pkey="533fe56d5619f185bcf503af0fa2cfab" style="">
<div class="dl-menuwrapper  wf-mobile-visible floatmenu floatwarpper fullfloatmenu center">
    <div class="dl-container">
        <ul data-st="1" data-sp="0" data-fh="0" data-mw="1600" data-lh="32" class="dl-menu"
            data-bit-menu="underline-hover" data-bit-float-menu="underline-hover">
            @component('./layouts/yzh/nav',['isMobile'=>'1'])
            @endcomponent
        </ul>
    </div>
</div>
<div id="mark_mask"
     style="display:none;position:fixed;top:40px;left:0;z-index:99999999;height:1000px;width:100%;background:rgba(0,0,0,0.4);"></div>
<script>
    var Random = function (min, max) {
        return Math.round(Math.random() * (max - min)) + min;
    }
    //apm normal access
    if (Random(1, 20) == 1) {
        !(function (c, b, d, a) {
            c[a] || (c[a] = {});
            c[a].config = {
                pid: "c9ihesw3jb@ccca834f41b7459",
                appType: "web",
                imgUrl: "https://arms-retcode.aliyuncs.com/r.png?"
            };
            with (b) with (body) with (insertBefore(createElement("script"), firstChild)) setAttribute("crossorigin", "", src = d)
        })(window, document, "https://retcode.alicdn.com/retcode/bl.js", "__bl");
    }
</script>


<div id="page" class=" wide ">
    <div id="dl-menu" class="dl-menuwrapper wf-mobile-visible floatmenu fullfloatmenu center positionFixed"
         style="right: 8px; top: 8px;"><a data-padding="" data-top="8" data-right="8" rel="nofollow" id="mobile-menu"
                                          class="glyphicon glyphicon-icon-align-justify fullfloatmenu positionFixed iconbigSize center">
            <span class="menu-open  phone-text">产品中心</span>
            <span class="menu-close">关闭</span>
            <span class="menu-back">返回上一级</span>
            <span class="wf-phone-visible">&nbsp;</span>
        </a></div>


    <!-- left, center, classical, classic-centered -->
    <!-- !Header -->
    <header id="header" class="logo-left-right  mobiletopbottom  ht headerPM menuPosition" role="banner">
        <!-- class="overlap"; class="logo-left", class="logo-center", class="logo-classic" -->
        <div class="wf-wrap">
            <div class="wf-table">


                <div id="branding" class="wf-td bit-logo-bar" style="width:200px;">
                    <a class="bitem logo  fixedwidth small" style="display: table-cell;"
                       href="http://www.yzhpps.com/"><span class="logospan"><img class="preload-me"
                                                                                                src="{{ asset('images/yzh/logo.png') }}"
                                                                                                width="299" height="88"
                                                                                                alt="{{ $configs['site_name'] }}"></span></a>

                    <!-- <div id="site-title" class="assistive-text"></div>
                    <div id="site-description" class="assistive-text"></div> -->
                </div>

                <!-- !- Navigation -->
                <nav style="min-height: 38px;" id="navigation" class="wf-td" bitdataaction="site_menu_container"
                     bitdatalocation="primary">


                    <div id="dl-menu"
                         class="dl-menuwrapper wf-mobile-visible main-mobile-menu mobiledefault_containter center"
                         style="display: none; right: 8px; top: 8px;"></div>


                </nav>
                <div style="display:none;" id="main-nav-slide">

                </div>

                <div class="wf-td assistive-info    " role="complementary" style="">
                    <div class="top-bar-right right bit_widget_more" bitdatamarker="bitHeader-2"
                         bitdataaction="site_fix_container" bitdatacolor="white" style="">
                        <div id="simplepage-3" style="margin-top:0;margin-bottom:0;"
                             class="mobileHidden headerWidget_1 simplepage site_tooler">
                            <style class="style_simplepage-3">#simplepage-3 .widget-title {
                                    padding: 0 0 0 10px;
                                    height: 28px;
                                    line-height: 28px;
                                    background-color: transparent;
                                    margin: 0px;
                                    font-family:;
                                    font-size: px;
                                    font-weight: normal;
                                    font-style: normal;
                                    text-decoration: none;
                                    color: #ffffff;
                                    border-top: 1px solid transparent;
                                    border-left: 1px solid transparent;
                                    border-right: 1px solid transparent;
                                    border-bottom: 0px solid transparent;
                                    background-image: none;
                                    -webkit-border-top-left-radius: 4px;
                                    -webkit-border-top-right-radius: 4px;
                                    -moz-border-radius-topleft: 4px;
                                    -moz-border-radius-topright: 4px;
                                    border-top-left-radius: 4px;
                                    border-top-right-radius: 4px;
                                }

                                #simplepage-3 .widget-title {
                                    border-top: 0;
                                    border-left: 0;
                                    border-right: 0;
                                }

                                #simplepage-3 .bitWidgetFrame {
                                    border-bottom: 0;
                                    border-top: 0;
                                    border-left: 0;
                                    border-right: 0;
                                    padding: 4px 10px 4px 10px;
                                }

                                #simplepage-3 {
                                    -webkit-box-shadow: none;
                                    box-shadow: none;
                                }

                                #simplepage-3 .bitWidgetFrame {
                                    background-color: transparent;
                                    background-image: none;
                                    -webkit-border-bottom-left-radius: 4px;
                                    border-bottom-left-radius: 4px;
                                    -webkit-border-bottom-right-radius: 4px;
                                    border-bottom-right-radius: 4px;
                                }

                                #simplepage-3 .bitWidgetFrame {
                                    padding-left: 0px;
                                    padding-right: 0px;
                                }

                                body #simplepage-3 .bitWidgetFrame {
                                    padding-top: 0px !important;
                                    padding-bottom: 0px !important;
                                }</style>
                            <div class="simplepage_container bitWidgetFrame" data-post_id="15366">
                                <section data-fixheight=""
                                         class="qfy-row-2-5d735318603e9839092 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                         id="bit_38bwq" style="margin-bottom:0;border-radius:0px;color:#382924;">
                                    <style class="row_class qfy_style_class">
                                        @media only screen and (min-width: 992px) {
                                            section.section.qfy-row-2-5d735318603e9839092 {
                                                padding-left: 0;
                                                padding-right: 0;
                                                padding-top: 0;
                                                padding-bottom: 0;
                                                margin-top: 0;
                                            }
                                        }

                                        @media only screen and (max-width: 992px) {
                                            .bit-html section.section.qfy-row-2-5d735318603e9839092 {
                                                padding-left: 15px;
                                                padding-right: 15px;
                                                padding-top: 0;
                                                padding-bottom: 0;
                                                margin-top: 0;
                                                min-height: 0;
                                            }
                                        }
                                    </style>
                                    <div class="section-background-overlay background-overlay grid-overlay-0 "
                                         style="background-color: #ffffff;"></div>

                                    <div class="container">
                                        <div class="row qfe_row">
                                            <div data-animaleinbegin="bottom-in-view" data-animalename="qfyfadeInUp"
                                                 data-duration="" data-delay=""
                                                 class=" qfy-column-2-5d735318606f4202505 qfy-column-inner  vc_span12  text-default small-screen-default fullrow"
                                                 data-dw="1/1" data-fixheight="">
                                                <div style=";position:relative;" class="column_inner ">
                                                    <div class=" background-overlay grid-overlay-"
                                                         style="background-color:transparent;width:100%;"></div>
                                                    <div class="column_containter" style="z-index:3;position:relative;">
                                                        <div id="vc_header_5d73531860e02898" m-padding="0px 0px 0px 0px"
                                                             p-padding="0 0 0 0" css_animation_delay="0"
                                                             qfyuuid="qfy_header_hy33n-c-y3" data-anitime="0.7"
                                                             data-ani_iteration_count="1"
                                                             class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                             style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                            <style>#vc_header_5d73531860e02898 .qfy_title_inner, #vc_header_5d73531860e02898 .qfy_title_image_inner {
                                                                    vertical-align: middle !important;
                                                                }

                                                                @media only screen and (max-width: 768px) {
                                                                    #vc_header_5d73531860e02898 .header_title {
                                                                        font-size: 16px !important;
                                                                    }
                                                                }

                                                                @media only screen and (max-width: 768px) {
                                                                    #vc_header_5d73531860e02898 .header_subtitle {
                                                                        font-size: 12px !important;
                                                                    }
                                                                }</style>
                                                            <div class="qfe_wrapper">
                                                                <div class="qfy_title left mobileleft">
                                                                    <div class="qfy_title_image_warpper"
                                                                         style="display:inline-block;position:relative;max-width:100%;">
                                                                        <div class="qfy_title_image_inner"
                                                                             style="display:inline-block;position:relative;max-width:100%;">
                                                                            <div class="header_image "
                                                                                 style="padding:5px 0 0 0;width:40px;">
                                                                                <img class=" ag_image"
                                                                                     src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi8yNDdhMTdlNmRkOGY1ZDE5YjU2YWFmNTQzYjBjMGJhOC00MHg0MC5qcGc_p_p100_p_3D.jpg"
                                                                                     alt="top" description=""
                                                                                     data-attach-id="15369"
                                                                                     data-title="top" title=""
                                                                                     src-img=""></div>
                                                                        </div>
                                                                        <div class="qfy_title_inner"
                                                                             style="display:inline-block;position:relative;max-width:calc(100% - 40px);">
                                                                            <div class="header_title"
                                                                                 style="font-family:微软雅黑;font-size:16px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                +86 {{ $configs['site_phone'] }}
                                                                            </div>
                                                                            <div class="header_subtitle"
                                                                                 style="font-size:12px;font-weight:normal;font-style:normal;color:#c5a862;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                {{ $configs['workday'] }}：{{ $configs['worktime'] }}
                                                                            </div>
                                                                        </div>
                                                                        <style></style>
                                                                        <div class="button_wrapper"
                                                                             style="display:none;">
                                                                            <div class="button "
                                                                                 style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                + 查看更多
                                                                            </div>
                                                                        </div>
                                                                        <span style="clear:both;"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                    .qfy-column-2-5d735318606f4202505 > .column_inner {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 0;
                                                        padding-bottom: 0;
                                                    }

                                                    .qfe_row .vc_span_class.qfy-column-2-5d735318606f4202505 {
                                                    }

                                                ;
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .qfy-column-2-5d735318606f4202505 > .column_inner {
                                                        margin: 0 auto 0 !important;
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top:;
                                                        padding-bottom:;
                                                    }

                                                    .display_entire .qfe_row .vc_span_class.qfy-column-2-5d735318606f4202505 {
                                                    }

                                                    .qfy-column-2-5d735318606f4202505 > .column_inner > .background-overlay, .qfy-column-2-5d735318606f4202505 > .column_inner > .background-media {
                                                        width: 100% !important;
                                                        left: 0 !important;
                                                        right: auto !important;
                                                    }
                                                }</style>
                                        </div>
                                    </div>

                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- #branding -->
        </div><!-- .wf-wrap -->
    </header><!-- #masthead -->

    <section class="bitBanner" id="bitBanner" bitdatamarker="bitBanner" bitdataaction="site_fix_container">
    </section>
    <div id="main" class="bit_main_content">
        <div class="main-gradient"></div>
        <div class="wf-wrap">
            <div class="wf-container-main">
                <div id="content" class="content" role="main">
                    <div class="main-outer-wrapper ">
                        <div class="bit_row">
                            <div class="twelve columns no-sidebar-content ">
                                <div class="bit_row">
                                    <div class="content-wrapper twelve columns ">
                                        @yield('content')
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div><!-- END .page-wrapper -->
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div><!-- .wf-container -->
        </div><!-- .wf-wrap -->
    </div>

<!-- #main -->
    @component('./layouts/yzh/footer')
    @endcomponent
    <a href="http://www.yzhpps.com/?page_id=11626#" class="scroll-top displaynone on"></a>

</div><!-- #page -->

<style type="text/css">
    #header .wf-wrap {

        max-width: 1520px;
        margin: 0 auto;
    }

</style>
<div style="display:none;"></div>        <!-- 起飞页/猫头鹰 -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function () {
        var u = "//t2.qifeiye.com/";
        _paq.push(['setTrackerUrl', u + 'js/tracker.php']);
        _paq.push(['setSiteId', '1']);
        var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.async = true;
        g.defer = true;
        g.src = u + 'js/tracker.php';
        s.parentNode.insertBefore(g, s);
    })();
</script>
<!-- End 起飞页/猫头鹰 Code -->

<div id="phantom" class="showed_tb" style="animation-duration: 0.4s;">
    <div class="ph-wrap with-logo">
        <div class="ph-wrap-content" style="max-width: 1600px;">
            <div class="ph-wrap-inner">
                <div class="logo-box">
                    <div style="height:48px;vertical-align: middle;display: table-cell;"><a
                                href="http://www.yzhpps.com/"><img
                                    src="{{ asset('images/yzh/logo.png') }}"
                                    height="88" width="299" style="max-height: 32px;"></a></div>
                </div>
                <div class="menu-box">
                    <ul id="main-nav" data-st="1" data-sp="0" data-fh="0" data-mw="1600" data-lh="32"
                        class="mainmenu fancy-rollovers wf-mobile-hidden position-text-right text-right underline-hover bit-menu-float"
                        data-bit-menu="underline-hover" data-bit-float-menu="underline-hover">
                        @component('./layouts/yzh/nav',['isMobile'=>'0'])
                        @endcomponent
                    </ul>
                </div>
                <div class="menu-info-box align_right">
                    <div class="main-nav-slide-inner" data-class="align_right">
                        <div class="floatmenu-bar-right bit_widget_more" bitdatamarker="bitHeader-3"
                             bitdataaction="site_fix_container" bitdatacolor="white">
                            <div id="simplepage-4" style="margin-top:0;margin-bottom:0;"
                                 class="headerWidget_1 simplepage site_tooler">
                                <style class="style_simplepage-4">#simplepage-4 .widget-title {
                                        padding: 0 0 0 10px;
                                        height: 28px;
                                        line-height: 28px;
                                        background-color: transparent;
                                        margin: 0px;
                                        font-family:;
                                        font-size: px;
                                        font-weight: normal;
                                        font-style: normal;
                                        text-decoration: none;
                                        color: #ffffff;
                                        border-top: 1px solid transparent;
                                        border-left: 1px solid transparent;
                                        border-right: 1px solid transparent;
                                        border-bottom: 0px solid transparent;
                                        background-image: none;
                                        -webkit-border-top-left-radius: 4px;
                                        -webkit-border-top-right-radius: 4px;
                                        -moz-border-radius-topleft: 4px;
                                        -moz-border-radius-topright: 4px;
                                        border-top-left-radius: 4px;
                                        border-top-right-radius: 4px;
                                    }

                                    #simplepage-4 .widget-title {
                                        border-top: 0;
                                        border-left: 0;
                                        border-right: 0;
                                    }

                                    #simplepage-4 .bitWidgetFrame {
                                        border-bottom: 0;
                                        border-top: 0;
                                        border-left: 0;
                                        border-right: 0;
                                        padding: 4px 10px 4px 10px;
                                    }

                                    #simplepage-4 {
                                        -webkit-box-shadow: none;
                                        box-shadow: none;
                                    }

                                    #simplepage-4 .bitWidgetFrame {
                                        background-color: transparent;
                                        background-image: none;
                                        -webkit-border-bottom-left-radius: 4px;
                                        border-bottom-left-radius: 4px;
                                        -webkit-border-bottom-right-radius: 4px;
                                        border-bottom-right-radius: 4px;
                                    }

                                    #simplepage-4 .bitWidgetFrame {
                                        padding-left: 0px;
                                        padding-right: 0px;
                                    }

                                    body #simplepage-4 .bitWidgetFrame {
                                        padding-top: 0px !important;
                                        padding-bottom: 0px !important;
                                    }</style>
                                <div class="simplepage_container bitWidgetFrame" data-post_id="15366">
                                    <section data-fixheight=""
                                             class="qfy-row-1-5d7353185efd1707931 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                             id="bit_38bwq" style="margin-bottom:0;border-radius:0px;color:#382924;">
                                        <style class="row_class qfy_style_class">
                                            @media only screen and (min-width: 992px) {
                                                section.section.qfy-row-1-5d7353185efd1707931 {
                                                    padding-left: 0;
                                                    padding-right: 0;
                                                    padding-top: 0;
                                                    padding-bottom: 0;
                                                    margin-top: 0;
                                                }
                                            }

                                            @media only screen and (max-width: 992px) {
                                                .bit-html section.section.qfy-row-1-5d7353185efd1707931 {
                                                    padding-left: 15px;
                                                    padding-right: 15px;
                                                    padding-top: 0;
                                                    padding-bottom: 0;
                                                    margin-top: 0;
                                                    min-height: 0;
                                                }
                                            }
                                        </style>
                                        <div class="section-background-overlay background-overlay grid-overlay-0 "
                                             style="background-color: #ffffff;"></div>

                                        <div class="container">
                                            <div class="row qfe_row">
                                                <div data-animaleinbegin="bottom-in-view" data-animalename="qfyfadeInUp"
                                                     data-duration="" data-delay=""
                                                     class=" qfy-column-1-5d7353185f349973443 qfy-column-inner  vc_span12  text-default small-screen-default fullrow"
                                                     data-dw="1/1" data-fixheight="">
                                                    <div style=";position:relative;" class="column_inner ">
                                                        <div class=" background-overlay grid-overlay-"
                                                             style="background-color:transparent;width:100%;"></div>
                                                        <div class="column_containter"
                                                             style="z-index:3;position:relative;">
                                                            <div id="vc_header_5d7353185faca830"
                                                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                 css_animation_delay="0" qfyuuid="qfy_header_hy33n-c-y3"
                                                                 data-anitime="0.7" data-ani_iteration_count="1"
                                                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                <style>#vc_header_5d7353185faca830 .qfy_title_inner, #vc_header_5d7353185faca830 .qfy_title_image_inner {
                                                                        vertical-align: middle !important;
                                                                    }

                                                                    @media only screen and (max-width: 768px) {
                                                                        #vc_header_5d7353185faca830 .header_title {
                                                                            font-size: 16px !important;
                                                                        }
                                                                    }

                                                                    @media only screen and (max-width: 768px) {
                                                                        #vc_header_5d7353185faca830 .header_subtitle {
                                                                            font-size: 12px !important;
                                                                        }
                                                                    }</style>
                                                                <div class="qfe_wrapper">
                                                                    <div class="qfy_title left mobileleft">
                                                                        <div class="qfy_title_image_warpper"
                                                                             style="display:inline-block;position:relative;max-width:100%;">
                                                                            <div class="qfy_title_image_inner"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="header_image "
                                                                                     style="padding:5px 0 0 0;width:40px;">
                                                                                    <img class=" ag_image"
                                                                                         src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi8yNDdhMTdlNmRkOGY1ZDE5YjU2YWFmNTQzYjBjMGJhOC00MHg0MC5qcGc_p_p100_p_3D.jpg"
                                                                                         alt="top" description=""
                                                                                         data-attach-id="15369"
                                                                                         data-title="top" title=""
                                                                                         src-img=""></div>
                                                                            </div>
                                                                            <div class="qfy_title_inner"
                                                                                 style="display:inline-block;position:relative;max-width:calc(100% - 40px);">
                                                                                <div class="header_title"
                                                                                     style="font-family:微软雅黑;font-size:16px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                    +86 {{ $configs['site_phone'] }}
                                                                                </div>
                                                                                <div class="header_subtitle"
                                                                                     style="font-size:12px;font-weight:normal;font-style:normal;color:#c5a862;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                    {{ $configs['workday'] }}：{{ $configs['worktime'] }}
                                                                                </div>
                                                                            </div>
                                                                            <style></style>
                                                                            <div class="button_wrapper"
                                                                                 style="display:none;">
                                                                                <div class="button "
                                                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                    + 查看更多
                                                                                </div>
                                                                            </div>
                                                                            <span style="clear:both;"></span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                        .qfy-column-1-5d7353185f349973443 > .column_inner {
                                                            padding-left: 0;
                                                            padding-right: 0;
                                                            padding-top: 0;
                                                            padding-bottom: 0;
                                                        }

                                                        .qfe_row .vc_span_class.qfy-column-1-5d7353185f349973443 {
                                                        }

                                                    ;
                                                    }

                                                    @media only screen and (max-width: 992px) {
                                                        .qfy-column-1-5d7353185f349973443 > .column_inner {
                                                            margin: 0 auto 0 !important;
                                                            padding-left: 0;
                                                            padding-right: 0;
                                                            padding-top:;
                                                            padding-bottom:;
                                                        }

                                                        .display_entire .qfe_row .vc_span_class.qfy-column-1-5d7353185f349973443 {
                                                        }

                                                        .qfy-column-1-5d7353185f349973443 > .column_inner > .background-overlay, .qfy-column-1-5d7353185f349973443 > .column_inner > .background-media {
                                                            width: 100% !important;
                                                            left: 0 !important;
                                                            right: auto !important;
                                                        }
                                                    }</style>
                                            </div>
                                        </div>

                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('script')
@show
</body>
</html>