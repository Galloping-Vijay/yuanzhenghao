<section data-fixheight=""
         class="qfy-row-3-5d73575ab21f3547659 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
         id="bit_5z7xh"
         style="margin-bottom:0;border-radius:0px;color:#ffffff;">
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            section.section.qfy-row-3-5d73575ab21f3547659 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 15vh;
                padding-bottom: 15vh;
                margin-top: 0;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-3-5d73575ab21f3547659 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 40px;
                padding-bottom: 40px;
                margin-top: 0;
                min-height: 0;
            }
        }
    </style>
    <div data-time="3" data-imagebgs="" class="background-media   "
         backgroundsize="true"
         style="background-image: url('/images/yzh/category_back.jpg'); background-repeat:no-repeat; background-size:cover; background-attachment:fixed; background-position:center top;">
    </div>

    <div class="section-background-overlay background-overlay grid-overlay-0 "
         style="background-color: rgba(93,69,60,0.5);"></div>

    <div class="container">
        <div class="row qfe_row">
            <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                 data-duration="" data-delay=""
                 class=" qfy-column-3-5d73575ab2657299437 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                 data-dw="1/1" data-fixheight="">
                <div style=";position:relative;" class="column_inner ">
                    <div class=" background-overlay grid-overlay-"
                         style="background-color:transparent;width:100%;"></div>
                    <div class="column_containter"
                         style="z-index:3;position:relative;">
                        <div id="vc_img_5d73575ab2d4d831"
                             style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                             m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                             css_animation_delay="0"
                             qfyuuid="qfy_single_image_qvgtf-c-ul"
                             data-anitime="0.7" data-ani_iteration_count="1"
                             class="qfy-element bitImageControlDiv ">
                            <style>@media only screen and (max-width: 768px) {
                                    .single_image_text-5d73575ab2e26972 .head {
                                        font-size: 16px !important;
                                    }

                                    .single_image_text-5d73575ab2e26972 .content {
                                        font-size: 16px !important;
                                    }
                                }</style>
                            <a class="bitImageAhover  ">
                                <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_center">
                                    <div class="qfe_wrapper"><span></span><img
                                                class="front_image   ag_image"
                                                src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy9hNWFlZTkwODNhZWQ0YjNiNDJkYTYyZTVkZTY2OGFkOS05MHgzMC5wbmc_p_p100_p_3D.png"
                                                alt="down" description=""
                                                data-attach-id="16721"
                                                data-title="down" title=""
                                                src-img="" style=""></div>
                                </div>
                            </a></div>
                        <a class="bitImageAhover  "> </a>
                        <div m-padding="15px 0 18px 0" p-padding="30px 0 30px 0"
                             css_animation_delay="0"
                             qfyuuid="qfy_column_text_gkebr-c-ul"
                             data-anitime="0.7" data-ani_iteration_count="1"
                             class="qfy-element qfy-text qfy-text-69127 qfe_text_column qfe_content_element  mobile_fontsize30 "
                             style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:30px;padding-bottom:30px;padding-right:0;padding-left:0;border-radius:0px;">
                            <div class="qfe_wrapper">
                                <div style="letter-spacing: 4px; text-align: center;">
                                    <span style="font-size: 30px;"></span><span
                                            style="font-size: 50px;"><strong> @yield('page_title','源正浩PPS')</strong></span><span
                                            style="font-size: 30px;"><strong><br></strong></span>
                                </div>
                            </div>
                        </div>
                        <div id="vc_img_5d73575ab3760189"
                             style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                             m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                             css_animation_delay="0"
                             qfyuuid="qfy_single_image_qvgtf-c-ul"
                             data-anitime="0.7" data-ani_iteration_count="1"
                             class="qfy-element bitImageControlDiv ">
                            <style>@media only screen and (max-width: 768px) {
                                    .single_image_text-5d73575ab3833446 .head {
                                        font-size: 16px !important;
                                    }

                                    .single_image_text-5d73575ab3833446 .content {
                                        font-size: 16px !important;
                                    }
                                }</style>
                            <a class="bitImageAhover  ">
                                <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_center">
                                    <div class="qfe_wrapper"><span></span><img
                                                class="front_image   ag_image"
                                                src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy80MmJlMTlhYzlkOWQ5OGEyZmE4ZjczODk1ODkyNTIyNC05MHgzMC5wbmc_p_p100_p_3D.png"
                                                alt="up" description=""
                                                data-attach-id="16720"
                                                data-title="up" title="" src-img=""
                                                style=""></div>
                                </div>
                            </a></div>
                        <a class="bitImageAhover  "> </a></div>
                </div>
            </div>
            <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                    .qfy-column-3-5d73575ab2657299437 > .column_inner {
                        padding-left: 0;
                        padding-right: 0;
                        padding-top: 0;
                        padding-bottom: 0;
                    }

                    .qfe_row .vc_span_class.qfy-column-3-5d73575ab2657299437 {
                    }

                ;
                }

                @media only screen and (max-width: 992px) {
                    .qfy-column-3-5d73575ab2657299437 > .column_inner {
                        margin: 0 auto 0 !important;
                        padding-left: 0;
                        padding-right: 0;
                        padding-top:;
                        padding-bottom:;
                    }

                    .display_entire .qfe_row .vc_span_class.qfy-column-3-5d73575ab2657299437 {
                    }

                    .qfy-column-3-5d73575ab2657299437 > .column_inner > .background-overlay, .qfy-column-3-5d73575ab2657299437 > .column_inner > .background-media {
                        width: 100% !important;
                        left: 0 !important;
                        right: auto !important;
                    }
                }</style>
        </div>
    </div>

</section>