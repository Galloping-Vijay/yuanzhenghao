@foreach($nav_list as $vk=>$vv)
    <li class="menu-item @if($vv['child'])  has-children @endif">
        <a title="{{ $vv['name'] }}" href="{{ $vv['url'] }}"><span>{{ $vv['name'] }}</span></a>
    @if($vv['child'])
        @if($isMobile)
            <ul class="dl-submenu">
                <li class="menu-item dl-back"><a href="#"><span>返回上一级</span></a></li>
                @foreach($vv['child'] as $sun)
                    <li class="menu-item">
                        <a href="{{ $sun['url'] }}" title="{{ $sun['name'] }}"><span>{{ $sun['name'] }}</span></a>
                    </li>
                @endforeach
            </ul>
        @else
            <ul class="sub-nav">
                @foreach($vv['child'] as $sun)
                    <li class="menu-item">
                        <a href="{{ $sun['url'] }}" title="{{ $sun['name'] }}"><span>{{ $sun['name'] }}</span></a>
                    </li>
                @endforeach
            </ul>
        @endif
    @endif
    </li>
@endforeach