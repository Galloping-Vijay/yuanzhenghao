@extends('layouts.yzh')
@section('header')
    <link href="{{ asset('css/yzh/swiper-4.1.0.min.css') }}" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="http://5d1c71c9850fc.t73.qifeiye.com/FeiEditor/bitSite/js/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://5d1c71c9850fc.t73.qifeiye.com/FeiEditor/bitSite/js/respond.min.js"></script>
    <![endif]-->
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            section.section.qfy-row-3-5d75150d24973558739 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 10vh;
                padding-bottom: 10vh;
                margin-top: 0;
            }

            section.section.qfy-row-3-5d75150d24973558739 > .container {
                max-width: 1600px;
                margin: 0 auto;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-3-5d75150d24973558739 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 40px;
                padding-bottom: 40px;
                margin-top: 0;
                min-height: 0;
            }
        }
    </style>
    <style>
        @media only screen and (max-width: 768px) {
            #vc_header_5d75150d24f82711 .header_title {
                font-size: 24px !important;
            }
        }

        @media only screen and (max-width: 768px) {
            #vc_header_5d75150d24f82711 .header_subtitle {
                font-size: 20px !important;
            }
        }
    </style>
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            section.section.qfy-row-4-5d75150d25725633064 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 20px;
                padding-bottom: 0;
                margin-top: 0;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-4-5d75150d25725633064 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 20px;
                padding-bottom:;
                margin-top: 0;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-4-5d75150d25939370738 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-4-5d75150d25939370738 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-4-5d75150d25939370738 > .column_inner {
                margin: 0 auto 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top:;
                padding-bottom:;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-4-5d75150d25939370738 {
            }

            .qfy-column-4-5d75150d25939370738 > .column_inner > .background-overlay, .qfy-column-4-5d75150d25939370738 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-3-5d75150d24c7b383541 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 30px;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-3-5d75150d24c7b383541 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-3-5d75150d24c7b383541 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-3-5d75150d24c7b383541 {
            }

            .qfy-column-3-5d75150d24c7b383541 > .column_inner > .background-overlay, .qfy-column-3-5d75150d24c7b383541 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style>
        @media only screen and (max-width: 760px) {
            .thumbnail_text-5d75150d26471668 .head {
                font-size: 16px !important;
            }

            .thumbnail_text-5d75150d26471668 .content {
                font-size: 16px !important;
            }
        }
    </style>
    <style>
        .swiper-container.gallerys .swiper-pagination-bullet {
            background: #9999
        }

        .swiper-pagination.fraction {
            color: #9999
        }

        .swiper-container.gallerys .swiper-pagination-bullet-active {
            background: #000000
        }
    </style>
@endsection
@section('content')
    <section data-fixheight=""
             class="qfy-row-3-5d75150d24973558739 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
             id="bit_kg8i9"
             style="margin-bottom:0;border-radius:0px;border-top:1px solid rgba(232,232,232,1);border-bottom:0px solid rgba(232,232,232,1);border-left:0px solid rgba(255,255,255,1);border-right:0px solid rgba(255,255,255,1);color:#382924;">

        <div class="section-background-overlay background-overlay grid-overlay-0 "
             style="background-color: #ffffff;"></div>

        <div class="container">
            <div class="row qfe_row">
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp" data-duration="1" data-delay=""
                     class=" qfy-column-3-5d75150d24c7b383541 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-undefined notfullrow"
                     data-dw="1/3" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:5vw;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter" style="z-index:3;position:relative;">
                            <div id="vc_header_5d75150d24f82711" m-padding="0px 0px 3px 0px" p-padding="0 0 3px 0"
                                 css_animation_delay="0" qfyuuid="qfy_column_text_header_smrs7-c-xn" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:5px;margin-bottom:0;padding-top:0;padding-bottom:3px;padding-right:0;padding-left:0;">

                                <div class="qfe_wrapper">
                                    <h1 class="qfy_title left mobileleft">
                                        <div class="qfy_title_inner"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="header_title"
                                                 style="font-family:微软雅黑;font-size:36px;font-weight:bold;font-style:normal;color:#382924;">
                                                {{ $info->title }}
                                            </div>
                                        </div>
                                        <style></style>
                                        <div class="button_wrapper" style="display:none;">
                                            <div class="button "
                                                 style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                + 查看更多
                                            </div>
                                        </div>
                                        <span style="clear:both;"></span></h1>
                                </div>
                            </div>
                            <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0" css_animation_delay="0"
                                 qfyuuid="qfy_column_text_viewgroup_r2xla" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element qfy-text qfy-text-56582 qfe_text_column qfe_content_element  mobile_align_inherit "
                                 style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:#c5a862;text-align:inherit;letter-spacing:;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                <div class="qfe_wrapper">
                                    <span style="font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:">标签：</span>{{ $info->keywords }}
                                </div>
                            </div>
                            <div m-padding="0 0 0 0" p-padding="0 0 0 0" css_animation_delay="0"
                                 qfyuuid="qfy_column_text_view_ouq1q-c-ty" data-anitime="0.7"
                                 class="qfy-element qfy-text qfy-text-73916   mobile_fontsize "
                                 style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;font-size:14px ;font-weight:normal ;font-style:normal ;color:;text-align:;letter-spacing:;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                <div class="qfe_wrapper">
                                    <section data-fixheight=""
                                             class="qfy-row-4-5d75150d25725633064 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                             style="margin-bottom:0;border-radius:0px;color:#382924;">

                                        <div class="section-background-overlay background-overlay grid-overlay-0 "
                                             style="background-color: #ffffff;"></div>

                                        <div class="container">
                                            <div class="row qfe_row">
                                                <div data-animaleinbegin="bottom-in-view" data-animalename="qfyfadeInUp"
                                                     data-duration="" data-delay=""
                                                     class=" qfy-column-4-5d75150d25939370738 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                                                     data-dw="1/1" data-fixheight="">
                                                    <div style=";position:relative;" class="column_inner ">
                                                        <div class=" background-overlay grid-overlay-"
                                                             style="background-color:transparent;width:100%;"></div>
                                                        <div class="column_containter"
                                                             style="z-index:3;position:relative;">
                                                            <div m-padding="0px 0px 0px 0px" p-padding="0px 0px 0px 0px"
                                                                 css_animation_delay="0" qfyuuid="0"
                                                                 class="qfy-element qfy-text qfy-text-37763 qfe_text_column qfe_content_element  mobile_fontsize "
                                                                 style="position: relative;;;line-height:1.5em;;background-repeat: no-repeat;;">
                                                                <div class="qfe_wrapper">
                                                                    {!! htmlspecialchars_decode($info->content) !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div data-animaleinbegin="bottom-in-view" data-animalename="qfyfadeInUp" data-duration="" data-delay=""
                     class=" qfy-column-5-5d75150d260bf161335 qfy-column-inner  vc_span_class  vc_span8  text-default small-screen-default notfullrow"
                     data-dw="2/3" data-fixheight="">
                    <div style=";position:relative;" class="column_inner ">
                        <div class=" background-overlay grid-overlay-"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter" style="z-index:3;position:relative;">
                            <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0" css_animation_delay="0"
                                 qfyuuid="qfy_column_text_sliders_mlcyv"
                                 class="qfy-element qfe_gallery qfe_content_element vc_clearfix"
                                 style="max-width:100%;width:100%;; margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <div style="" class="qfe_wrapper ">

                                    <div id="vc-gallery-5d75150d27162612578"
                                         class="swiper-container gallerys loaded swiper-container-horizontal swiper-container-wp8-horizontal"
                                         data-swiper-group="" data-swiper-effect="slide" data-swiper-freemodel="0"
                                         data-swiper-slidesperview="1" data-swiper-percolumn="1"
                                         data-swiper-direction="horizontal" data-swiper-space="0" data-swiper-center="0"
                                         d="" data-swiper-autoheight="0" data-swiper-pagination="none"
                                         data-swiper-loop="1" data-swiper-autoplay="30000"
                                         style="margin: 0px auto; width: 100%; max-width: 100%; cursor: grab;">
                                        <ul style="list-style: none; transform: translate3d(-836px, 0px, 0px); transition-duration: 0ms;"
                                            class="swiper-wrapper">
                                            <li class="swiper-slide swiper-slide-duplicate swiper-slide-prev"
                                                data-swiper-slide-index="2" style="width: 836px;">
                                                <div class="qfy_image_vc_item " style="position:relative;"><img class=" ag_image" data-width="1016" style="max-width:100%;" src="{{ $info->cover }}" alt="details1" description="" data-attach-id="15419"  data-title="details1" title="" src-img=""><i></i>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-5-5d75150d260bf161335 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-5-5d75150d260bf161335 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-5-5d75150d260bf161335 > .column_inner {
                            margin: 0 auto 0 !important;
                            padding-left: 0;
                            padding-right: 0;
                            padding-top:;
                            padding-bottom:;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-5-5d75150d260bf161335 {
                        }

                        .qfy-column-5-5d75150d260bf161335 > .column_inner > .background-overlay, .qfy-column-5-5d75150d260bf161335 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div class="wf-mobile-hidden qfy-clumn-clear" style="clear:both;"></div>
            </div>
        </div>

    </section>
@endsection
@section('script')
    <script src="{{ asset('js/home/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ asset('js/yzh/product.js') }}"></script>
@show
