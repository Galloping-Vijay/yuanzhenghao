@extends('layouts.yzh')
@section('header')
    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
            .qfy-column-5-5d7514d628f5f774723 > .column_inner {
                padding-left: 30px;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-5-5d7514d628f5f774723 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-5-5d7514d628f5f774723 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-5-5d7514d628f5f774723 {
            }

            .qfy-column-5-5d7514d628f5f774723 > .column_inner > .background-overlay, .qfy-column-5-5d7514d628f5f774723 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }</style>
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            section.section.qfy-row-3-5d7514d6254ee777806 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 10vh;
                padding-bottom: 10vh;
                margin-top: 0;
            }

            section.section.qfy-row-3-5d7514d6254ee777806 > .container {
                max-width: 1600px;
                margin: 0 auto;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-3-5d7514d6254ee777806 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 40px;
                padding-bottom: 40px;
                margin-top: 0;
                min-height: 0;
            }
        }
    </style>
    <style>@media only screen and (max-width: 768px) {
            #vc_header_5d7514d625b25451 .header_title {
                font-size: 24px !important;
            }
        }

        @media only screen and (max-width: 768px) {
            #vc_header_5d7514d625b25451 .header_subtitle {
                font-size: 20px !important;
            }
        }</style>
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            section.section.qfy-row-4-5d7514d6266ff411982 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 20px;
                padding-bottom: 0;
                margin-top: 0;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-4-5d7514d6266ff411982 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 20px;
                padding-bottom:;
                margin-top: 0;
            }
        }
    </style>
    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
            .qfy-column-4-5d7514d62690f376562 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-4-5d7514d62690f376562 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-4-5d7514d62690f376562 > .column_inner {
                margin: 0 auto 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top:;
                padding-bottom:;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-4-5d7514d62690f376562 {
            }

            .qfy-column-4-5d7514d62690f376562 > .column_inner > .background-overlay, .qfy-column-4-5d7514d62690f376562 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }</style>
    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
            .qfy-column-3-5d7514d62580d417739 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-3-5d7514d62580d417739 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-3-5d7514d62580d417739 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-3-5d7514d62580d417739 {
            }

            .qfy-column-3-5d7514d62580d417739 > .column_inner > .background-overlay, .qfy-column-3-5d7514d62580d417739 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }</style>
    <style>.vcprenext-5d7514d6270f31350 a:hover span {
            color: #c5a862 !important
        }

        .vcprenext-5d7514d6270f31350 a:hover svg polyline {
            stroke: #c5a862 !important
        }</style>
    <style>.UUID-POSTS-5d7514d62dcd3650090 li:hover .post-title > i.glyphicon {
            color: #cccccc !important;
        }

        .UUID-POSTS-5d7514d62dcd3650090 li:hover .post-title > a, .UUID-POSTS-5d7514d62dcd3650090 li:hover .post-title > div > a, .UUID-POSTS-5d7514d62dcd3650090 li:hover .post-title > span {
            color: #382924 !important;
        }

        .UUID-POSTS-5d7514d62dcd3650090 li:hover .post-title > span.toEditor {
            color: #fff !important;
        }

        .UUID-POSTS-5d7514d62dcd3650090 li:hover .subtitle {
            color: #cccccc !important;
        }

        .UUID-POSTS-5d7514d62dcd3650090 li .post-comment {
            width: 100% !important;
            text-align: left
        }

        .UUID-POSTS-5d7514d62dcd3650090 li:hover .vc_read_more {
        }

        @media only screen and (max-width: 992px) {
            .UUID-POSTS-5d7514d62dcd3650090 .vc_span_mobile {
                float: left !important;
                max-width: 99.8% !important;
            }

            .UUID-POSTS-5d7514d62dcd3650090 .post_blog .vc_span_mobile .blog-media {
                width: auto !important;
            }

            .thumbnail_text-5d7514d62dc86298 .head {
                font-size: 16px !important;
            }

            .thumbnail_text-5d7514d62dc86298 .content {
                font-size: 16px !important;
            }
        }

        .UUID-POSTS-5d7514d62dcd3650090 .qfy_item_post .price_warp .amount, .UUID-POSTS-5d7514d62dcd3650090 .qfy_item_post .price_warp del {
            color: #cccccc;
            font-size: 12px;
        }

        .UUID-POSTS-5d7514d62dcd3650090 .qfy_item_post .price_warp ins .amount {
            color: #cccccc;
            font-size: 12px;
        }

        .UUID-POSTS-5d7514d62dcd3650090 .qfy_item_post .price_warp {
            padding-top: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            padding-right: 0px;
        }

        .UUID-POSTS-5d7514d62dcd3650090 .qfy_item_post:hover .price_warp .amount, .UUID-POSTS-5d7514d62dcd3650090 .qfy_item_post:hover .price_warp del {
            color: #cccccc !important;
        }

        .UUID-POSTS-5d7514d62dcd3650090 .qfy_item_post:hover .price_warp ins .amount {
            color: #cccccc !important;
        }

        .UUID-POSTS-5d7514d62dcd3650090 li:hover {
            transition: all .6s ease;
        }

        .UUID-POSTS-5d7514d62dcd3650090 li {
            transition: all .6s ease;
        }</style>
@endsection
@section('content')
    <section data-fixheight=""
             class="qfy-row-3-5d7514d6254ee777806 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
             id="bit_ic6ni"
             style="margin-bottom:0;border-radius:0px;border-top:1px solid rgba(232,232,232,1);border-bottom:0px solid rgba(232,232,232,1);border-left:0px solid rgba(255,255,255,1);border-right:0px solid rgba(255,255,255,1);color:#382924;">
        <div class="section-background-overlay background-overlay grid-overlay-0 "
             style="background-color: #ffffff;"></div>
        <div class="container">
            <div class="row qfe_row">
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-3-5d7514d62580d417739 qfy-column-inner  vc_span_class  vc_span8  text-Default small-screen-undefined notfullrow"
                     data-dw="2/3" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:5vw;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d7514d625b25451"
                                 m-padding="0px 0px 3px 0px" p-padding="0 0 3px 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_column_text_header_smrs7-c-y4"
                                 data-anitime="0.7" data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-bottom:0;padding-top:0;padding-bottom:3px;padding-right:0;padding-left:0;">

                                <div class="qfe_wrapper">
                                    <h1 class="qfy_title left mobileleft">
                                        <div class="qfy_title_inner"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="header_title"
                                                 style="font-family:微软雅黑;font-size:36px;font-weight:bold;font-style:normal;color:#382924;">
                                                {{ $info->title }}
                                            </div>
                                        </div>
                                        <style></style>
                                        <div class="button_wrapper"
                                             style="display:none;">
                                            <div class="button "
                                                 style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                + 查看更多
                                            </div>
                                        </div>
                                        <span style="clear:both;"></span></h1>
                                </div>
                            </div>
                            <div m-padding="5px 0px 2px 0px" p-padding="5px 0 2px 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_column_text_view_pldre"
                                 data-anitime="0.7" data-ani_iteration_count="1"
                                 class="qfy-element qfy-text qfy-text-71499 qfe_text_column qfe_content_element  mobile_align_inherit "
                                 style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:#c5a862;text-align:inherit;letter-spacing:;;margin-top:0;margin-bottom:0;padding-top:5px;padding-bottom:2px;padding-right:0;padding-left:0;border-radius:0px;">
                                <div class="qfe_wrapper">
                                    <span style="font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:;">发布日期：</span>{{ $info->created_at }}
                                </div>
                            </div>
                            <div style="margin-top:0;margin-bottom:0;padding-top:19px;padding-bottom:0;padding-right:0;padding-left:0;"
                                 m-padding="19px 0px 0px 0px" p-padding="19px 0 0 0"
                                 css_animation_delay="0" qfyuuid="qfyshare_gwlwy"
                                 class="qfy-share qfy-jiathis qfy-element  ">

                            </div>
                            <div m-padding="0 0 0 0" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_column_text_view_ouq1q-c-y4"
                                 data-anitime="0.7"
                                 class="qfy-element qfy-text qfy-text-27230   mobile_fontsize "
                                 style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;font-size:14px ;font-weight:normal ;font-style:normal ;color:;text-align:;letter-spacing:;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                <div class="qfe_wrapper">
                                    <section data-fixheight=""
                                             class="qfy-row-4-5d7514d6266ff411982 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                             style="margin-bottom:0;border-radius:0px;color:#382924;">

                                        <div class="section-background-overlay background-overlay grid-overlay-0 "
                                             style="background-color: #ffffff;"></div>

                                        <div class="container">
                                            <div class="row qfe_row">
                                                <div data-animaleinbegin="bottom-in-view"
                                                     data-animalename="qfyfadeInUp"
                                                     data-duration="" data-delay=""
                                                     class=" qfy-column-4-5d7514d62690f376562 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                                                     data-dw="1/1"
                                                     data-fixheight="">
                                                    <div style=";position:relative;"
                                                         class="column_inner ">
                                                        <div class=" background-overlay grid-overlay-"
                                                             style="background-color:transparent;width:100%;"></div>
                                                        <div class="column_containter"
                                                             style="z-index:3;position:relative;">
                                                            <div m-padding="0px 0px 0px 0px"
                                                                 p-padding="0px 0px 0px 0px"
                                                                 css_animation_delay="0"
                                                                 qfyuuid="0"
                                                                 class="qfy-element qfy-text qfy-text-60962 qfe_text_column qfe_content_element  mobile_fontsize "
                                                                 style="position: relative;;;line-height:1.5em;;background-repeat: no-repeat;;">
                                                                <div class="qfe_wrapper">{!! htmlspecialchars_decode($info->content) !!}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </section>
                                </div>
                            </div>
                            <div style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0" qfyuuid="qfy_prenext_tq83q"
                                 class="qfy-prenext  qfy-element  lrlayout vcprenext-5d7514d6270f31350">

                                <div class="prenext_inner ">
                                    @if(!empty($pre))
                                        <div class="pre_inner " style="padding:20px 0;">
                                            <a href="/article/{{ $pre->id }}"
                                               class="pre_a">
                                                <div style="display:table-cell;vertical-align: middle;"
                                                     class="icon_warp">
                                                    <svg style="	stroke: #c5a862;margin-right:10px;width:16px;height:50px;"
                                                         class="qfy-pagination-link-icon"
                                                         viewBox="0 0 23 48">
                                                        <g class="svg-icon">
                                                            <polyline fill="none" stroke-miterlimit="10"
                                                                      points="21.5,1.3 2.6,23.4 21.5,45.7 "></polyline>
                                                        </g>
                                                    </svg>
                                                </div>
                                                <div style="display:table-cell;vertical-align: middle;height:50px;overflow:hidden;"
                                                     class="td_warp">
                                                    <div style="display:table;vertical-align: middle;"
                                                         class="td_inner"><span
                                                                class="action"
                                                                style="font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:#c5a862;">上一篇</span><span
                                                                class="blank"
                                                                style="width:10px;height:0px"></span><span
                                                                class="title text-overflow"
                                                                style="font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:#c5a862;max-width:300px;">{{ $pre->title }}</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                    @if(!empty($next))
                                        <div class="next_inner first "
                                             style="padding:20px 0;"><a
                                                    href="/article/{{ $next->id }}"
                                                    class="next_a">
                                                <div style="display:table-cell;vertical-align: middle;height:50px;overflow:hidden;"
                                                     class="td_warp">
                                                    <div style="display:table;vertical-align: middle;"
                                                         class="td_inner"><span
                                                                class="action"
                                                                style="font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:#c5a862;">下一篇</span><span
                                                                class="blank"
                                                                style="width:10px;height:0px"></span><span
                                                                class="title text-overflow"
                                                                style="font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:#c5a862;max-width:300px;">{{ $next->title }}</span>
                                                    </div>
                                                </div>
                                                <div style="display:table-cell;vertical-align: middle;"
                                                     class="icon_warp">
                                                    <svg style="stroke:#c5a862;margin-left:10px;width:16px;height:50px;"
                                                         class="qfy-pagination-link-icon"
                                                         viewBox="0 0 23 48">
                                                        <g class="svg-icon">
                                                            <polyline fill="none"
                                                                      stroke-miterlimit="10"
                                                                      points="1.5,45.7 20.4,23.5 1.5,1.3"></polyline>
                                                        </g>
                                                    </svg>
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                    <div class="next_inner second"
                                         style="padding:20px 0;"><a
                                                href="http://5d1c71c9850fc.t73.qifeiye.com/?p=13391">
                                            <div style="display:table-cell;vertical-align: middle;">
                                                <svg style="stroke: #c5a862;margin-right:10px;width:16px;height:50px;"
                                                     class="qfy-pagination-link-icon"
                                                     viewBox="0 0 23 48">
                                                    <g class="svg-icon">
                                                        <polyline fill="none"
                                                                  stroke-miterlimit="10"
                                                                  points="1.5,45.7 20.4,23.5 1.5,1.3"></polyline>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div style="display:table-cell;vertical-align: middle;height:50px;overflow:hidden;"
                                                 class="td_warp">
                                                <div style="display:table;vertical-align: middle;"
                                                     class="td_inner"><span
                                                            class="action"
                                                            style="font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:#c5a862;">下一篇</span><span
                                                            class="blank"
                                                            style="width:10px;height:0px"></span><span
                                                            class="title text-overflow"
                                                            style="font-size:14px ;font-weight:normal ;font-style:normal ;font-family:默认 ;color:#c5a862;max-width:300px;">强者云集 华腾杯2019铝合金门窗品牌征选活动燃爆盛夏</span>
                                                </div>
                                            </div>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-animaleinbegin="bottom-in-view"
                     data-animalename="qfyfadeInUp" data-duration="1" data-delay=""
                     class=" qfy-column-5-5d7514d628f5f774723 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-Default mobileHidden notfullrow"
                     data-dw="1/3" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="" data-loading="" data-loading-w=""
                                 data-open="" data-post="post" data-cate=" "
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_posts_grid_zwd1u"
                                 class="qfy-element  UUID-POSTS-5d7514d62dcd3650090 qfe_teaser_grid qfe_content_element  qfe_grid columns_count_1 columns_count_1 qfe_teaser_grid_post  "
                                 style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <div class="qfe_wrapper">
                                    <div class="teaser_grid_container noanimale"
                                         style=";clear:both;" data-type="post"
                                         data-cate=" " data-pcate="">
                                        <ul style="min-height:60px;"
                                            class="qfe_thumbnails qfe_thumbnails-fluid vc_clearfix  post_blog "
                                            data-layout-mode="fitRows">
                                            @foreach($artList as $art)
                                                <li data-postid="{{ $art->id }}"
                                                    data-animaleinbegin="bottom-in-view"
                                                    data-animalename="qfyfadeInUp"
                                                    data-delay="0" data-duration=""
                                                    class="isotope-item qfy_item_post vc_span12 vc_span_mobile vc_span_mobile12 grid-cat-1 grid-cat-2"
                                                    style="max-width:99.8%;margin-bottom:15px;padding-bottom:15px;border-bottom:1px solid #e5e5e5;">
                                                    <div class="itembody  itempcbody" style="">
                                                        <div class="blog-content wf-td"
                                                             style="display:block;word-break:break-all">
                                                            <div class="post-title"
                                                                 style="font-weight:normal;color:#382924;line-height:15px; vertical-align: top; text-align:left;width:100%; ">
                                                                <a data-title="true"
                                                                   style="color:#382924;font-size:15px;font-family:微软雅黑;line-height:15px"
                                                                   href="/article/{{ $art->id }}"
                                                                   class="bitImageAhover   link_title"
                                                                   title="{{ $art->title }}">{{ $art->title }}</a></div>
                                                            <div class="post-comment"
                                                                 style="color:#bfbfbf;font-size:12px;line-height:12px;font-family:微软雅黑;">
    <span> <span class="post_date">发布日期：<span data-date="{{ $art->created_at }}"
                                              class="post_date_text">{{ $art->created_at }}</span></span>		</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>

                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wf-mobile-hidden qfy-clumn-clear"
                     style="clear:both;"></div>
            </div>
        </div>
    </section>
@endsection
