@extends('layouts.yzh')
@section('header')

@endsection
@section('page_title', '联系我们')
@section('content')
    @include('./layouts/yzh/back')
    <section data-fixheight=""
             class="qfy-row-4-5d73575ab3a30650623 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
             id="bit_pvcrx"
             style="margin-bottom:0;border-radius:0px;color:#382924;">
        <style class="row_class qfy_style_class">
            @media only screen and (min-width: 992px) {
                section.section.qfy-row-4-5d73575ab3a30650623 {
                    padding-left: 0;
                    padding-right: 0;
                    padding-top: 15vh;
                    padding-bottom: 15vh;
                    margin-top: 0;
                }

                section.section.qfy-row-4-5d73575ab3a30650623 > .container {
                    max-width: 1600px;
                    margin: 0 auto;
                }
            }

            @media only screen and (max-width: 992px) {
                .bit-html section.section.qfy-row-4-5d73575ab3a30650623 {
                    padding-left: 15px;
                    padding-right: 15px;
                    padding-top: 20px;
                    padding-bottom: 40px;
                    margin-top: 0;
                    min-height: 0;
                }
            }
        </style>
        <div class="section-background-overlay background-overlay grid-overlay-0 "
             style="background-color: #ffffff;"></div>

        <div class="container">
            <div class="row qfe_row">
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-4-5d73575ab3cdc491733 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-undefined notfullrow"
                     data-dw="1/3" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;" class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d73575ab3fbb667"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_header_kh8w7-c-65" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>@font-face {
                                        font-family: "fdc31ec57bc2f0c6aaac66299585a50e4";
                                        src: url("https://fonts.goodq.top/201907/fdc31ec57bc2f0c6aaac66299585a50e4/SourceHanSansCN-Heavy.eot"); /* IE9 */
                                        src: url("https://fonts.goodq.top/201907/fdc31ec57bc2f0c6aaac66299585a50e4/SourceHanSansCN-Heavy.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */ url("https://fonts.goodq.top/201907/fdc31ec57bc2f0c6aaac66299585a50e4/SourceHanSansCN-Heavy.woff") format("woff"), /* chrome, firefox */ url("https://fonts.goodq.top/201907/fdc31ec57bc2f0c6aaac66299585a50e4/SourceHanSansCN-Heavy.ttf") format("truetype"), /* chrome, firefox, opera, Safari, Android, iOS 4.2+ */ url("https://fonts.goodq.top/201907/fdc31ec57bc2f0c6aaac66299585a50e4/SourceHanSansCN-Heavy.svg#fdc31ec57bc2f0c6aaac66299585a50e4") format("svg"); /* iOS 4.1- */
                                        font-style: normal;
                                        font-weight: normal;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d73575ab3fbb667 .header_title {
                                            font-size: 24px !important;
                                        }
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d73575ab3fbb667 .header_subtitle {
                                            font-size: 12px !important;
                                        }
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="qfy_title left mobileleft">
                                        <div class="qfy_title_image_warpper"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="qfy_title_inner"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="header_title"
                                                     style="font-family:fdc31ec57bc2f0c6aaac66299585a50e4;font-size:36px;font-weight:bold;font-style:normal;color:382924;padding:0 0 15px 0;display:block;padding:0 0 15px 0;vertical-align:bottom;">
                                                    CENTER OFFICE
                                                </div>
                                                <div class="header_subtitle"
                                                     style="font-family:century gothic;font-size:13px;font-weight:normal;font-style:normal;color:#bababa;display:block;vertical-align:bottom;"></div>
                                            </div>
                                            <style></style>
                                            <div class="button_wrapper"
                                                 style="display:none;">
                                                <div class="button "
                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                    + 查看更多
                                                </div>
                                            </div>
                                            <span style="clear:both;"></span></div>
                                    </div>
                                </div>
                            </div>
                            {!! htmlspecialchars_decode($info->content) !!}
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-4-5d73575ab3cdc491733 > .column_inner {
                            padding-left: 0;
                            padding-right: 5vw;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-4-5d73575ab3cdc491733 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-4-5d73575ab3cdc491733 > .column_inner {
                            margin: 0 auto 0 !important;
                            min-height: 0 !important;
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-4-5d73575ab3cdc491733 {
                        }

                        .qfy-column-4-5d73575ab3cdc491733 > .column_inner > .background-overlay, .qfy-column-4-5d73575ab3cdc491733 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-5-5d73575ab5e2e524307 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-undefined notfullrow"
                     data-dw="1/3" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d73575ab611a8"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0" qfyuuid="qfy_header_kh8w7"
                                 data-anitime="0.7" data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>@font-face {
                                        font-family: "f97c0d7efb1ef9bb52608d4c7a1cf0c6e";
                                        src: url("https://fonts.goodq.top/201907/f97c0d7efb1ef9bb52608d4c7a1cf0c6e/SourceHanSansCN-Heavy.eot"); /* IE9 */
                                        src: url("https://fonts.goodq.top/201907/f97c0d7efb1ef9bb52608d4c7a1cf0c6e/SourceHanSansCN-Heavy.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */ url("https://fonts.goodq.top/201907/f97c0d7efb1ef9bb52608d4c7a1cf0c6e/SourceHanSansCN-Heavy.woff") format("woff"), /* chrome, firefox */ url("https://fonts.goodq.top/201907/f97c0d7efb1ef9bb52608d4c7a1cf0c6e/SourceHanSansCN-Heavy.ttf") format("truetype"), /* chrome, firefox, opera, Safari, Android, iOS 4.2+ */ url("https://fonts.goodq.top/201907/f97c0d7efb1ef9bb52608d4c7a1cf0c6e/SourceHanSansCN-Heavy.svg#f97c0d7efb1ef9bb52608d4c7a1cf0c6e") format("svg"); /* iOS 4.1- */
                                        font-style: normal;
                                        font-weight: normal;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d73575ab611a8 .header_title {
                                            font-size: 24px !important;
                                        }
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d73575ab611a8 .header_subtitle {
                                            font-size: 12px !important;
                                        }
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="qfy_title left mobileleft">
                                        <div class="qfy_title_image_warpper"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="qfy_title_inner"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="header_title"
                                                     style="font-family:f97c0d7efb1ef9bb52608d4c7a1cf0c6e;font-size:36px;font-weight:bold;font-style:normal;color:382924;padding:0 0 15px 0;display:block;padding:0 0 15px 0;vertical-align:bottom;">
                                                    CONTCT FORM
                                                </div>
                                                <div class="header_subtitle"
                                                     style="font-family:century gothic;font-size:13px;font-weight:normal;font-style:normal;color:#bababa;display:block;vertical-align:bottom;"></div>
                                            </div>
                                            <style></style>
                                            <div class="button_wrapper"
                                                 style="display:none;">
                                                <div class="button "
                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                    + 查看更多
                                                </div>
                                            </div>
                                            <span style="clear:both;"></span></div>
                                    </div>
                                </div>
                            </div>
                            <div data-tag="iphorm_vc" data-id="67"
                                 m-padding="33px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0" qfyuuid="iphorm_vc_kacvd"
                                 class="qfy-element qfe_layerslider_element qfe_content_element"
                                 style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <div id="iphorm-outer-5d73575ab67a6"
                                     class="bitWidgetFrame iphorm-outer iphorm-outer-67 iphorm-uniform-theme-default">

                                    <form id="iphorm-5d73575ab67a6"
                                          class="iphorm iphorm-form-67"
                                          action="http://5d1c71c9850fc.t73.qifeiye.com/?page_id=11813#iphorm-5d73575ab67a6"
                                          method="post"
                                          enctype="multipart/form-data"
                                          data-check="">
                                        <div class="iphorm-inner iphorm-inner-67">
                                            <input type="hidden" name="iphorm_id"
                                                   value="67">
                                            <input type="hidden" name="iphorm_uid"
                                                   value="5d73575ab67a6">
                                            <input type="hidden" name="form_url"
                                                   value="http://5d1c71c9850fc.t73.qifeiye.com/?page_id=11813">
                                            <input type="hidden"
                                                   name="referring_url"
                                                   value="http://5d1c71c9850fc.t73.qifeiye.com/?page_id=11761">
                                            <input type="hidden" name="post_id"
                                                   value="11813">
                                            <input type="hidden" name="post_title"
                                                   value="联系我们">

                                            <div class="iphorm-success-message iphorm-hidden"></div>
                                            <div class="iphorm-error-message iphorm-hidden"></div>
                                            <input type="hidden" class="popaction"
                                                   value="">
                                            <div class="iphorm-elements iphorm-elements-67 iphorm-clearfix">
                                                <div class="iphorm-group-wrap iphorm_67_6-group-wrap iphorm-clearfix iphorm-labels-inside iphorm-group-style-plain iphorm-group-alignment-proportional">
                                                    <div class="iphorm-group-elements">
                                                        <div class="iphorm-group-row iphorm-clearfix iphorm-group-row-2cols last-child">
                                                            <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_67_1-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-required">
                                                                <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_67_1-element-spacer">
                                                                    <label for="iphorm_67_1_5d73575ab67a6"
                                                                           style="color: rgb(204, 204, 204);font-size: 14px;line-height:40px;margin-top:0;;">
                                                                        &nbsp;&nbsp;姓名 </label>
                                                                    <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_67_1-input-wrap">
                                                                        <input class="iphorm-element-text  iphorm_67_1"
                                                                               id="iphorm_67_1_5d73575ab67a6"
                                                                               type="text"
                                                                               name="iphorm_67_1"
                                                                               value=""
                                                                               placeholder=""
                                                                               style="background-color: rgb(255, 255, 255);border: 1px solid rgb(56, 41, 36);color: rgb(115, 113, 115);height: 40px;font-size: 14px;">
                                                                    </div>
                                                                    <div class="iphorm-errors-wrap iphorm-hidden">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_67_8-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-optional last-child">
                                                                <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_67_8-element-spacer">
                                                                    <label for="iphorm_67_8_5d73575ab67a6"
                                                                           style="color: rgb(204, 204, 204);font-size: 14px;line-height:40px;margin-top:0;;">
                                                                        &nbsp;&nbsp;电话 </label>
                                                                    <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_67_8-input-wrap">
                                                                        <input class="iphorm-element-text  iphorm_67_8"
                                                                               id="iphorm_67_8_5d73575ab67a6"
                                                                               type="text"
                                                                               name="iphorm_67_8"
                                                                               value=""
                                                                               placeholder=""
                                                                               style="background-color: rgb(255, 255, 255);border: 1px solid rgb(56, 41, 36);color: rgb(115, 113, 115);height: 40px;font-size: 14px;">
                                                                    </div>
                                                                    <div class="iphorm-errors-wrap iphorm-hidden">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.iphorm-group-row -->
                                                    </div>
                                                    <!-- /.iphorm-group-elements -->
                                                </div> <!-- /.iphorm-group-wrap -->
                                                <div class="iphorm-group-wrap iphorm_67_9-group-wrap iphorm-clearfix iphorm-labels-inside iphorm-group-style-plain iphorm-group-alignment-proportional">
                                                    <div class="iphorm-group-elements">
                                                        <div class="iphorm-group-row iphorm-clearfix iphorm-group-row-2cols last-child">
                                                            <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_67_5-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-optional">
                                                                <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_67_5-element-spacer">
                                                                    <label for="iphorm_67_5_5d73575ab67a6"
                                                                           style="color: rgb(204, 204, 204);font-size: 14px;line-height:40px;margin-top:0;;">
                                                                        &nbsp;&nbsp;地址 </label>
                                                                    <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_67_5-input-wrap">
                                                                        <input class="iphorm-element-text  iphorm_67_5"
                                                                               id="iphorm_67_5_5d73575ab67a6"
                                                                               type="text"
                                                                               name="iphorm_67_5"
                                                                               value=""
                                                                               placeholder=""
                                                                               style="background-color: rgb(255, 255, 255);border: 1px solid rgb(56, 41, 36);color: rgb(115, 113, 115);height: 40px;font-size: 14px;">
                                                                    </div>
                                                                    <div class="iphorm-errors-wrap iphorm-hidden">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_67_3-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-required last-child">
                                                                <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_67_3-element-spacer">
                                                                    <label for="iphorm_67_3_5d73575ab67a6"
                                                                           style="color: rgb(204, 204, 204);font-size: 14px;line-height:40px;margin-top:0;;">
                                                                        &nbsp;&nbsp;邮箱 </label>
                                                                    <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_67_3-input-wrap">
                                                                        <input class="iphorm-element-text  iphorm_67_3"
                                                                               id="iphorm_67_3_5d73575ab67a6"
                                                                               type="text"
                                                                               name="iphorm_67_3"
                                                                               value=""
                                                                               placeholder=""
                                                                               style="background-color: rgb(255, 255, 255);border: 1px solid rgb(56, 41, 36);color: rgb(115, 113, 115);height: 40px;font-size: 14px;">
                                                                    </div>
                                                                    <div class="iphorm-errors-wrap iphorm-hidden">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.iphorm-group-row -->
                                                    </div>
                                                    <!-- /.iphorm-group-elements -->
                                                </div> <!-- /.iphorm-group-wrap -->
                                                <div class="iphorm-element-wrap iphorm-element-wrap-textarea iphorm_67_4-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-required">

                                                    <div class="iphorm-element-spacer iphorm-element-spacer-textarea iphorm_67_4-element-spacer">
                                                        <label for="iphorm_67_4_5d73575ab67a6"
                                                               style="color: rgb(204, 204, 204);font-size: 14px;line-height:14px;">
                                                            &nbsp;&nbsp;详情 </label>
                                                        <div class="iphorm-input-wrap iphorm-input-wrap-textarea iphorm_67_4-input-wrap">
                                                                                                <textarea placeholder=""
                                                                                                          class="iphorm-element-textarea  iphorm_67_4"
                                                                                                          id="iphorm_67_4_5d73575ab67a6"
                                                                                                          name="iphorm_67_4"
                                                                                                          style="background-color: rgb(255, 255, 255);border: 1px solid rgb(56, 41, 36);color: rgb(115, 113, 115);font-size: 14px;"
                                                                                                          rows="3"
                                                                                                          cols="25"></textarea>
                                                        </div>
                                                        <div class="iphorm-errors-wrap iphorm-hidden">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="iphorm-hidden">
                                                    <label>这个输入框应该是留空的<input
                                                                type="text"
                                                                name="iphorm_67_0"></label>
                                                </div>
                                                <div class="iphorm-submit-wrap iphorm-submit-wrap-67 iphorm-clearfix">

                                                    <div class="iphorm-submit-input-wrap iphorm-submit-input-wrap-67">

                                                        <button class="iphorm-submit-element"
                                                                style="width:px;"
                                                                type="submit"
                                                                name="iphorm_submit">
                                                                                                <span style="font-size: 14px;line-height:14px;;padding-left: 55px !important;;background-color: #382924;border: 1px solid #382924;"><em
                                                                                                            style="color: #ffffff;font-size: 14px;line-height:14px;padding:15px 55px 15px 0;;background-color: #382924;">发送</em></span>
                                                        </button>

                                                        <div style="display:inline-block;">
                                                            <div class="iphorm-loading-wrap">
                                                                <span class="iphorm-loading">请稍候</span>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <style>.iphorm-elements-67, .iphorm-elements-67 button, .iphorm-elements-67 input, .iphorm-elements-67 select, .iphorm-elements-67 textarea {
                                                        font-family: 微软雅黑;
                                                        font-style: normal
                                                    }

                                                    .iphorm-submit-wrap-67 button.iphorm-submit-element:hover span, .iphorm-submit-wrap-67 button.iphorm-submit-element:hover em {
                                                        background-color: #382924 !important;
                                                    }

                                                    .iphorm-submit-wrap-67 button.iphorm-submit-element:hover span, .iphorm-submit-wrap-67 button.iphorm-submit-element:hover em {
                                                        border-color: #382924 !important;
                                                    }

                                                    .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap-text input, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap-captcha input, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap-password input, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap select, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap textarea {
                                                        width: 100%;
                                                        max-width: 100% !important;
                                                    }

                                                    .iphorm-elements.iphorm-elements-67 .iphorm-group-wrap, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap-text, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap-captcha, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap-password, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap {
                                                        width: 100% !important;
                                                        max-width: 100% !important;
                                                    }

                                                    .iphorm-submit-wrap-67 .iphorm-submit-input-wrap {
                                                        width: 100% !important;
                                                        max-width: 100% !important;
                                                    }

                                                    .iphorm-submit-wrap-67 .iphorm-submit-input-wrap button {
                                                        margin: 0;
                                                    }

                                                    .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap-text input, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap-captcha input, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap-password input, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap select, .iphorm-elements.iphorm-elements-67 .iphorm-element-wrap textarea {
                                                        border-radius: 0px;
                                                    }

                                                    .iphorm-elements-67 .iphorm-element-wrap-text input,
                                                    .iphorm-elements-67 .iphorm-element-wrap-captcha input,
                                                    .iphorm-elements-67 .iphorm-element-wrap-password input,
                                                    .iphorm-elements-67 .iphorm-input-wrap-select select,
                                                    .iphorm-elements-67 .iphorm-element-wrap select, .iphorm-elements .iphorm-element-wrap textarea {
                                                        box-shadow: none !important;
                                                    }

                                                    .iphorm-elements-67 .iphorm-element-wrap-text input,
                                                    .iphorm-elements-67 .iphorm-element-wrap-captcha input,
                                                    .iphorm-elements-67 .iphorm-element-wrap-password input,
                                                    .iphorm-elements-67 .iphorm-input-wrap-select select,
                                                    .iphorm-elements-67 .iphorm-element-wrap select,
                                                    .iphorm-elements-67 .iphorm-input-wrap-select select,
                                                    .iphorm-elements-67 .iphorm-element-wrap textarea {
                                                        background-color: rgba(255, 255, 255, 1) !important;
                                                    }

                                                    .iphorm-elements-67 .iphorm-element-wrap-text input,
                                                    .iphorm-elements-67 .iphorm-element-wrap-captcha input,
                                                    .iphorm-elements-67 .iphorm-element-wrap-password input,
                                                    .iphorm-elements-67 .iphorm-input-wrap-select select,
                                                    .iphorm-elements-67 .iphorm-element-wrap select,
                                                    .iphorm-elements-67 .iphorm-input-wrap-select select,
                                                    .iphorm-elements-67 .iphorm-element-wrap textarea,
                                                    .iphorm-elements-67 .iphorm-input-li > label,
                                                    .iphorm-elements-67 .iphorm-datepicker-icon,
                                                    .iphorm-elements-67 .iphorm-input-wrap > .iphorm-element-file-inner > div.uploader > span {
                                                        color: #737173;
                                                    }

                                                    .iphorm-elements-67 .iphorm-input-li > label {
                                                        font-size: 14px;
                                                    }

                                                    .iphorm-elements-67 .iphorm-element-spacer {
                                                        padding-bottom: 10px !important;
                                                    }

                                                    .iphorm-elements-67 .iphorm-element-wrap-text input,
                                                    .iphorm-elements-67 .iphorm-element-wrap-captcha input,
                                                    .iphorm-elements-67 .iphorm-element-wrap-password input,
                                                    .iphorm-elements-67 .iphorm-input-wrap-select select,
                                                    .iphorm-elements-67 .iphorm-element-wrap select,
                                                    .iphorm-elements-67 .iphorm-input-wrap-select select,
                                                    .iphorm-elements-67 .iphorm-element-wrap textarea {
                                                        border-width: 1px !important;
                                                        border-color: #382924 !important;

                                                    }

                                                    .iphorm-elements-67 .iphorm-submit-input-wrap {
                                                        float: none !important;
                                                        text-align: left !important;
                                                    }</style>
                                            </div>

                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-5-5d73575ab5e2e524307 > .column_inner {
                            padding-left: 40px;
                            padding-right: 40px;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-5-5d73575ab5e2e524307 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-5-5d73575ab5e2e524307 > .column_inner {
                            margin: 0 auto 0 !important;
                            min-height: 0 !important;
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 39px;
                            padding-bottom: 0;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-5-5d73575ab5e2e524307 {
                        }

                        .qfy-column-5-5d73575ab5e2e524307 > .column_inner > .background-overlay, .qfy-column-5-5d73575ab5e2e524307 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="" data-delay=""
                     class=" qfy-column-6-5d73575abb0e5474109 qfy-column-inner  vc_span_class  vc_span4  text-default small-screen-undefined notfullrow"
                     data-dw="1/3" data-fixheight="">
                    <div style=";position:relative;" class="column_inner ">
                        <div class=" background-overlay grid-overlay-"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_img_5d73575abb900865"
                                 style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_single_image_gfkr6" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element bitImageControlDiv ">
                                <style>@media only screen and (max-width: 768px) {
                                        .single_image_text-5d73575abb9da162 .head {
                                            font-size: 16px !important;
                                        }

                                        .single_image_text-5d73575abb9da162 .content {
                                            font-size: 16px !important;
                                        }
                                    }</style>
                                <a class="bitImageAhover  ">
                                    <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_center">
                                        <div class="qfe_wrapper"><span></span><img
                                                    width="360" height="360"
                                                    src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi9hMjAyN2Y0ZDEyNzliNDI1Mjc5MGJkMjhlNGIwNzlmOS5qcGc_p_p100_p_3D.jpg"
                                                    class="front_image  attachment-large"
                                                    alt="contacts-4" title=""
                                                    description=""
                                                    data-title="contacts-4" src-img=""
                                                    style=""></div>
                                    </div>
                                </a></div>
                            <a class="bitImageAhover  "> </a></div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-6-5d73575abb0e5474109 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 4px;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-6-5d73575abb0e5474109 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-6-5d73575abb0e5474109 > .column_inner {
                            margin: 0 auto 0 !important;
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 4px;
                            padding-bottom:;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-6-5d73575abb0e5474109 {
                        }

                        .qfy-column-6-5d73575abb0e5474109 > .column_inner > .background-overlay, .qfy-column-6-5d73575abb0e5474109 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div class="wf-mobile-hidden qfy-clumn-clear"
                     style="clear:both;"></div>
            </div>
        </div>

    </section>
    <section data-fixheight=""
             class="qfy-row-5-5d73575abbbea367024 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
             id="bit_g8gib"
             style="margin-bottom:0;border-radius:0px;color:#382924;">
        <style class="row_class qfy_style_class">
            @media only screen and (min-width: 992px) {
                section.section.qfy-row-5-5d73575abbbea367024 {
                    padding-left: 0;
                    padding-right: 0;
                    padding-top: 0;
                    padding-bottom: 0;
                    margin-top: 0;
                }
            }

            @media only screen and (max-width: 992px) {
                .bit-html section.section.qfy-row-5-5d73575abbbea367024 {
                    padding-left: 15px;
                    padding-right: 15px;
                    padding-top: 0;
                    padding-bottom: 0;
                    margin-top: 0;
                    min-height: 0;
                }
            }
        </style>
        <div class="section-background-overlay background-overlay grid-overlay-0 "
             style="background-color: #ffffff;"></div>



    </section>
@endsection