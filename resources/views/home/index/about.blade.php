@extends('layouts.yzh')
@section('header')

@endsection
@section('page_title', '关于我们')
@section('content')
    @include('./layouts/yzh/back')
    <section data-fixheight=""
             class="qfy-row-4-5d734f97455eb374886 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
             id="bit_13qrs"
             style="margin-bottom:0;border-radius:0px;border-top:0px solid rgba(255,255,255,1);border-bottom:1px solid rgba(232,232,232,1);border-left:0px solid rgba(255,255,255,1);border-right:0px solid rgba(255,255,255,1);color:#382924;">
        <style class="row_class qfy_style_class">
            @media only screen and (min-width: 992px) {
                section.section.qfy-row-4-5d734f97455eb374886 {
                    padding-left: 0;
                    padding-right: 0;
                    padding-top: 10vh;
                    padding-bottom: 10vh;
                    margin-top: 0;
                }

                section.section.qfy-row-4-5d734f97455eb374886 > .container {
                    max-width: 1600px;
                    margin: 0 auto;
                }
            }

            @media only screen and (max-width: 992px) {
                .bit-html section.section.qfy-row-4-5d734f97455eb374886 {
                    padding-left: 15px;
                    padding-right: 15px;
                    padding-top: 40px;
                    padding-bottom: 40px;
                    margin-top: 0;
                    min-height: 0;
                }
            }
        </style>
        <div class="section-background-overlay background-overlay grid-overlay-0 "
             style="background-color: #ffffff;"></div>

        <div class="container">
            <div class="row qfe_row">
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-4-5d734f9745a46110637 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-undefined notfullrow"
                     data-dw="1/3" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:5vw;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d734f9746118722"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0" qfyuuid="qfy_header_fwnoy"
                                 data-anitime="0.7" data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>@font-face {
                                        font-family: "f9c2524dc0f631cd4641ad47985a6c2d2";
                                        src: url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.eot"); /* IE9 */
                                        src: url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */ url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.woff") format("woff"), /* chrome, firefox */ url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.ttf") format("truetype"), /* chrome, firefox, opera, Safari, Android, iOS 4.2+ */ url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.svg#f9c2524dc0f631cd4641ad47985a6c2d2") format("svg"); /* iOS 4.1- */
                                        font-style: normal;
                                        font-weight: normal;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f9746118722 .header_title {
                                            font-size: 14px !important;
                                        }
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f9746118722 .header_subtitle {
                                            font-size: 30px !important;
                                        }
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="qfy_title left mobileleft">
                                        <div class="qfy_title_image_warpper"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="qfy_title_inner"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="header_title"
                                                     style="font-size:14px;font-weight:normal;font-style:normal;color:#c5a862;display:block;vertical-align:bottom;">

                                                </div>
                                                <div class="header_subtitle"
                                                     style="font-family:f9c2524dc0f631cd4641ad47985a6c2d2;font-size:36px;font-weight:bold;font-style:normal;color:#382924;display:block;vertical-align:bottom;">
                                                    {{ $info->title }}
                                                </div>
                                            </div>
                                            <style></style>
                                            <div class="button_wrapper"
                                                 style="display:none;">
                                                <div class="button "
                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                    + 查看更多
                                                </div>
                                            </div>
                                            <span style="clear:both;"></span></div>
                                    </div>
                                </div>
                            </div>
                            <div m-padding="19px 0px 0px 0px" p-padding="19px 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_column_text_qsxkq" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element qfy-text qfy-text-91060 qfe_text_column qfe_content_element  mobile_fontsize "
                                 style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:19px;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                <div class="qfe_wrapper">
                                    <div style="line-height: 28px;"><span
                                                style="font-size: 15px;">


{!! htmlspecialchars_decode($info->content) !!}</span>
                                    </div>
                                </div>
                            </div>
                            <div id="qfy-btn-5d734f9746b594881"
                                 style="margin-top:20px;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0" qfyuuid="qfy_btn_lpy3l"
                                 data-anitime="0.7" data-ani_iteration_count="1"
                                 class="qfy-element vc_btn3-container vc_btn3-left">
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-4-5d734f9745a46110637 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 30px;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-4-5d734f9745a46110637 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-4-5d734f9745a46110637 > .column_inner {
                            margin: 0 auto 0 !important;
                            min-height: 0 !important;
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-4-5d734f9745a46110637 {
                        }

                        .qfy-column-4-5d734f9745a46110637 > .column_inner > .background-overlay, .qfy-column-4-5d734f9745a46110637 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div data-animaleinbegin="bottom-in-view"
                     data-animalename="qfyfadeInUp" data-duration="" data-delay=""
                     class=" qfy-column-5-5d734f9746d0761306 qfy-column-inner  vc_span_class  vc_span8  text-default small-screen-default notfullrow"
                     data-dw="2/3" data-fixheight="">
                    <div style=";position:relative;" class="column_inner ">
                        <div class=" background-overlay grid-overlay-"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_img_5d734f97478dc125"
                                 style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_single_image_rwlys" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element bitImageControlDiv ">
                                <style>@media only screen and (max-width: 768px) {
                                        .single_image_text-5d734f97479fc283 .head {
                                            font-size: 16px !important;
                                        }

                                        .single_image_text-5d734f97479fc283 .content {
                                            font-size: 16px !important;
                                        }
                                    }</style>
                                <a class="bitImageAhover  ">
                                    <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_center">
                                        <div class="qfe_wrapper"><span></span><img
                                                    width="1024" height="565"
                                                    src="{{ $info->cover }}"
                                                    class="front_image  attachment-large"
                                                    alt="magazin-1" title=""
                                                    description=""
                                                    data-title="magazin-1" src-img=""
                                                    style=""></div>
                                    </div>
                                </a></div>
                            <a class="bitImageAhover  "> </a></div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-5-5d734f9746d0761306 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-5-5d734f9746d0761306 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-5-5d734f9746d0761306 > .column_inner {
                            margin: 0 auto 0 !important;
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 26px;
                            padding-bottom:;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-5-5d734f9746d0761306 {
                        }

                        .qfy-column-5-5d734f9746d0761306 > .column_inner > .background-overlay, .qfy-column-5-5d734f9746d0761306 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div class="wf-mobile-hidden qfy-clumn-clear"
                     style="clear:both;"></div>
            </div>
        </div>

    </section>
    <section data-fixheight=""
             class="qfy-row-5-5d734f9747c56279643 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
             id="bit_vbfsg"
             style="margin-bottom:0;border-radius:0px;color:#382924;">
        <style class="row_class qfy_style_class">
            @media only screen and (min-width: 992px) {
                section.section.qfy-row-5-5d734f9747c56279643 {
                    padding-left: 0;
                    padding-right: 0;
                    padding-top: 40px;
                    padding-bottom: 40px;
                    margin-top: 0;
                }

                section.section.qfy-row-5-5d734f9747c56279643 > .container {
                    max-width: 1600px;
                    margin: 0 auto;
                }
            }

            @media only screen and (max-width: 992px) {
                .bit-html section.section.qfy-row-5-5d734f9747c56279643 {
                    padding-left: 15px;
                    padding-right: 15px;
                    padding-top: 10px;
                    padding-bottom: 20px;
                    margin-top: 0;
                    min-height: 0;
                }
            }
        </style>
        <div class="section-background-overlay background-overlay grid-overlay-0 "
             style="background-color: #ffffff;"></div>

        <div class="container">
            <div class="row qfe_row">
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-6-5d734f974805f514649 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                     data-dw="1/6" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d734f9748766309"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_header_hy33n-c-x4" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>#vc_header_5d734f9748766309 .qfy_title_inner, #vc_header_5d734f9748766309 .qfy_title_image_inner {
                                        vertical-align: middle !important;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f9748766309 .header_title {
                                            font-size: 18px !important;
                                        }
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f9748766309 .header_subtitle {
                                            font-size: 12px !important;
                                        }
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="qfy_title left mobileleft">
                                        <div class="qfy_title_image_warpper"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="qfy_title_image_inner"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="header_image "
                                                     style="padding:5px 0 0 0;width:50px;">
                                                    <img class=" ag_image"
                                                         src="/images/yzh//aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi9hOTk2ZDI1MzhiOTgzYmI2ODdiMjlhMzk4NWFjNGM3NC01MHg1MC5wbmc_p_p100_p_3D.png"
                                                         alt="about (3)"
                                                         description=""
                                                         data-attach-id="14881"
                                                         data-title="about (3)"
                                                         title="" src-img=""></div>
                                            </div>
                                            <div class="qfy_title_inner"
                                                 style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                <div class="header_title"
                                                     style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                    精工细作
                                                </div>
                                                <div class="header_subtitle"
                                                     style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                    网站建设网站运营管理
                                                </div>
                                            </div>
                                            <style></style>
                                            <div class="button_wrapper"
                                                 style="display:none;">
                                                <div class="button "
                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                    + 查看更多
                                                </div>
                                            </div>
                                            <span style="clear:both;"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-6-5d734f974805f514649 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-6-5d734f974805f514649 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-6-5d734f974805f514649 > .column_inner {
                            margin: 15px auto 0 !important;
                            min-height: 0 !important;
                            padding-left: 5px;
                            padding-right: 5px;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-6-5d734f974805f514649 {
                        }

                        .qfy-column-6-5d734f974805f514649 > .column_inner > .background-overlay, .qfy-column-6-5d734f974805f514649 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-7-5d734f9748f65270443 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                     data-dw="1/6" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d734f974963e859"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_header_hy33n-c-x4" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>#vc_header_5d734f974963e859 .qfy_title_inner, #vc_header_5d734f974963e859 .qfy_title_image_inner {
                                        vertical-align: middle !important;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974963e859 .header_title {
                                            font-size: 18px !important;
                                        }
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974963e859 .header_subtitle {
                                            font-size: 12px !important;
                                        }
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="qfy_title left mobileleft">
                                        <div class="qfy_title_image_warpper"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="qfy_title_image_inner"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="header_image "
                                                     style="padding:5px 0 0 0;width:50px;">
                                                    <img class=" ag_image"
                                                         src="/images/yzh//aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi84NmU1YjE2ZWJkNjNjMGNiZmVjZmZlNjNjNmJhNDI1Ni01MHg1MC5wbmc_p_p100_p_3D.png"
                                                         alt="about (5)"
                                                         description=""
                                                         data-attach-id="14883"
                                                         data-title="about (5)"
                                                         title="" src-img=""></div>
                                            </div>
                                            <div class="qfy_title_inner"
                                                 style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                <div class="header_title"
                                                     style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                    质感于室
                                                </div>
                                                <div class="header_subtitle"
                                                     style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                    网站建设网站运营管理
                                                </div>
                                            </div>
                                            <style></style>
                                            <div class="button_wrapper"
                                                 style="display:none;">
                                                <div class="button "
                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                    + 查看更多
                                                </div>
                                            </div>
                                            <span style="clear:both;"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-7-5d734f9748f65270443 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-7-5d734f9748f65270443 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-7-5d734f9748f65270443 > .column_inner {
                            margin: 15px auto 0 !important;
                            min-height: 0 !important;
                            padding-left: 5px;
                            padding-right: 5px;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-7-5d734f9748f65270443 {
                        }

                        .qfy-column-7-5d734f9748f65270443 > .column_inner > .background-overlay, .qfy-column-7-5d734f9748f65270443 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div class="wf-mobile-visible qfy-clumn-clear"
                     style="clear:both;"></div>
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-8-5d734f9749e2e175300 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                     data-dw="1/6" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d734f974a575763"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_header_hy33n-c-x4" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>#vc_header_5d734f974a575763 .qfy_title_inner, #vc_header_5d734f974a575763 .qfy_title_image_inner {
                                        vertical-align: middle !important;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974a575763 .header_title {
                                            font-size: 18px !important;
                                        }
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974a575763 .header_subtitle {
                                            font-size: 12px !important;
                                        }
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="qfy_title left mobileleft">
                                        <div class="qfy_title_image_warpper"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="qfy_title_image_inner"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="header_image "
                                                     style="padding:5px 0 0 0;width:50px;">
                                                    <img class=" ag_image"
                                                         src="/images/yzh//aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi9hNzY0MDlhODc1MWIwMWIwYjM4ZjMzNmJkYjE5N2IzMi01MHg1MC5wbmc_p_p100_p_3D.png"
                                                         alt="about (1)"
                                                         description=""
                                                         data-attach-id="14911"
                                                         data-title="about (1)"
                                                         title="" src-img=""></div>
                                            </div>
                                            <div class="qfy_title_inner"
                                                 style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                <div class="header_title"
                                                     style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                    安全耐用
                                                </div>
                                                <div class="header_subtitle"
                                                     style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                    网站建设网站运营管理
                                                </div>
                                            </div>
                                            <style></style>
                                            <div class="button_wrapper"
                                                 style="display:none;">
                                                <div class="button "
                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                    + 查看更多
                                                </div>
                                            </div>
                                            <span style="clear:both;"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-8-5d734f9749e2e175300 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-8-5d734f9749e2e175300 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-8-5d734f9749e2e175300 > .column_inner {
                            margin: 15px auto 0 !important;
                            min-height: 0 !important;
                            padding-left: 5px;
                            padding-right: 5px;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-8-5d734f9749e2e175300 {
                        }

                        .qfy-column-8-5d734f9749e2e175300 > .column_inner > .background-overlay, .qfy-column-8-5d734f9749e2e175300 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-9-5d734f974adef871532 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                     data-dw="1/6" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d734f974b4de645"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_header_hy33n-c-x4" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>#vc_header_5d734f974b4de645 .qfy_title_inner, #vc_header_5d734f974b4de645 .qfy_title_image_inner {
                                        vertical-align: middle !important;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974b4de645 .header_title {
                                            font-size: 18px !important;
                                        }
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974b4de645 .header_subtitle {
                                            font-size: 12px !important;
                                        }
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="qfy_title left mobileleft">
                                        <div class="qfy_title_image_warpper"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="qfy_title_image_inner"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="header_image "
                                                     style="padding:5px 0 0 0;width:50px;">
                                                    <img class=" ag_image"
                                                         src="/images/yzh//aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi83YWM4MDcwMjY0Y2FmOTdjM2U3YWM0MTU5NmU2ZDY4Ni01MHg1MC5wbmc_p_p100_p_3D.png"
                                                         alt="about (4)"
                                                         description=""
                                                         data-attach-id="14882"
                                                         data-title="about (4)"
                                                         title="" src-img=""></div>
                                            </div>
                                            <div class="qfy_title_inner"
                                                 style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                <div class="header_title"
                                                     style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                    节能环保
                                                </div>
                                                <div class="header_subtitle"
                                                     style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                    网站建设网站运营管理
                                                </div>
                                            </div>
                                            <style></style>
                                            <div class="button_wrapper"
                                                 style="display:none;">
                                                <div class="button "
                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                    + 查看更多
                                                </div>
                                            </div>
                                            <span style="clear:both;"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-9-5d734f974adef871532 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-9-5d734f974adef871532 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-9-5d734f974adef871532 > .column_inner {
                            margin: 15px auto 0 !important;
                            min-height: 0 !important;
                            padding-left: 5px;
                            padding-right: 5px;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-9-5d734f974adef871532 {
                        }

                        .qfy-column-9-5d734f974adef871532 > .column_inner > .background-overlay, .qfy-column-9-5d734f974adef871532 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div class="wf-mobile-visible qfy-clumn-clear"
                     style="clear:both;"></div>
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-10-5d734f974bdb0548612 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                     data-dw="1/6" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d734f974c4c8803"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_header_hy33n-c-x4" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>#vc_header_5d734f974c4c8803 .qfy_title_inner, #vc_header_5d734f974c4c8803 .qfy_title_image_inner {
                                        vertical-align: middle !important;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974c4c8803 .header_title {
                                            font-size: 18px !important;
                                        }
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974c4c8803 .header_subtitle {
                                            font-size: 12px !important;
                                        }
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="qfy_title left mobileleft">
                                        <div class="qfy_title_image_warpper"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="qfy_title_image_inner"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="header_image "
                                                     style="padding:5px 0 0 0;width:50px;">
                                                    <img class=" ag_image"
                                                         src="/images/yzh//aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi8yZGJiNGU0MTY5MzgyZDYwOTFmMDAyOGU4ZWEyYjNiNy01MHg1MC5wbmc_p_p100_p_3D.png"
                                                         alt="about (2)"
                                                         description=""
                                                         data-attach-id="14912"
                                                         data-title="about (2)"
                                                         title="" src-img=""></div>
                                            </div>
                                            <div class="qfy_title_inner"
                                                 style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                <div class="header_title"
                                                     style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                    静音密封
                                                </div>
                                                <div class="header_subtitle"
                                                     style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                    网站建设网站运营管理
                                                </div>
                                            </div>
                                            <style></style>
                                            <div class="button_wrapper"
                                                 style="display:none;">
                                                <div class="button "
                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                    + 查看更多
                                                </div>
                                            </div>
                                            <span style="clear:both;"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-10-5d734f974bdb0548612 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-10-5d734f974bdb0548612 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-10-5d734f974bdb0548612 > .column_inner {
                            margin: 15px auto 0 !important;
                            min-height: 0 !important;
                            padding-left: 5px;
                            padding-right: 5px;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-10-5d734f974bdb0548612 {
                        }

                        .qfy-column-10-5d734f974bdb0548612 > .column_inner > .background-overlay, .qfy-column-10-5d734f974bdb0548612 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="1" data-delay=""
                     class=" qfy-column-11-5d734f974d16f687653 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                     data-dw="1/6" data-fixheight="">
                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                         class="column_inner ">
                        <div class=" background-overlay grid-overlay-0"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="vc_header_5d734f974d868364"
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_header_hy33n-c-x4" data-anitime="0.7"
                                 data-ani_iteration_count="1"
                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>#vc_header_5d734f974d868364 .qfy_title_inner, #vc_header_5d734f974d868364 .qfy_title_image_inner {
                                        vertical-align: middle !important;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974d868364 .header_title {
                                            font-size: 18px !important;
                                        }
                                    }

                                    @media only screen and (max-width: 768px) {
                                        #vc_header_5d734f974d868364 .header_subtitle {
                                            font-size: 12px !important;
                                        }
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="qfy_title left mobileleft">
                                        <div class="qfy_title_image_warpper"
                                             style="display:inline-block;position:relative;max-width:100%;">
                                            <div class="qfy_title_image_inner"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="header_image "
                                                     style="padding:5px 0 0 0;width:50px;">
                                                    <img class=" ag_image"
                                                         src="/images/yzh//aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi85MTA1MWVmNDE2Yzg5YmI4MzhkYjNkZmFkMDUyY2Y2ZC01MHg1MC5wbmc_p_p100_p_3D.png"
                                                         alt="about (6)"
                                                         description=""
                                                         data-attach-id="14884"
                                                         data-title="about (6)"
                                                         title="" src-img=""></div>
                                            </div>
                                            <div class="qfy_title_inner"
                                                 style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                <div class="header_title"
                                                     style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                    美观牢固
                                                </div>
                                                <div class="header_subtitle"
                                                     style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                    网站建设网站运营管理
                                                </div>
                                            </div>
                                            <style></style>
                                            <div class="button_wrapper"
                                                 style="display:none;">
                                                <div class="button "
                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                    + 查看更多
                                                </div>
                                            </div>
                                            <span style="clear:both;"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-11-5d734f974d16f687653 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-11-5d734f974d16f687653 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-11-5d734f974d16f687653 > .column_inner {
                            margin: 15px auto 0 !important;
                            min-height: 0 !important;
                            padding-left: 5px;
                            padding-right: 5px;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-11-5d734f974d16f687653 {
                        }

                        .qfy-column-11-5d734f974d16f687653 > .column_inner > .background-overlay, .qfy-column-11-5d734f974d16f687653 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
                <div class="wf-mobile-hidden qfy-clumn-clear"
                     style="clear:both;"></div>
                <div class="wf-mobile-visible qfy-clumn-clear"
                     style="clear:both;"></div>
            </div>
        </div>

    </section>
@endsection