<!DOCTYPE html>
<!-- saved from url=(0037)http://www.yzhpps.com/ -->
<html class="bit-html mobile-false js_active  vc_desktop  vc_transform  vc_transform  iphorm-js no-touch" dir="ltr"
      lang="zh-CN"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- for 360 -->
    <meta name="renderer" content="webkit">
    <meta name="applicable-device" content="pc,mobile"> <!-- for baidu -->
    <meta http-equiv="Cache-Control" content="no-transform"> <!-- for baidu -->
    <meta name="MobileOptimized" content="width"><!-- for baidu -->
    <meta name="HandheldFriendly" content="true"><!-- for baidu -->
    <!-- start of customer header -->
    <!-- end of customer header -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{ $configs['seo_title'] }} | {{ $title??$configs['seo_title'] }}</title>
    <meta name="keywords" content="{{ $configs['site_seo_keywords'] }}">
    <meta name="description" content="{{ $configs['site_seo_description'] }}">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="http://www.yzhpps.com/xmlrpc.php">
    <!--[if lt IE 9]>
    <script src="http://www.yzhpps.com/FeiEditor/bitSite/js/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://www.yzhpps.com/FeiEditor/bitSite/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css" id="static-stylesheet"></style>
    <link rel="stylesheet" href="{{ asset('css/yzh/yzh.css') }}" type="text/css" media="all">
    <!-- Cache! -->
    <style type="text/css">
        #header .wf-wrap {

            max-width: 1520px;
            margin: 0 auto;
        }

    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-7-5d735f3f8e3b766954 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-7-5d735f3f8e3b766954 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-7-5d735f3f8e3b766954 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 5px;
                padding-right: nanpx;
                padding-top: 0;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-7-5d735f3f8e3b766954 {
            }

            .qfy-column-7-5d735f3f8e3b766954 > .column_inner > .background-overlay, .qfy-column-7-5d735f3f8e3b766954 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style>
        @media only screen and (max-width: 768px) {
            #vc_header_5d735f3f9cd8a257 .header_title {
                font-size: 20px !important;
            }
        }

        @media only screen and (max-width: 768px) {
            #vc_header_5d735f3f9cd8a257 .header_subtitle {
                font-size: 12px !important;
            }
        }
    </style>
    <script type="text/javascript" async="" defer="" src="{{ asset('js/yzh/matomo.js') }}"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        /* dt-plugins */
        var dtLocal = {
            "passText": "\u67e5\u770b\u8fd9\u4e2a\u52a0\u5bc6\u8d44\u8baf\uff0c\u8bf7\u5728\u4e0b\u9762\u8f93\u5165\u5bc6\u7801\uff1a",
            "moreButtonAllLoadedText": "\u5168\u90e8\u5df2\u52a0\u8f7d",
            "postID": "11965",
            "ajaxurl": "http:\/\/5d1c71c9850fc.t73.qifeiye.com\/admin\/admin-ajax.php",
            "contactNonce": "5f9425247e",
            "ajaxNonce": "8c35441399",
            "pageData": {"type": "page", "template": "page", "layout": null},
            "themeSettings": {"smoothScroll": "on"}
        };
        /* thickbox */
        var thickboxL10n = {
            "next": "\u4e0b\u4e00\u5f20 >",
            "prev": "< \u4e0a\u4e00\u5f20",
            "image": "\u56fe\u7247",
            "of": "\/",
            "close": "\u5173\u95ed",
            "noiframes": "This feature requires inline frames. You have iframes disabled or your browser does not support them.",
            "loadingAnimation": "\/\/fast.qifeiye.com\/FeiEditor\/bitSite\/images\/preloader.gif"
        };

        /* ]]> */
    </script>
    <script type="text/javascript" src="{{ asset('js/yzh/yzh.js') }}">/*Cache!*/</script>
    <script>var geURL = 'http://www.yzhpps.com/ge/index.html?embed=1&ui=embed&spin=1&modified=unsavedChanges&proto=json&lang=zh';</script>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-3-5d735f3f8c629822443 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 20vh;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-3-5d735f3f8c629822443 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-3-5d735f3f8c629822443 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-3-5d735f3f8c629822443 {
            }

            .qfy-column-3-5d735f3f8c629822443 > .column_inner > .background-overlay, .qfy-column-3-5d735f3f8c629822443 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="http://www.yzhpps.com/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="http://www.yzhpps.com/qfy-includes/wlwmanifest.xml">

    <link rel="canonical" href="http://www.yzhpps.com/">
    <link rel="shortlink" href="http://www.yzhpps.com/?p=11965">
    <style class="style_0">
        .bitRightSider .widget-title, .bitLeftSider .widget-title {
            padding: 0 0 0 10px;
            height: 28px;
            line-height: 28px;
            background-color: #024886;
            margin: 0px;
            font-family:;
            font-size: px;
            font-weight: normal;
            font-style: normal;
            text-decoration: none;
            color: #fff;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            background-image: url(//fast.qifeiye.com/qfy-content/plugins//bit-plugin/assets/frame/header_bg/1/bg.png);
            background-repeat: repeat;
            -webkit-border-top-left-radius: 0;
            -webkit-border-top-right-radius: 0;
            -moz-border-radius-topleft: 0;
            -moz-border-radius-topright: 0;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .bitRightSider .bitWidgetFrame, .bitLeftSider .bitWidgetFrame {
            border-top: 0;
            border-bottom: 1px solid #bababa;
            border-left: 1px solid #bababa;
            border-right: 1px solid #bababa;
            padding: 4px 10px 4px 10px;
            -webkit-border-bottom-left-radius: 0;
            -webkit-border-bottom-right-radius: 0;
            -moz-border-radius-bottomleft: 0;
            -moz-border-radius-bottomright: 0;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .bitRightSider .site_tooler, .bitLeftSider .site_tooler {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .bitRightLeftSiderWidget.site_tooler .bitWidgetFrame, .bitRightLeftSiderWidget.site_tooler .bitWidgetFrame {
            background-color: transparent;
            background-image: none;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }</style>
    <style class="style_0">.footer .widget-title {
            padding: 0 0 0 10px;
            height: 28px;
            line-height: 28px;
            background-color: #024886;
            margin: 0px;
            font-family:;
            font-size: px;
            font-weight: normal;
            font-style: normal;
            text-decoration: none;
            color: #fff;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            background-image: none;
            -webkit-border-top-left-radius: 4px;
            -webkit-border-top-right-radius: 4px;
            -moz-border-radius-topleft: 4px;
            -moz-border-radius-topright: 4px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
        }

        .footer .widget-title {
            border-top: 0;
            border-left: 0;
            border-right: 0;
        }

        .footer .bitWidgetFrame {
            border-bottom: 0;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            padding: 4px 10px 4px 10px;
        }

        .footer .site_tooler {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .footer .site_tooler .bitWidgetFrame {
            background-color: transparent;
            background-image: none;
            -webkit-border-bottom-left-radius: 4px;
            border-bottom-left-radius: 4px;
            -webkit-border-bottom-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-15-5d735f3f960cb832458 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 30px;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-15-5d735f3f960cb832458 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-15-5d735f3f960cb832458 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-15-5d735f3f960cb832458 {
            }

            .qfy-column-15-5d735f3f960cb832458 > .column_inner > .background-overlay, .qfy-column-15-5d735f3f960cb832458 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }</style>
    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
            .qfy-column-2-5d735f3f80562401992 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-2-5d735f3f80562401992 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-2-5d735f3f80562401992 > .column_inner {
                margin: 0 auto 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top:;
                padding-bottom:;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-2-5d735f3f80562401992 {
            }

            .qfy-column-2-5d735f3f80562401992 > .column_inner > .background-overlay, .qfy-column-2-5d735f3f80562401992 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="style_simplepage-3">
        #simplepage-3 .widget-title {
            padding: 0 0 0 10px;
            height: 28px;
            line-height: 28px;
            background-color: transparent;
            margin: 0px;
            font-family:;
            font-size: px;
            font-weight: normal;
            font-style: normal;
            text-decoration: none;
            color: #ffffff;
            border-top: 1px solid transparent;
            border-left: 1px solid transparent;
            border-right: 1px solid transparent;
            border-bottom: 0px solid transparent;
            background-image: none;
            -webkit-border-top-left-radius: 4px;
            -webkit-border-top-right-radius: 4px;
            -moz-border-radius-topleft: 4px;
            -moz-border-radius-topright: 4px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
        }

        #simplepage-3 .widget-title {
            border-top: 0;
            border-left: 0;
            border-right: 0;
        }

        #simplepage-3 .bitWidgetFrame {
            border-bottom: 0;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            padding: 4px 10px 4px 10px;
        }

        #simplepage-3 {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        #simplepage-3 .bitWidgetFrame {
            background-color: transparent;
            background-image: none;
            -webkit-border-bottom-left-radius: 4px;
            border-bottom-left-radius: 4px;
            -webkit-border-bottom-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }

        #simplepage-3 .bitWidgetFrame {
            padding-left: 0px;
            padding-right: 0px;
        }

        body #simplepage-3 .bitWidgetFrame {
            padding-top: 0px !important;
            padding-bottom: 0px !important;
        }</style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-6-5d735f3f8e17b561777 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-6-5d735f3f8e17b561777 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-6-5d735f3f8e17b561777 > .column_inner {
                margin: 0 auto 0 !important;
                padding-left: nanpx;
                padding-right: nanpx;
                padding-top:;
                padding-bottom:;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-6-5d735f3f8e17b561777 {
            }

            .qfy-column-6-5d735f3f8e17b561777 > .column_inner > .background-overlay, .qfy-column-6-5d735f3f8e17b561777 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-5-5d735f3f8d99d895814 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-5-5d735f3f8d99d895814 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-5-5d735f3f8d99d895814 > .column_inner {
                margin: 0 auto 0 !important;
                padding-left: nanpx;
                padding-right: nanpx;
                padding-top:;
                padding-bottom:;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-5-5d735f3f8d99d895814 {
            }

            .qfy-column-5-5d735f3f8d99d895814 > .column_inner > .background-overlay, .qfy-column-5-5d735f3f8d99d895814 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-4-5d735f3f8d28c562241 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-4-5d735f3f8d28c562241 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-4-5d735f3f8d28c562241 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: nanpx;
                padding-right: 5px;
                padding-top: 0;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-4-5d735f3f8d28c562241 {
            }

            .qfy-column-4-5d735f3f8d28c562241 > .column_inner > .background-overlay, .qfy-column-4-5d735f3f8d28c562241 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-9-5d735f3f90a7a339135 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 10vh;
                padding-bottom: 10vh;
            }

            .qfe_row .vc_span_class.qfy-column-9-5d735f3f90a7a339135 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-9-5d735f3f90a7a339135 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 20px;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-9-5d735f3f90a7a339135 {
            }

            .qfy-column-9-5d735f3f90a7a339135 > .column_inner > .background-overlay, .qfy-column-9-5d735f3f90a7a339135 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-10-5d735f3f919a9855820 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 10vh;
                padding-bottom: 10vh;
            }

            .qfe_row .vc_span_class.qfy-column-10-5d735f3f919a9855820 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-10-5d735f3f919a9855820 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 20px;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-10-5d735f3f919a9855820 {
            }

            .qfy-column-10-5d735f3f919a9855820 > .column_inner > .background-overlay, .qfy-column-10-5d735f3f919a9855820 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-11-5d735f3f92714376310 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 10vh;
                padding-bottom: 10vh;
            }

            .qfe_row .vc_span_class.qfy-column-11-5d735f3f92714376310 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-11-5d735f3f92714376310 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 20px;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-11-5d735f3f92714376310 {
            }

            .qfy-column-11-5d735f3f92714376310 > .column_inner > .background-overlay, .qfy-column-11-5d735f3f92714376310 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-12-5d735f3f934b4890920 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 10vh;
                padding-bottom: 10vh;
            }

            .qfe_row .vc_span_class.qfy-column-12-5d735f3f934b4890920 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-12-5d735f3f934b4890920 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 20px;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-12-5d735f3f934b4890920 {
            }

            .qfy-column-12-5d735f3f934b4890920 > .column_inner > .background-overlay, .qfy-column-12-5d735f3f934b4890920 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-13-5d735f3f94273754790 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 10vh;
                padding-bottom: 10vh;
            }

            .qfe_row .vc_span_class.qfy-column-13-5d735f3f94273754790 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-13-5d735f3f94273754790 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 20px;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-13-5d735f3f94273754790 {
            }

            .qfy-column-13-5d735f3f94273754790 > .column_inner > .background-overlay, .qfy-column-13-5d735f3f94273754790 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class"
    >@media only screen and (min-width: 992px) {
            .qfy-column-16-5d735f3f96e6d288843 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-16-5d735f3f96e6d288843 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-16-5d735f3f96e6d288843 > .column_inner {
                margin: 0 auto 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 28px;
                padding-bottom:;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-16-5d735f3f96e6d288843 {
            }

            .qfy-column-16-5d735f3f96e6d288843 > .column_inner > .background-overlay, .qfy-column-16-5d735f3f96e6d288843 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            .qfy-column-18-5d735f3f98718252069 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-18-5d735f3f98718252069 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-18-5d735f3f98718252069 > .column_inner {
                margin: 15px auto 0 !important;
                min-height: 0 !important;
                padding-left: 5px;
                padding-right: 5px;
                padding-top: 0;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-18-5d735f3f98718252069 {
            }

            .qfy-column-18-5d735f3f98718252069 > .column_inner > .background-overlay, .qfy-column-18-5d735f3f98718252069 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
    </style>
    <script type="text/javascript">
        var qfyuser_ajax_url = 'http://www.yzhpps.com/admin/admin-ajax.php';
    </script>
    <script type="text/javascript">
        var qfyuser_upload_url = 'http://www.yzhpps.com/qfy-content/plugins/qfyuser/lib/fileupload/fileupload.php?upload_nonce=a0522fba95';
    </script>
    <meta name="robots" content="index, follow">
    <meta name="description" content="首页">
    <style type="text/css">
        body {
            background-color: #ffffff;
            background-position: center top;
            background-repeat: repeat;
            background-size: cover;
            background-attachment: fixed;
        }

        #page {
            background-color: #ffffff;
            background-position: center top;
            background-repeat: repeat;
            background-size: auto;
            background-attachment: scroll;
        }
    </style>
    <script type="text/javascript">
        dtGlobals.logoEnabled = 1;
        dtGlobals.curr_id = '11965';
        dtGlobals.logoURL = '/images/yzh/logo.png';
        dtGlobals.logoW = '299';
        dtGlobals.logoH = '88';
        dtGlobals.qfyname = '起飞页';
        dtGlobals.id = '5d1c71c9850fc';
        dtGlobals.language = '';
        smartMenu = 1;
        document.cookie = 'resolution=' + Math.max(screen.width, screen.height) + '; path=/';
        dtGlobals.gallery_bgcolor = 'rgba(51,51,51,1)';
        dtGlobals.gallery_showthumbs = '0';
        dtGlobals.gallery_style = '';
        dtGlobals.gallery_autoplay = '0';
        dtGlobals.gallery_playspeed = '3';
        dtGlobals.gallery_imagesize = '100';
        dtGlobals.gallery_imageheight = '100';
        dtGlobals.gallery_stopbutton = '';
        dtGlobals.gallery_thumbsposition = '';
        dtGlobals.gallery_tcolor = '#fff';
        dtGlobals.gallery_tsize = '16';
        dtGlobals.gallery_dcolor = '#fff';
        dtGlobals.gallery_dsize = '14';
        dtGlobals.gallery_tfamily = '';
        dtGlobals.gallery_dfamily = '';
        dtGlobals.gallery_blankclose = '0';
        dtGlobals.gallery_arrowstyle = '0';
        dtGlobals.fm_showstyle = '';
        dtGlobals.fm_showspeed = '';
        dtGlobals.cdn_url = 'https://ccdn.goodq.top';
        dtGlobals.qfymodel = "";
        var socail_back_url = '';

    </script>
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            section.section.qfy-row-2-5d735f3f80336893590 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
                margin-top: 0;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-2-5d735f3f80336893590 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 0;
                padding-bottom: 0;
                margin-top: 0;
                min-height: 0;
            }
        }
    </style>
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            section.section.qfy-row-3-5d735f3f8c3ef878849 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 10vh;
                padding-bottom: 10vh;
                margin-top: 0;
            }

            section.section.qfy-row-3-5d735f3f8c3ef878849 > .container {
                max-width: 1600px;
                margin: 0 auto;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-3-5d735f3f8c3ef878849 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 40px;
                padding-bottom: 40px;
                margin-top: 0;
                min-height: 0;
            }
        }
    </style>
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            section.section.qfy-row-4-5d735f3f8cf8b114900 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
                margin-top: 0;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-4-5d735f3f8cf8b114900 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 0;
                padding-bottom:;
                margin-top: 0;
            }
        }
    </style>
    <link href="/js/yzh/swiper-4.1.0.min.css" rel="stylesheet" type="text/css">
</head>


<body class="home page page-id-11965 page-template-default image-blur widefull_header3 widefull_topbar3 btn-flat content-fullwidth qfe-js-composer js-comp-ver-4.0.1 vc_responsive is-webkit mobilefloatmenu"
      backgroundsize="true" data-pid="11965" data-pkey="15aa7899159a08f808a2fba99be2707f" style="">
<div class="dl-menuwrapper  wf-mobile-visible floatmenu floatwarpper fullfloatmenu center">
    <div class="dl-container">
        <ul data-st="1" data-sp="0" data-fh="0" data-mw="1600" data-lh="32" class="dl-menu"
            data-bit-menu="underline-hover" data-bit-float-menu="underline-hover">
            @component('./layouts/yzh/nav',['isMobile'=>'1'])
            @endcomponent
        </ul>
    </div>
</div>
<div id="mark_mask"
     style="display:none;position:fixed;top:40px;left:0;z-index:99999999;height:1000px;width:100%;background:rgba(0,0,0,0.4);"></div>
<script>
    var Random = function (min, max) {
        return Math.round(Math.random() * (max - min)) + min;
    }
    //apm normal access
    if (Random(1, 20) == 1) {
        !(function (c, b, d, a) {
            c[a] || (c[a] = {});
            c[a].config = {
                pid: "c9ihesw3jb@ccca834f41b7459",
                appType: "web",
                imgUrl: "https://arms-retcode.aliyuncs.com/r.png?"
            };
            with (b) with (body) with (insertBefore(createElement("script"), firstChild)) setAttribute("crossorigin", "", src = d)
        })(window, document, "https://retcode.alicdn.com/retcode/bl.js", "__bl");
    }
</script>


<div id="page" class=" wide ">
    <div id="dl-menu" class="dl-menuwrapper wf-mobile-visible floatmenu fullfloatmenu center"
         style="right: 8px; top: 8px;"><a data-padding="" data-top="8" data-right="8" rel="nofollow" id="mobile-menu"
                                          class="glyphicon glyphicon-icon-align-justify fullfloatmenu positionFixed iconbigSize center">
            <span class="menu-open  phone-text">首页</span>
            <span class="menu-close">关闭</span>
            <span class="menu-back">返回上一级</span>
            <span class="wf-phone-visible">&nbsp;</span>
        </a></div>


    <!-- left, center, classical, classic-centered -->
    <!-- !Header -->
    <header id="header" class="logo-left-right  mobiletopbottom  ht headerPM menuPosition" role="banner">
        <!-- class="overlap"; class="logo-left", class="logo-center", class="logo-classic" -->
        <div class="wf-wrap">
            <div class="wf-table">


                <div id="branding" class="wf-td bit-logo-bar" style="width:200px;">
                    <a class="bitem logo  fixedwidth small" style="display: table-cell;"
                       href="http://www.yzhpps.com/"><span class="logospan"><img class="preload-me"
                                                                                 src="{{ asset('images/yzh/logo.png') }}"
                                                                                 width="299" height="88"
                                                                                 alt="门窗企业"></span></a>

                    <!-- <div id="site-title" class="assistive-text"></div>
                    <div id="site-description" class="assistive-text"></div> -->
                </div>

                <!-- !- Navigation -->
                <nav style="min-height: auto;" id="navigation" class="wf-td" bitdataaction="site_menu_container"
                     bitdatalocation="primary">


                    <div id="dl-menu"
                         class="dl-menuwrapper wf-mobile-visible main-mobile-menu mobiledefault_containter center"
                         style="display: none; right: 8px; top: 8px;"></div>


                    <ul id="main-nav" data-st="1" data-sp="0" data-fh="0" data-mw="1600" data-lh="32"
                        class="mainmenu fancy-rollovers wf-mobile-hidden position-text-right text-right underline-hover bit-menu-default"
                        data-bit-menu="underline-hover" data-bit-float-menu="underline-hover">
                        @component('./layouts/yzh/nav',['isMobile'=>'0'])
                        @endcomponent
                    </ul>
                </nav>
                <div style="display:none;" id="main-nav-slide">

                </div>

                <div class="wf-td assistive-info    " role="complementary" style="">
                    <div class="top-bar-right right bit_widget_more" bitdatamarker="bitHeader-2"
                         bitdataaction="site_fix_container" bitdatacolor="white" style="">
                        <div id="simplepage-3" style="margin-top:0;margin-bottom:0;"
                             class="mobileHidden headerWidget_1 simplepage site_tooler">

                            <div class="simplepage_container bitWidgetFrame" data-post_id="15366">
                                <section data-fixheight=""
                                         class="qfy-row-2-5d735f3f80336893590 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                         id="bit_38bwq" style="margin-bottom:0;border-radius:0px;color:#382924;">
                                    <div class="section-background-overlay background-overlay grid-overlay-0 "
                                         style="background-color: #ffffff;"></div>

                                    <div class="container">
                                        <div class="row qfe_row">
                                            <div data-animaleinbegin="bottom-in-view" data-animalename="qfyfadeInUp"
                                                 data-duration="" data-delay=""
                                                 class=" qfy-column-2-5d735f3f80562401992 qfy-column-inner  vc_span12  text-default small-screen-default fullrow"
                                                 data-dw="1/1" data-fixheight="">
                                                <div style=";position:relative;" class="column_inner ">
                                                    <div class=" background-overlay grid-overlay-"
                                                         style="background-color:transparent;width:100%;"></div>
                                                    <div class="column_containter" style="z-index:3;position:relative;">
                                                        <div id="vc_header_5d735f3f80853851" m-padding="0px 0px 0px 0px"
                                                             p-padding="0 0 0 0" css_animation_delay="0"
                                                             qfyuuid="qfy_header_hy33n-c-y3" data-anitime="0.7"
                                                             data-ani_iteration_count="1"
                                                             class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                             style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                            <style>#vc_header_5d735f3f80853851 .qfy_title_inner, #vc_header_5d735f3f80853851 .qfy_title_image_inner {
                                                                    vertical-align: middle !important;
                                                                }

                                                                @media only screen and (max-width: 768px) {
                                                                    #vc_header_5d735f3f80853851 .header_title {
                                                                        font-size: 16px !important;
                                                                    }
                                                                }

                                                                @media only screen and (max-width: 768px) {
                                                                    #vc_header_5d735f3f80853851 .header_subtitle {
                                                                        font-size: 12px !important;
                                                                    }
                                                                }</style>
                                                            <div class="qfe_wrapper">
                                                                <div class="qfy_title left mobileleft">
                                                                    <div class="qfy_title_image_warpper"
                                                                         style="display:inline-block;position:relative;max-width:100%;">
                                                                        <div class="qfy_title_image_inner"
                                                                             style="display:inline-block;position:relative;max-width:100%;">
                                                                            <div class="header_image "
                                                                                 style="padding:5px 0 0 0;width:40px;">
                                                                                <img class=" ag_image"
                                                                                     src="{{ asset('images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi8yNDdhMTdlNmRkOGY1ZDE5YjU2YWFmNTQzYjBjMGJhOC00MHg0MC5qcGc_p_p100_p_3D.jpg') }}"
                                                                                     alt="top" description=""
                                                                                     data-attach-id="15369"
                                                                                     data-title="top" title=""
                                                                                     src-img=""></div>
                                                                        </div>
                                                                        <div class="qfy_title_inner"
                                                                             style="display:inline-block;position:relative;max-width:calc(100% - 40px);">
                                                                            <div class="header_title"
                                                                                 style="font-family:微软雅黑;font-size:16px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                +86（010）6296 296
                                                                            </div>
                                                                            <div class="header_subtitle"
                                                                                 style="font-size:12px;font-weight:normal;font-style:normal;color:#c5a862;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                周一-周六：早上10:00-下午2:00
                                                                            </div>
                                                                        </div>
                                                                        <style></style>
                                                                        <div class="button_wrapper"
                                                                             style="display:none;">
                                                                            <div class="button "
                                                                                 style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                + 查看更多
                                                                            </div>
                                                                        </div>
                                                                        <span style="clear:both;"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- #branding -->
        </div><!-- .wf-wrap -->
    </header><!-- #masthead -->

    <section class="bitBanner" id="bitBanner" bitdatamarker="bitBanner" bitdataaction="site_fix_container">
    </section>
    <div id="main" class="bit_main_content">


        <div class="main-gradient"></div>

        <div class="wf-wrap">
            <div class="wf-container-main">


                <div id="content" class="content" role="main">


                    <div class="main-outer-wrapper ">
                        <div class="bit_row">

                            <div class="twelve columns no-sidebar-content ">

                                <div class="bit_row">

                                    <div class="content-wrapper twelve columns ">
                                        <section data-fixheight=""
                                                 class="qfy-row-3-5d735f3f8c3ef878849 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_1so19"
                                                 style="margin-bottom:0;border-radius:0px;color:#382924;">

                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: #ffffff;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="1" data-delay=""
                                                         class=" qfy-column-3-5d735f3f8c629822443 qfy-column-inner  vc_span_class  vc_span6  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/2" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div m-padding="0px 0px 20px 0px" p-padding="0 0 20px 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_column_text_1hdlh" data-anitime="1"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-11735 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom mobile_fontsize30 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 0px 0px 20px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="letter-spacing: 3px; line-height: 50px;">
                                                                            <strong><span style="font-size: 60px;">高端隔音门窗<br></span></strong>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div m-padding="0px 0px 30px 0px" p-padding="0 0 30px 0"
                                                                     css_animation_delay="0.2"
                                                                     qfyuuid="qfy_column_text_gm8i1" data-anitime="1"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-6023 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.2 mobile_fontsize30 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 0px 0px 30px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="letter-spacing: 3px; line-height: 50px;">
                                                                            <span style="color: rgb(197, 168, 98);"><strong><span
                                                                                            style="font-size: 60px;">门 · 窗 · 生活 &nbsp;岂止于此 <br></span></strong></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div m-padding="0px 0px 40px 0px" p-padding="0 0 40px 0"
                                                                     css_animation_delay="0.4"
                                                                     qfyuuid="qfy_column_text_tnr7m" data-anitime="1"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-77915 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.4 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 0px 0px 40px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div data-ani_iteration_count="1"
                                                                             data-anitime="0.7"
                                                                             style="letter-spacing: 1px;"><span
                                                                                    style="font-size: 14px;">
 网站运营管理到网站推广的自动化免费建站平台，为中小微企业及个人提供免费的在线响应式建站服务</span></div>
                                                                        <div data-ani_iteration_count="1"
                                                                             data-anitime="0.7"
                                                                             style="letter-spacing: 1px;"><span
                                                                                    style="font-size: 14px;">满足企业品牌宣传个人形象展示产品展示营销推广数据收集。 </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <section data-fixheight=""
                                                                         class="qfy-row-4-5d735f3f8cf8b114900 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                                         style="margin-bottom:0;border-radius:0px;color:#000000;">

                                                                    <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                                         style="background-color: transparent;"></div>

                                                                    <div class="container">
                                                                        <div class="row qfe_row">
                                                                            <div data-animaleinbegin="90%"
                                                                                 data-animalename="qfyfadeInUp"
                                                                                 data-duration="1" data-delay=""
                                                                                 class=" qfy-column-4-5d735f3f8d28c562241 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span3  text-Default small-screen-Default notfullrow"
                                                                                 data-dw="1/4" data-fixheight="">
                                                                                <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                                                     class="column_inner ">
                                                                                    <div class=" background-overlay grid-overlay-0"
                                                                                         style="background-color:transparent;width:100%;"></div>
                                                                                    <div class="column_containter"
                                                                                         style="z-index:3;position:relative;">
                                                                                        <div id="qfy-btn-5d735f3f8d7d59111"
                                                                                             style="margin-top: 0px; margin-bottom: 0px; padding: 0px; animation-duration: 1s; animation-iteration-count: 1;"
                                                                                             m-padding="0px 0px 0px 0px"
                                                                                             p-padding="0 0 0 0"
                                                                                             css_animation_delay="0.5"
                                                                                             qfyuuid="qfy_btn_8kmgp-c-tf"
                                                                                             data-anitime="1"
                                                                                             data-ani_iteration_count="1"
                                                                                             class="qfy-element vc_btn3-container  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.5 vc_btn3-left qfe_start_animation">
                                                                                            <a onmouseleave="this.style.borderColor=&#39;#382924&#39;; this.style.backgroundColor=&#39;#382924&#39;; this.style.color=&#39;#fff&#39;;"
                                                                                               onmouseenter="this.style.backgroundColor=&#39;#382924&#39;; this.style.borderColor=&#39;#382924&#39;; this.style.color=&#39;#fff&#39;;"
                                                                                               style="font-size:15px; letter-spacing:1px;text-indent: 1px; padding-left:35px;padding-right:35px; padding-top:13px;padding-bottom:13px; border-width:2px; border-color:#382924; background-color:#382924; color:#fff;"
                                                                                               class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-rounded vc_btn3-style-qfy-custom"
                                                                                               href="/category/4"
                                                                                               target="">查看产品 →</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div data-animaleinbegin="90%"
                                                                                 data-animalename="qfyfadeInUp"
                                                                                 data-duration="" data-delay=""
                                                                                 class=" qfy-column-5-5d735f3f8d99d895814 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span3  text-default small-screen-default notfullrow"
                                                                                 data-dw="1/4" data-fixheight="">
                                                                                <div style=";position:relative;"
                                                                                     class="column_inner ">
                                                                                    <div class=" background-overlay grid-overlay-"
                                                                                         style="background-color:transparent;width:100%;"></div>
                                                                                    <div class="column_containter"
                                                                                         style="z-index:3;position:relative;">
                                                                                        <div id="qfy-btn-5d735f3f8dfba4476"
                                                                                             style="margin-top: 0px; margin-bottom: 0px; padding: 0px; animation-duration: 1s; animation-iteration-count: 1;"
                                                                                             m-padding="0px 0px 0px 0px"
                                                                                             p-padding="0 0 0 0"
                                                                                             css_animation_delay="0.6"
                                                                                             qfyuuid="qfy_btn_8kmgp"
                                                                                             data-anitime="1"
                                                                                             data-ani_iteration_count="1"
                                                                                             class="qfy-element vc_btn3-container  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.6 vc_btn3-left qfe_start_animation">
                                                                                            <a onmouseleave="this.style.borderColor=&#39;#c5a862&#39;; this.style.backgroundColor=&#39;#c5a862&#39;; this.style.color=&#39;#fff&#39;;"
                                                                                               onmouseenter="this.style.backgroundColor=&#39;#c5a862&#39;; this.style.borderColor=&#39;#c5a862&#39;; this.style.color=&#39;#fff&#39;;"
                                                                                               style="font-size:15px; letter-spacing:1px;text-indent: 1px; padding-left:35px;padding-right:35px; padding-top:13px;padding-bottom:13px; border-width:2px; border-color:#c5a862; background-color:#c5a862; color:#fff;"
                                                                                               class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-rounded vc_btn3-style-qfy-custom"
                                                                                               href="/contact"
                                                                                               target="">联系我们 →</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="wf-mobile-visible qfy-clumn-clear"
                                                                                 style="clear:both;"></div>
                                                                            <div data-animaleinbegin="90%"
                                                                                 data-animalename="qfyfadeInUp"
                                                                                 data-duration="" data-delay=""
                                                                                 class=" qfy-column-6-5d735f3f8e17b561777 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span3  text-default small-screen-default notfullrow"
                                                                                 data-dw="1/4" data-fixheight="">
                                                                                <div style=";position:relative;"
                                                                                     class="column_inner ">
                                                                                    <div class=" background-overlay grid-overlay-"
                                                                                         style="background-color:transparent;width:100%;"></div>
                                                                                    <div class="column_containter"
                                                                                         style="z-index:3;position:relative;"></div>
                                                                                </div>
                                                                            </div>

                                                                            <div data-animaleinbegin="bottom-in-view"
                                                                                 data-animalename="qfyfadeInUp"
                                                                                 data-duration="1" data-delay=""
                                                                                 class=" qfy-column-7-5d735f3f8e3b766954 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span3  text-Default small-screen-Default notfullrow"
                                                                                 data-dw="1/4" data-fixheight="">
                                                                                <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                                                     class="column_inner ">
                                                                                    <div class=" background-overlay grid-overlay-0"
                                                                                         style="background-color:transparent;width:100%;"></div>
                                                                                    <div class="column_containter"
                                                                                         style="z-index:3;position:relative;"></div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="wf-mobile-hidden qfy-clumn-clear"
                                                                                 style="clear:both;"></div>
                                                                            <div class="wf-mobile-visible qfy-clumn-clear"
                                                                                 style="clear:both;"></div>
                                                                        </div>
                                                                    </div>

                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="" data-delay=""
                                                         class=" qfy-column-8-5d735f3f8e5eb795009 qfy-column-inner  vc_span_class  vc_span6  text-default small-screen-undefined notfullrow"
                                                         data-dw="1/2" data-fixheight="">
                                                        <div style=";position:relative;" class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0" qfyuuid="qfy_gallery_l7mp3"
                                                                     class="qfy-element qfe_gallery qfe_content_element vc_clearfix"
                                                                     style="max-width:100%;width:100%;; margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <div style="" class="qfe_wrapper ">
                                                                        <style>@media only screen and (max-width: 760px) {
                                                                                .thumbnail_text-5d735f3f8ee0e75 .head {
                                                                                    font-size: 16px !important;
                                                                                }

                                                                                .thumbnail_text-5d735f3f8ee0e75 .content {
                                                                                    font-size: 16px !important;
                                                                                }
                                                                            }</style>
                                                                        <style>.swiper-container.gallerys .swiper-pagination-bullet {
                                                                                background: #c5a862
                                                                            }

                                                                            .swiper-pagination.fraction {
                                                                                color: #c5a862
                                                                            }

                                                                            .swiper-container.gallerys .swiper-pagination-bullet-active {
                                                                                background: #382924
                                                                            }</style>
                                                                        <div id="vc-gallery-5d735f3f9026a275058"
                                                                             class="swiper-container gallerys loaded swiper-container-horizontal swiper-container-wp8-horizontal"
                                                                             data-swiper-group=""
                                                                             data-swiper-effect="slide"
                                                                             data-swiper-freemodel="0"
                                                                             data-swiper-slidesperview="1"
                                                                             data-swiper-percolumn="1"
                                                                             data-swiper-direction="horizontal"
                                                                             data-swiper-space="0"
                                                                             data-swiper-center="0" d=""
                                                                             data-swiper-autoheight="0"
                                                                             data-swiper-pagination="bullets"
                                                                             data-swiper-loop="1"
                                                                             data-swiper-autoplay="3000"
                                                                             style="margin: 0px auto; width: 100%; max-width: 100%; cursor: grab;">
                                                                            <ul style="list-style: none; transform: translate3d(-2500px, 0px, 0px); transition-duration: 0ms;"
                                                                                class="swiper-wrapper">
                                                                                <li class="swiper-slide swiper-slide-duplicate swiper-slide-next swiper-slide-duplicate-prev"
                                                                                    data-swiper-slide-index="2"
                                                                                    style="width: 625px;">
                                                                                    <div class="qfy_image_vc_item "
                                                                                         style="position:relative;"><img
                                                                                                class=" ag_image"
                                                                                                data-width="756"
                                                                                                data-height="650"
                                                                                                style="width:756px;height:650px;max-width:100%;"
                                                                                                src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy85MGE5YmIyYWY1MjJiOTE4NWM5ZWE4OTVhODQzYTNhNS03NTZ4NjUwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                alt="silder_2"
                                                                                                description=""
                                                                                                data-attach-id="15599"
                                                                                                data-title="silder_2"
                                                                                                title=""
                                                                                                src-img=""><i></i></div>
                                                                                </li>
                                                                                <li class="swiper-slide swiper-slide-duplicate-active"
                                                                                    data-swiper-slide-index="0"
                                                                                    style="width: 625px;">
                                                                                    <div class="qfy_image_vc_item "
                                                                                         style="position:relative;"><img
                                                                                                class=" ag_image"
                                                                                                data-width="756"
                                                                                                data-height="650"
                                                                                                style="width:756px;height:650px;max-width:100%;"
                                                                                                src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wOC85YjkwMDg4MDEyYjY1ZDY3MGQ1OWUwOGMzMDhmOWZmOC03NTZ4NjUwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                alt="banner3"
                                                                                                description=""
                                                                                                data-attach-id="16853"
                                                                                                data-title="banner3"
                                                                                                title=""
                                                                                                src-img=""><i></i></div>
                                                                                </li>
                                                                                <li class="swiper-slide"
                                                                                    data-swiper-slide-index="1"
                                                                                    style="width: 625px;">
                                                                                    <div class="qfy_image_vc_item "
                                                                                         style="position:relative;"><img
                                                                                                class=" ag_image"
                                                                                                data-width="756"
                                                                                                data-height="650"
                                                                                                style="width:756px;height:650px;max-width:100%;"
                                                                                                src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8yNzc1ZTU1MmMzOTk3M2U0ZDEwOGE0MGM3ZmUxNzY1MC03NTZ4NjUwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                alt="silder_1"
                                                                                                description=""
                                                                                                data-attach-id="15540"
                                                                                                data-title="silder_1"
                                                                                                title=""
                                                                                                src-img=""><i></i></div>
                                                                                </li>
                                                                                <li class="swiper-slide swiper-slide-prev swiper-slide-duplicate-next"
                                                                                    data-swiper-slide-index="2"
                                                                                    style="width: 625px;">
                                                                                    <div class="qfy_image_vc_item "
                                                                                         style="position:relative;"><img
                                                                                                class=" ag_image"
                                                                                                data-width="756"
                                                                                                data-height="650"
                                                                                                style="width:756px;height:650px;max-width:100%;"
                                                                                                src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy85MGE5YmIyYWY1MjJiOTE4NWM5ZWE4OTVhODQzYTNhNS03NTZ4NjUwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                alt="silder_2"
                                                                                                description=""
                                                                                                data-attach-id="15599"
                                                                                                data-title="silder_2"
                                                                                                title=""
                                                                                                src-img=""><i></i></div>
                                                                                </li>
                                                                                <li class="swiper-slide swiper-slide-duplicate swiper-slide-active"
                                                                                    data-swiper-slide-index="0"
                                                                                    style="width: 625px;">
                                                                                    <div class="qfy_image_vc_item "
                                                                                         style="position:relative;"><img
                                                                                                class=" ag_image"
                                                                                                data-width="756"
                                                                                                data-height="650"
                                                                                                style="width:756px;height:650px;max-width:100%;"
                                                                                                src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wOC85YjkwMDg4MDEyYjY1ZDY3MGQ1OWUwOGMzMDhmOWZmOC03NTZ4NjUwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                alt="banner3"
                                                                                                description=""
                                                                                                data-attach-id="16853"
                                                                                                data-title="banner3"
                                                                                                title=""
                                                                                                src-img=""><i></i></div>
                                                                                </li>
                                                                            </ul>
                                                                            <div class="swiper-pagination bullets swiper-pagination-clickable swiper-pagination-bullets">
                                                                                <span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span
                                                                                        class="swiper-pagination-bullet"></span><span
                                                                                        class="swiper-pagination-bullet"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-8-5d735f3f8e5eb795009 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-8-5d735f3f8e5eb795009 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-8-5d735f3f8e5eb795009 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top:;
                                                                padding-bottom:;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-8-5d735f3f8e5eb795009 {
                                                            }

                                                            .qfy-column-8-5d735f3f8e5eb795009 > .column_inner > .background-overlay, .qfy-column-8-5d735f3f8e5eb795009 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                    <div class="wf-mobile-hidden qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                </div>
                                            </div>

                                        </section>
                                        <section data-fixheight=""
                                                 class="qfy-row-5-5d735f3f90564899204 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_czzyn"
                                                 style="margin-bottom:0;border-radius:0px;color:#ffffff;">
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-5-5d735f3f90564899204 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 0;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-5-5d735f3f90564899204 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 20px;
                                                        padding-bottom: 20px;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div data-time="3" data-imagebgs="" class="background-media   "
                                                 backgroundsize="true"
                                                 style="background-image: url('/images/yzh/category_back.jpg'); background-repeat:no-repeat; background-size:cover; background-attachment:scroll; background-position:Array;">
                                            </div>

                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: rgba(56,41,36,0.9);"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="1" data-delay=""
                                                         class=" qfy-column-9-5d735f3f90a7a339135 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/6" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class="background-media  " backgroundsize="true"
                                                                 style="background-image: url(&#39;https://ccdn.goodq.top/caches/b15bd6c05a6cc12948141d4271734ac7/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8xMWExMmM1ZGRjNjU3ODFhNDY1OGI4YjJlMzVlNDU5MS5qcGc_p_p100_p_3D.jpg&#39;); background-repeat:no-repeat; background-size:cover; background-attachment:scroll;background-position:Array;;width:100%;"></div>
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:rgba(56,41,36,0.9);width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_img_5d735f3f91436107"
                                                                     style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0.2"
                                                                     qfyuuid="qfy_single_image_ttwsi-c-3l"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element bitImageControlDiv ">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            .single_image_text-5d735f3f9152b507 .head {
                                                                                font-size: 16px !important;
                                                                            }

                                                                            .single_image_text-5d735f3f9152b507 .content {
                                                                                font-size: 16px !important;
                                                                            }
                                                                        }</style>
                                                                    <a class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_top-to-bottom delay0.2 vc_align_center qfe_start_animation"
                                                                             style="animation-duration: 1s; animation-iteration-count: 1;">
                                                                            <div class="qfe_wrapper"><span></span><img
                                                                                        class="front_image   ag_image"
                                                                                        src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8yY2JmYTliODBkMzk3MzdhMjE1ZjAwNGQxOGNkNWVkOS05MHg5MC5wbmc_p_p100_p_3D.png"
                                                                                        alt="图层 1@0,3x" description=""
                                                                                        data-attach-id="16378"
                                                                                        data-title="图层 1@0,3x" title=""
                                                                                        src-img="" style=""></div>
                                                                        </div>
                                                                    </a></div>
                                                                <a class="bitImageAhover  "> </a>
                                                                <div m-padding="23px 0px 0px 0px" p-padding="23px 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_column_text_r55ry-c-3l"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-92484 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 23px 0px 0px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="text-align: center; letter-spacing: 2px;">
                                                                            <span style="font-size: 16px;">造型美观</span>
                                                                        </div>
                                                                        <div style="text-align: center;"><span
                                                                                    style="font-size: 15px;">DOOR SERISE
</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="1" data-delay=""
                                                         class=" qfy-column-10-5d735f3f919a9855820 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/6" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class="background-media  " backgroundsize="true"
                                                                 style="background-image: url(&#39;https://ccdn.goodq.top/caches/b15bd6c05a6cc12948141d4271734ac7/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8xMWExMmM1ZGRjNjU3ODFhNDY1OGI4YjJlMzVlNDU5MS5qcGc_p_p100_p_3D.jpg&#39;); background-repeat:no-repeat; background-size:cover; background-attachment:scroll;background-position:Array;;width:100%;"></div>
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:#382924;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_img_5d735f3f921a954"
                                                                     style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0.1"
                                                                     qfyuuid="qfy_single_image_ttwsi-c-ik"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element bitImageControlDiv ">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            .single_image_text-5d735f3f92286999 .head {
                                                                                font-size: 16px !important;
                                                                            }

                                                                            .single_image_text-5d735f3f92286999 .content {
                                                                                font-size: 16px !important;
                                                                            }
                                                                        }</style>
                                                                    <a class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_top-to-bottom delay0.1 vc_align_center qfe_start_animation"
                                                                             style="animation-duration: 1s; animation-iteration-count: 1;">
                                                                            <div class="qfe_wrapper"><span></span><img
                                                                                        class="front_image   ag_image"
                                                                                        src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy80OWZmOGExZDk0Yjc3MDQ5YjhlMjVmMWI0NmRmYjRiNC05MHg5MC5wbmc_p_p100_p_3D.png"
                                                                                        alt="图层 2@0,3x" description=""
                                                                                        data-attach-id="16379"
                                                                                        data-title="图层 2@0,3x" title=""
                                                                                        src-img="" style=""></div>
                                                                        </div>
                                                                    </a></div>
                                                                <a class="bitImageAhover  "> </a>
                                                                <div m-padding="23px 0px 0px 0px" p-padding="23px 0 0 0"
                                                                     css_animation_delay="0.1"
                                                                     qfyuuid="qfy_column_text_r55ry-c-97"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-95600 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.1 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 23px 0px 0px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="text-align: center; letter-spacing: 2px;">
                                                                            <span style="font-size: 16px;">密封更好</span>
                                                                        </div>
                                                                        <div style="text-align: center;"><span
                                                                                    style="font-size: 15px;">DOOR SERISE
</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="wf-mobile-visible qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="1" data-delay=""
                                                         class=" qfy-column-11-5d735f3f92714376310 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/6" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class="background-media  " backgroundsize="true"
                                                                 style="background-image: url(&#39;https://ccdn.goodq.top/caches/b15bd6c05a6cc12948141d4271734ac7/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8xMWExMmM1ZGRjNjU3ODFhNDY1OGI4YjJlMzVlNDU5MS5qcGc_p_p100_p_3D.jpg&#39;); background-repeat:no-repeat; background-size:cover; background-attachment:scroll;background-position:Array;;width:100%;"></div>
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:rgba(56,41,36,0.9);width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_img_5d735f3f92f4b489"
                                                                     style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0.2"
                                                                     qfyuuid="qfy_single_image_ttwsi" data-anitime="1"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element bitImageControlDiv ">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            .single_image_text-5d735f3f93028787 .head {
                                                                                font-size: 16px !important;
                                                                            }

                                                                            .single_image_text-5d735f3f93028787 .content {
                                                                                font-size: 16px !important;
                                                                            }
                                                                        }</style>
                                                                    <a class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_top-to-bottom delay0.2 vc_align_center qfe_start_animation"
                                                                             style="animation-duration: 1s; animation-iteration-count: 1;">
                                                                            <div class="qfe_wrapper"><span></span><img
                                                                                        class="front_image   ag_image"
                                                                                        src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy81YTA1YzdhNWQ4OTJhM2EyYjIyNjlmNzFlNjQ4MDljNi05MHg5MC5wbmc_p_p100_p_3D.png"
                                                                                        alt="图层 3@0,3x" description=""
                                                                                        data-attach-id="16380"
                                                                                        data-title="图层 3@0,3x" title=""
                                                                                        src-img="" style=""></div>
                                                                        </div>
                                                                    </a></div>
                                                                <a class="bitImageAhover  "> </a>
                                                                <div m-padding="23px 0px 0px 0px" p-padding="23px 0 0 0"
                                                                     css_animation_delay="0.2"
                                                                     qfyuuid="qfy_column_text_r55ry-c-9w"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-26990 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.2 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 23px 0px 0px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="text-align: center; letter-spacing: 2px;">
                                                                            <span style="font-size: 16px;">质轻高强</span>
                                                                        </div>
                                                                        <div style="text-align: center;"><span
                                                                                    style="font-size: 15px;">DOOR SERISE
</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="1" data-delay=""
                                                         class=" qfy-column-12-5d735f3f934b4890920 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/6" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class="background-media  " backgroundsize="true"
                                                                 style="background-image: url(&#39;https://ccdn.goodq.top/caches/b15bd6c05a6cc12948141d4271734ac7/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8xMWExMmM1ZGRjNjU3ODFhNDY1OGI4YjJlMzVlNDU5MS5qcGc_p_p100_p_3D.jpg&#39;); background-repeat:no-repeat; background-size:cover; background-attachment:scroll;background-position:Array;;width:100%;"></div>
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:#382924;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_img_5d735f3f93d05638"
                                                                     style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0.3"
                                                                     qfyuuid="qfy_single_image_ttwsi-c-h1"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element bitImageControlDiv ">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            .single_image_text-5d735f3f93de2148 .head {
                                                                                font-size: 16px !important;
                                                                            }

                                                                            .single_image_text-5d735f3f93de2148 .content {
                                                                                font-size: 16px !important;
                                                                            }
                                                                        }</style>
                                                                    <a class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_top-to-bottom delay0.3 vc_align_center qfe_start_animation"
                                                                             style="animation-duration: 1s; animation-iteration-count: 1;">
                                                                            <div class="qfe_wrapper"><span></span><img
                                                                                        class="front_image   ag_image"
                                                                                        src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy9kNDQ1NTU4N2FjMzc4OWE2Y2VjODE2NjgwZDAwZWRiMS05MHg5MC5wbmc_p_p100_p_3D.png"
                                                                                        alt="图层 4@0,3x" description=""
                                                                                        data-attach-id="16381"
                                                                                        data-title="图层 4@0,3x" title=""
                                                                                        src-img="" style=""></div>
                                                                        </div>
                                                                    </a></div>
                                                                <a class="bitImageAhover  "> </a>
                                                                <div m-padding="23px 0px 0px 0px" p-padding="23px 0 0 0"
                                                                     css_animation_delay="0.3"
                                                                     qfyuuid="qfy_column_text_r55ry-c-gr"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-71337 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.3 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 23px 0px 0px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="text-align: center; letter-spacing: 2px;">
                                                                            <span style="font-size: 16px;">安全耐用</span>
                                                                        </div>
                                                                        <div style="text-align: center;"><span
                                                                                    style="font-size: 15px;">DOOR SERISE
</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="wf-mobile-visible qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="1" data-delay=""
                                                         class=" qfy-column-13-5d735f3f94273754790 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/6" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class="background-media  " backgroundsize="true"
                                                                 style="background-image: url(&#39;https://ccdn.goodq.top/caches/b15bd6c05a6cc12948141d4271734ac7/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8xMWExMmM1ZGRjNjU3ODFhNDY1OGI4YjJlMzVlNDU5MS5qcGc_p_p100_p_3D.jpg&#39;); background-repeat:no-repeat; background-size:cover; background-attachment:scroll;background-position:Array;;width:100%;"></div>
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:rgba(56,41,36,0.9);width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_img_5d735f3f94a98209"
                                                                     style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0.4"
                                                                     qfyuuid="qfy_single_image_ttwsi-c-pe"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element bitImageControlDiv ">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            .single_image_text-5d735f3f94b75492 .head {
                                                                                font-size: 16px !important;
                                                                            }

                                                                            .single_image_text-5d735f3f94b75492 .content {
                                                                                font-size: 16px !important;
                                                                            }
                                                                        }</style>
                                                                    <a class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_top-to-bottom delay0.4 vc_align_center qfe_start_animation"
                                                                             style="animation-duration: 1s; animation-iteration-count: 1;">
                                                                            <div class="qfe_wrapper"><span></span><img
                                                                                        class="front_image   ag_image"
                                                                                        src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy9lOTNmZjVmYzU2MmUxNmM4YmY5NGE5Zjc4MzE0MDQ2Mi05MHg5MC5wbmc_p_p100_p_3D.png"
                                                                                        alt="图层 5@0,3x" description=""
                                                                                        data-attach-id="16382"
                                                                                        data-title="图层 5@0,3x" title=""
                                                                                        src-img="" style=""></div>
                                                                        </div>
                                                                    </a></div>
                                                                <a class="bitImageAhover  "> </a>
                                                                <div m-padding="23px 0px 0px 0px" p-padding="23px 0 0 0"
                                                                     css_animation_delay="0.4"
                                                                     qfyuuid="qfy_column_text_r55ry-c-pe"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-86969 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.4 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 23px 0px 0px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="text-align: center; letter-spacing: 2px;">
                                                                            <span style="font-size: 16px;">创新优势</span>
                                                                        </div>
                                                                        <div style="text-align: center;"><span
                                                                                    style="font-size: 15px;">DOOR SERISE
</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="1" data-delay=""
                                                         class=" qfy-column-14-5d735f3f94fcb771214 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/6" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class="background-media  " backgroundsize="true"
                                                                 style="background-image: url(&#39;https://ccdn.goodq.top/caches/b15bd6c05a6cc12948141d4271734ac7/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8xMWExMmM1ZGRjNjU3ODFhNDY1OGI4YjJlMzVlNDU5MS5qcGc_p_p100_p_3D.jpg&#39;); background-repeat:no-repeat; background-size:cover; background-attachment:scroll;background-position:Array;;width:100%;"></div>
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:#382924;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_img_5d735f3f957ca403"
                                                                     style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0.5"
                                                                     qfyuuid="qfy_single_image_ttwsi-c-ly"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element bitImageControlDiv ">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            .single_image_text-5d735f3f958b1766 .head {
                                                                                font-size: 16px !important;
                                                                            }

                                                                            .single_image_text-5d735f3f958b1766 .content {
                                                                                font-size: 16px !important;
                                                                            }
                                                                        }</style>
                                                                    <a class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_top-to-bottom delay0.5 vc_align_center qfe_start_animation"
                                                                             style="animation-duration: 1s; animation-iteration-count: 1;">
                                                                            <div class="qfe_wrapper"><span></span><img
                                                                                        class="front_image   ag_image"
                                                                                        src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8xMGRmNWJhZDM4NWZkMDM2YThjNzA3YTA3MmJjNWRmYi05MHg5MC5wbmc_p_p100_p_3D.png"
                                                                                        alt="图层 6@0,3x" description=""
                                                                                        data-attach-id="16383"
                                                                                        data-title="图层 6@0,3x" title=""
                                                                                        src-img="" style=""></div>
                                                                        </div>
                                                                    </a></div>
                                                                <a class="bitImageAhover  "> </a>
                                                                <div m-padding="23px 0px 0px 0px" p-padding="23px 0 0 0"
                                                                     css_animation_delay="0.5"
                                                                     qfyuuid="qfy_column_text_r55ry-c-ly"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-21878 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.5 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 23px 0px 0px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="text-align: center; letter-spacing: 2px;">
                                                                            <span style="font-size: 16px;">牢固美观</span>
                                                                        </div>
                                                                        <div style="text-align: center;"><span
                                                                                    style="font-size: 15px;">DOOR SERISE
</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-14-5d735f3f94fcb771214 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 10vh;
                                                                padding-bottom: 10vh;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-14-5d735f3f94fcb771214 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-14-5d735f3f94fcb771214 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 20px;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-14-5d735f3f94fcb771214 {
                                                            }

                                                            .qfy-column-14-5d735f3f94fcb771214 > .column_inner > .background-overlay, .qfy-column-14-5d735f3f94fcb771214 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                    <div class="wf-mobile-hidden qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                    <div class="wf-mobile-visible qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                </div>
                                            </div>

                                        </section>
                                        <section data-fixheight=""
                                                 class="qfy-row-6-5d735f3f95d8c965327 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_wx8yg"
                                                 style="margin-bottom:0;border-radius:0px;border-top:0px solid rgba(255,255,255,1);border-bottom:1px solid rgba(232,232,232,1);border-left:0px solid rgba(255,255,255,1);border-right:0px solid rgba(255,255,255,1);color:#382924;">
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-6-5d735f3f95d8c965327 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 15vh;
                                                        padding-bottom: 10vh;
                                                        margin-top: 0;
                                                    }

                                                    section.section.qfy-row-6-5d735f3f95d8c965327 > .container {
                                                        max-width: 1600px;
                                                        margin: 0 auto;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-6-5d735f3f95d8c965327 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 40px;
                                                        padding-bottom: 40px;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: #ffffff;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="1" data-delay=""
                                                         class=" qfy-column-15-5d735f3f960cb832458 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/3" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:5vw;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3f963e213"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_header_fwnoy-c-6y" data-anitime="1"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom qfe_start_animation"
                                                                     style="position: relative; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <style>@font-face {
                                                                            font-family: "f9c2524dc0f631cd4641ad47985a6c2d2";
                                                                            src: url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.eot"); /* IE9 */
                                                                            src: url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */ url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.woff") format("woff"), /* chrome, firefox */ url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.ttf") format("truetype"), /* chrome, firefox, opera, Safari, Android, iOS 4.2+ */ url("https://fonts.goodq.top/201907/f9c2524dc0f631cd4641ad47985a6c2d2/SourceHanSansCN-Heavy.svg#f9c2524dc0f631cd4641ad47985a6c2d2") format("svg"); /* iOS 4.1- */
                                                                            font-style: normal;
                                                                            font-weight: normal;
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f963e213 .header_title {
                                                                                font-size: 14px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f963e213 .header_subtitle {
                                                                                font-size: 30px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title left mobileleft">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_title"
                                                                                         style="font-size:14px;font-weight:normal;font-style:normal;color:#c5a862;display:block;vertical-align:bottom;">
                                                                                        您可以双击这里编辑
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-family:f9c2524dc0f631cd4641ad47985a6c2d2;font-size:36px;font-weight:bold;font-style:normal;color:#382924;display:block;vertical-align:bottom;">
                                                                                        整体门窗定制解决方案
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div m-padding="19px 0px 0px 0px" p-padding="19px 0 0 0"
                                                                     css_animation_delay="0.1"
                                                                     qfyuuid="qfy_column_text_qsxkq-c-vm"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-54783 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.1 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 19px 0px 0px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="line-height: 28px;"><span
                                                                                    style="font-size: 15px;">


起飞页倾力打造的一个从网站建设，网站运营管理到网站推广的自动化免费建站平台，为中小微企业及个人提供免费的在线响应式建站服务.起飞页倾力打造的一个从网站建设，网网站运营管理到网站推广的自动化免费建站平台，为中小微企业及个人提供免费的在线响应式建站服务。为终端用户浏览带来最佳用户体验.实现无需编程零基础建站,使人们轻松建设企业网站专题网站个人网站，满足企业品牌宣传个人形象展示产品展示营销推广数据收集。</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="qfy-btn-5d735f3f96cd19075"
                                                                     style="margin-top: 20px; margin-bottom: 0px; padding: 0px; animation-duration: 1s; animation-iteration-count: 1;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0.2"
                                                                     qfyuuid="qfy_btn_lpy3l-c-6y" data-anitime="1"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element vc_btn3-container  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.2 vc_btn3-left qfe_start_animation">
                                                                    <button onmouseleave="this.style.borderColor=&#39;#c5a862&#39;; this.style.backgroundColor=&#39;#c5a862&#39;; this.style.color=&#39;#fff&#39;;"
                                                                            onmouseenter="this.style.backgroundColor=&#39;#c5a862&#39;; this.style.borderColor=&#39;#c5a862&#39;; this.style.color=&#39;#fff&#39;;"
                                                                            style="font-size:15px; padding-left:35px;padding-right:35px; padding-top:15px;padding-bottom:15px; border-width:2px; border-color:#c5a862; background-color:#c5a862; color:#fff;"
                                                                            class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-rounded vc_btn3-style-qfy-custom">
                                                                        下载产品列表 →
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div data-animaleinbegin="bottom-in-view"
                                                         data-animalename="qfyfadeInUp" data-duration="" data-delay=""
                                                         class=" qfy-column-16-5d735f3f96e6d288843 qfy-column-inner  vc_span_class  vc_span8  text-default small-screen-default notfullrow"
                                                         data-dw="2/3" data-fixheight="">
                                                        <div style=";position:relative;" class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_img_5d735f3f976e7447"
                                                                     style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_single_image_rwlys-c-6y"
                                                                     data-anitime="0.7" data-ani_iteration_count="1"
                                                                     class="qfy-element bitImageControlDiv ">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            .single_image_text-5d735f3f977ce246 .head {
                                                                                font-size: 16px !important;
                                                                            }

                                                                            .single_image_text-5d735f3f977ce246 .content {
                                                                                font-size: 16px !important;
                                                                            }
                                                                        }</style>
                                                                    <a class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_right-to-left vc_align_center qfe_start_animation"
                                                                             style="animation-duration: 0.7s; animation-iteration-count: 1;">
                                                                            <div class="qfe_wrapper"><span></span><img
                                                                                        width="1024" height="565"
                                                                                        src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi8wZWViZDJmMzRmMjdjNmI5ZmExNDU1NjE4MzYyMWY1Yy0xMDI0eDU2NS5qcGc_p_p100_p_3D.jpg"
                                                                                        class="front_image  attachment-large"
                                                                                        alt="magazin-1" title=""
                                                                                        description=""
                                                                                        data-title="magazin-1"
                                                                                        src-img="" style=""></div>
                                                                        </div>
                                                                    </a></div>
                                                                <a class="bitImageAhover  "> </a></div>
                                                        </div>
                                                    </div>

                                                    <div class="wf-mobile-hidden qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                </div>
                                            </div>

                                        </section>
                                        <section data-fixheight=""
                                                 class="qfy-row-7-5d735f3f979da396250 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_d8b8g"
                                                 style="margin-bottom:0;border-radius:0px;color:#382924;">
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-7-5d735f3f979da396250 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 40px;
                                                        padding-bottom: 40px;
                                                        margin-top: 0;
                                                    }

                                                    section.section.qfy-row-7-5d735f3f979da396250 > .container {
                                                        max-width: 1600px;
                                                        margin: 0 auto;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-7-5d735f3f979da396250 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 10px;
                                                        padding-bottom: 20px;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: #ffffff;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInDown"
                                                         data-duration="1" data-delay="0.1"
                                                         class="qfyanimate qfy-column-17-5d735f3f97cd9954225 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow qfyanimated"
                                                         data-dw="1/6" data-fixheight=""
                                                         style="animation-duration: 1s; animation-delay: 0.1s; animation-name: qfyfadeInDown; visibility: visible;">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3f97fcc171"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_header_hy33n-c-ut" data-anitime="0.7"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                     style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <style>#vc_header_5d735f3f97fcc171 .qfy_title_inner, #vc_header_5d735f3f97fcc171 .qfy_title_image_inner {
                                                                            vertical-align: middle !important;
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f97fcc171 .header_title {
                                                                                font-size: 18px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f97fcc171 .header_subtitle {
                                                                                font-size: 12px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title left mobileleft">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_image_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_image "
                                                                                         style="padding:5px 0 0 0;width:50px;">
                                                                                        <img class=" ag_image"
                                                                                             src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi9hOTk2ZDI1MzhiOTgzYmI2ODdiMjlhMzk4NWFjNGM3NC01MHg1MC5wbmc_p_p100_p_3D.png"
                                                                                             alt="about (3)"
                                                                                             description=""
                                                                                             data-attach-id="14881"
                                                                                             data-title="about (3)"
                                                                                             title="" src-img=""></div>
                                                                                </div>
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                                                    <div class="header_title"
                                                                                         style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                        精工细作
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                        网站建设网站运营管理
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-17-5d735f3f97cd9954225 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-17-5d735f3f97cd9954225 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-17-5d735f3f97cd9954225 > .column_inner {
                                                                margin: 15px auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 5px;
                                                                padding-right: 5px;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-17-5d735f3f97cd9954225 {
                                                            }

                                                            .qfy-column-17-5d735f3f97cd9954225 > .column_inner > .background-overlay, .qfy-column-17-5d735f3f97cd9954225 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInDown"
                                                         data-duration="1" data-delay="0.2"
                                                         class="qfyanimate qfy-column-18-5d735f3f98718252069 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow qfyanimated"
                                                         data-dw="1/6" data-fixheight=""
                                                         style="animation-duration: 1s; animation-delay: 0.2s; animation-name: qfyfadeInDown; visibility: visible;">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3f98a049"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_header_hy33n-c-ut" data-anitime="0.7"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                     style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <style>#vc_header_5d735f3f98a049 .qfy_title_inner, #vc_header_5d735f3f98a049 .qfy_title_image_inner {
                                                                            vertical-align: middle !important;
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f98a049 .header_title {
                                                                                font-size: 18px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f98a049 .header_subtitle {
                                                                                font-size: 12px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title left mobileleft">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_image_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_image "
                                                                                         style="padding:5px 0 0 0;width:50px;">
                                                                                        <img class=" ag_image"
                                                                                             src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi84NmU1YjE2ZWJkNjNjMGNiZmVjZmZlNjNjNmJhNDI1Ni01MHg1MC5wbmc_p_p100_p_3D.png"
                                                                                             alt="about (5)"
                                                                                             description=""
                                                                                             data-attach-id="14883"
                                                                                             data-title="about (5)"
                                                                                             title="" src-img=""></div>
                                                                                </div>
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                                                    <div class="header_title"
                                                                                         style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                        质感于室
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                        网站建设网站运营管理
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="wf-mobile-visible qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInDown"
                                                         data-duration="1" data-delay="0.3"
                                                         class="qfyanimate qfy-column-19-5d735f3f99179169847 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow qfyanimated"
                                                         data-dw="1/6" data-fixheight=""
                                                         style="animation-duration: 1s; animation-delay: 0.3s; animation-name: qfyfadeInDown; visibility: visible;">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3f99485208"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_header_hy33n-c-ut" data-anitime="0.7"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                     style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <style>#vc_header_5d735f3f99485208 .qfy_title_inner, #vc_header_5d735f3f99485208 .qfy_title_image_inner {
                                                                            vertical-align: middle !important;
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f99485208 .header_title {
                                                                                font-size: 18px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f99485208 .header_subtitle {
                                                                                font-size: 12px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title left mobileleft">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_image_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_image "
                                                                                         style="padding:5px 0 0 0;width:50px;">
                                                                                        <img class=" ag_image"
                                                                                             src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi9hNzY0MDlhODc1MWIwMWIwYjM4ZjMzNmJkYjE5N2IzMi01MHg1MC5wbmc_p_p100_p_3D.png"
                                                                                             alt="about (1)"
                                                                                             description=""
                                                                                             data-attach-id="14911"
                                                                                             data-title="about (1)"
                                                                                             title="" src-img=""></div>
                                                                                </div>
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                                                    <div class="header_title"
                                                                                         style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                        安全耐用
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                        网站建设网站运营管理
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-19-5d735f3f99179169847 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-19-5d735f3f99179169847 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-19-5d735f3f99179169847 > .column_inner {
                                                                margin: 15px auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 5px;
                                                                padding-right: 5px;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-19-5d735f3f99179169847 {
                                                            }

                                                            .qfy-column-19-5d735f3f99179169847 > .column_inner > .background-overlay, .qfy-column-19-5d735f3f99179169847 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInDown"
                                                         data-duration="1" data-delay="0.4"
                                                         class="qfyanimate qfy-column-20-5d735f3f99bff385467 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow qfyanimated"
                                                         data-dw="1/6" data-fixheight=""
                                                         style="animation-duration: 1s; animation-delay: 0.4s; animation-name: qfyfadeInDown; visibility: visible;">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3f99ef9659"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_header_hy33n-c-ut" data-anitime="0.7"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                     style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <style>#vc_header_5d735f3f99ef9659 .qfy_title_inner, #vc_header_5d735f3f99ef9659 .qfy_title_image_inner {
                                                                            vertical-align: middle !important;
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f99ef9659 .header_title {
                                                                                font-size: 18px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f99ef9659 .header_subtitle {
                                                                                font-size: 12px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title left mobileleft">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_image_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_image "
                                                                                         style="padding:5px 0 0 0;width:50px;">
                                                                                        <img class=" ag_image"
                                                                                             src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi83YWM4MDcwMjY0Y2FmOTdjM2U3YWM0MTU5NmU2ZDY4Ni01MHg1MC5wbmc_p_p100_p_3D.png"
                                                                                             alt="about (4)"
                                                                                             description=""
                                                                                             data-attach-id="14882"
                                                                                             data-title="about (4)"
                                                                                             title="" src-img=""></div>
                                                                                </div>
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                                                    <div class="header_title"
                                                                                         style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                        节能环保
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                        网站建设网站运营管理
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-20-5d735f3f99bff385467 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-20-5d735f3f99bff385467 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-20-5d735f3f99bff385467 > .column_inner {
                                                                margin: 15px auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 5px;
                                                                padding-right: 5px;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-20-5d735f3f99bff385467 {
                                                            }

                                                            .qfy-column-20-5d735f3f99bff385467 > .column_inner > .background-overlay, .qfy-column-20-5d735f3f99bff385467 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                    <div class="wf-mobile-visible qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInDown"
                                                         data-duration="1" data-delay="0.5"
                                                         class="qfyanimate qfy-column-21-5d735f3f9a64f995083 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow qfyanimated"
                                                         data-dw="1/6" data-fixheight=""
                                                         style="animation-duration: 1s; animation-delay: 0.5s; animation-name: qfyfadeInDown; visibility: visible;">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3f9a962656"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_header_hy33n-c-ut" data-anitime="0.7"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                     style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <style>#vc_header_5d735f3f9a962656 .qfy_title_inner, #vc_header_5d735f3f9a962656 .qfy_title_image_inner {
                                                                            vertical-align: middle !important;
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f9a962656 .header_title {
                                                                                font-size: 18px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f9a962656 .header_subtitle {
                                                                                font-size: 12px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title left mobileleft">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_image_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_image "
                                                                                         style="padding:5px 0 0 0;width:50px;">
                                                                                        <img class=" ag_image"
                                                                                             src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi8yZGJiNGU0MTY5MzgyZDYwOTFmMDAyOGU4ZWEyYjNiNy01MHg1MC5wbmc_p_p100_p_3D.png"
                                                                                             alt="about (2)"
                                                                                             description=""
                                                                                             data-attach-id="14912"
                                                                                             data-title="about (2)"
                                                                                             title="" src-img=""></div>
                                                                                </div>
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                                                    <div class="header_title"
                                                                                         style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                        静音密封
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                        网站建设网站运营管理
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-21-5d735f3f9a64f995083 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-21-5d735f3f9a64f995083 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-21-5d735f3f9a64f995083 > .column_inner {
                                                                margin: 15px auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 5px;
                                                                padding-right: 5px;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-21-5d735f3f9a64f995083 {
                                                            }

                                                            .qfy-column-21-5d735f3f9a64f995083 > .column_inner > .background-overlay, .qfy-column-21-5d735f3f9a64f995083 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInDown"
                                                         data-duration="1" data-delay="0.6"
                                                         class="qfyanimate qfy-column-22-5d735f3f9b0cd549695 qfy-column-inner  vc_span_mobile6  vc_span_class  vc_span_mobile  vc_span2  text-Default small-screen-undefined notfullrow qfyanimated"
                                                         data-dw="1/6" data-fixheight=""
                                                         style="animation-duration: 1s; animation-delay: 0.6s; animation-name: qfyfadeInDown; visibility: visible;">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3f9b3cc633"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_header_hy33n-c-ut" data-anitime="0.7"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                     style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <style>#vc_header_5d735f3f9b3cc633 .qfy_title_inner, #vc_header_5d735f3f9b3cc633 .qfy_title_image_inner {
                                                                            vertical-align: middle !important;
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f9b3cc633 .header_title {
                                                                                font-size: 18px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f9b3cc633 .header_subtitle {
                                                                                font-size: 12px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title left mobileleft">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_image_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_image "
                                                                                         style="padding:5px 0 0 0;width:50px;">
                                                                                        <img class=" ag_image"
                                                                                             src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi85MTA1MWVmNDE2Yzg5YmI4MzhkYjNkZmFkMDUyY2Y2ZC01MHg1MC5wbmc_p_p100_p_3D.png"
                                                                                             alt="about (6)"
                                                                                             description=""
                                                                                             data-attach-id="14884"
                                                                                             data-title="about (6)"
                                                                                             title="" src-img=""></div>
                                                                                </div>
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:calc(100% - 50px);">
                                                                                    <div class="header_title"
                                                                                         style="font-family:微软雅黑;font-size:18px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                        美观牢固
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-size:14px;font-weight:normal;font-style:normal;color:#382924;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                        网站建设网站运营管理
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-22-5d735f3f9b0cd549695 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-22-5d735f3f9b0cd549695 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-22-5d735f3f9b0cd549695 > .column_inner {
                                                                margin: 15px auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 5px;
                                                                padding-right: 5px;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-22-5d735f3f9b0cd549695 {
                                                            }

                                                            .qfy-column-22-5d735f3f9b0cd549695 > .column_inner > .background-overlay, .qfy-column-22-5d735f3f9b0cd549695 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                    <div class="wf-mobile-hidden qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                    <div class="wf-mobile-visible qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                </div>
                                            </div>

                                        </section>
                                        <section data-fixheight=""
                                                 class="qfy-row-8-5d735f3f9bb97804116 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_vbof2"
                                                 style="margin-bottom:0;border-radius:0px;color:#ffffff;">
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-8-5d735f3f9bb97804116 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 10vh;
                                                        padding-bottom: 40px;
                                                        margin-top: 0;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-8-5d735f3f9bb97804116 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 40px;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: #c5a862;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="" data-delay=""
                                                         class=" qfy-column-23-5d735f3f9bd16263057 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                                                         data-dw="1/1" data-fixheight="">
                                                        <div style=";position:relative;" class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3f9bfdf387"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_header_2wylr-c-5b" data-anitime="1"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom qfe_start_animation"
                                                                     style="position: relative; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f9bfdf387 .header_title {
                                                                                font-size: 14px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f9bfdf387 .header_subtitle {
                                                                                font-size: 30px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title center mobilecenter">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_title"
                                                                                         style="font-family:arial;font-size:16px;font-weight:bold;font-style:normal;color:#ffffff;display:block;vertical-align:bottom;">
                                                                                        Our Product
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-size:36px;font-weight:bold;font-style:normal;color:#382924;display:block;vertical-align:bottom;">
                                                                                        我们的产品
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-23-5d735f3f9bd16263057 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-23-5d735f3f9bd16263057 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-23-5d735f3f9bd16263057 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top:;
                                                                padding-bottom:;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-23-5d735f3f9bd16263057 {
                                                            }

                                                            .qfy-column-23-5d735f3f9bd16263057 > .column_inner > .background-overlay, .qfy-column-23-5d735f3f9bd16263057 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                </div>
                                            </div>

                                        </section>
                                        <section data-fixheight=""
                                                 class="qfy-row-9-5d735f3f9c35c13088 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_77cfc"
                                                 style="margin-bottom:0;border-radius:0px;color:#ffffff;">
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-9-5d735f3f9c35c13088 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 20px;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                    }

                                                    section.section.qfy-row-9-5d735f3f9c35c13088 > .container {
                                                        max-width: 1600px;
                                                        margin: 0 auto;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-9-5d735f3f9c35c13088 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 20px;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div data-time="3" data-imagebgs="" class="background-media   "
                                                 style="background-image: url(&#39;https://ccdn.goodq.top/caches/b15bd6c05a6cc12948141d4271734ac7/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy81YTc5YTZhYzQwODg2NDgxMmY5OWI0MGM2NDJjMzg3MC5qcGc_p_p100_p_3D.jpg&#39;); background-repeat:repeat-x;background-attachment:scroll;background-size:auto 90%;background-position:left top">
                                            </div>

                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: transparent;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    @foreach($categories as $cate)
                                                        <div data-animaleinbegin="90%" data-animalename="qfyfadeInDown"
                                                             data-duration="1" data-delay="0.1"
                                                             class=" qfy-column-24-5d735f3f9c811755563 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-undefined notfullrow"
                                                             data-dw="1/3" data-fixheight="">
                                                            <div style="margin-top:0;margin-bottom:0;margin-left:10px;margin-right:10px;border-radius:10px;;position:relative;"
                                                                 class="column_inner ">
                                                                <div class="background-media  " backgroundsize="true"
                                                                     style="background-image: url(&#39;https://ccdn.goodq.top/caches/b15bd6c05a6cc12948141d4271734ac7/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy83ZGIzNzhkNTY5MjcyMTdmZTY2Yjc3M2MwNTk2YTZmYy5qcGc_p_p100_p_3D.jpg&#39;); background-repeat:no-repeat; background-size:cover; background-attachment:scroll;background-position:Array;;width:100%;border-radius:10px;"></div>
                                                                <div class=" background-overlay grid-overlay-0"
                                                                     style="background-color:transparent;width:100%;border-radius:10px;"></div>
                                                                <div class="column_containter"
                                                                     style="z-index:3;position:relative;border-radius:10px;">
                                                                    <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                         css_animation_delay="0"
                                                                         qfyuuid="qfy_header_l8lgf" data-anitime="0.7"
                                                                         data-ani_iteration_count="1"
                                                                         class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                         style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">

                                                                        <div class="qfe_wrapper">
                                                                            <div class="qfy_title left mobileleft">
                                                                                <div class="qfy_title_image_warpper"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="qfy_title_inner"
                                                                                         style="display:inline-block;position:relative;max-width:100%;">
                                                                                        <div class="header_title"
                                                                                             style="font-size:30px;font-weight:bold;font-style:normal;color:#ffffff;padding:0 5px 0 0;display:block;padding:0 5px 0 0;vertical-align:bottom;">
                                                                                            {{ $cate->name }}系列产品
                                                                                        </div>
                                                                                        <div class="header_subtitle"
                                                                                             style="font-family:arial;font-size:16px;font-weight:normal;font-style:normal;color:#ffffff;display:block;padding:2px;vertical-align:bottom;">
                                                                                            / {{ $cate->keywords }}
                                                                                        </div>
                                                                                    </div>
                                                                                    <style></style>
                                                                                    <div class="button_wrapper"
                                                                                         style="display:none;">
                                                                                        <div class="button "
                                                                                             style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                            + 查看更多
                                                                                        </div>
                                                                                    </div>
                                                                                    <span style="clear:both;"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="qfy-btn-5d735f3f9d32f7843"
                                                                         style="margin-top:0;margin-bottom:0;padding-top:20px;padding-bottom:0;padding-right:0;padding-left:0;"
                                                                         m-padding="20px 0px 0px 0px"
                                                                         p-padding="20px 0 0 0"
                                                                         css_animation_delay="0" qfyuuid="qfy_btn_9fvde"
                                                                         data-anitime="0.7" data-ani_iteration_count="1"
                                                                         class="qfy-element vc_btn3-container vc_btn3-left">
                                                                        <a onmouseleave="this.style.borderColor=&#39;#382924&#39;; this.style.backgroundColor=&#39;#382924&#39;; this.style.color=&#39;#fff&#39;;"
                                                                           onmouseenter="this.style.backgroundColor=&#39;#382924&#39;; this.style.borderColor=&#39;#382924&#39;; this.style.color=&#39;#fff&#39;;"
                                                                           style="font-size:15px; padding-left:35px;padding-right:35px; padding-top:10px;padding-bottom:10px; border-width:2px; border-color:#382924; background-color:#382924; color:#fff;"
                                                                           class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-rounded vc_btn3-style-qfy-custom"
                                                                           href="/category/{{ $cate->id }}"
                                                                           target="">查看产品 →</a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <style class="column_class qfy_style_class">
                                                            @media only screen and (min-width: 992px) {
                                                                .qfy-column-24-5d735f3f9c811755563 > .column_inner {
                                                                    padding-left: 50px;
                                                                    padding-right: 50px;
                                                                    padding-top: 45vh;
                                                                    padding-bottom: 40px;
                                                                }

                                                                .qfe_row .vc_span_class.qfy-column-24-5d735f3f9c811755563 {
                                                                }

                                                            ;
                                                            }

                                                            @media only screen and (max-width: 992px) {
                                                                .qfy-column-24-5d735f3f9c811755563 > .column_inner {
                                                                    margin: 10px auto 0 !important;
                                                                    min-height: 0 !important;
                                                                    padding-left: 30px;
                                                                    padding-right: 30px;
                                                                    padding-top: 20vh;
                                                                    padding-bottom: 30px;
                                                                }

                                                                .display_entire .qfe_row .vc_span_class.qfy-column-24-5d735f3f9c811755563 {
                                                                }

                                                                .qfy-column-24-5d735f3f9c811755563 > .column_inner > .background-overlay, .qfy-column-24-5d735f3f9c811755563 > .column_inner > .background-media {
                                                                    width: 100% !important;
                                                                    left: 0 !important;
                                                                    right: auto !important;
                                                                }
                                                            }
                                                        </style>
                                                    @endforeach
                                                </div>
                                            </div>

                                        </section>
                                        <section data-fixheight=""
                                                 class="qfy-row-10-5d735f3f9f095672267 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_dwgxx"
                                                 style="margin-bottom:0;border-radius:0px;color:#382924;">
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-10-5d735f3f9f095672267 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 15vh;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-10-5d735f3f9f095672267 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 40px;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: #ffffff;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="bottom-in-view"
                                                         data-animalename="qfyfadeInUp" data-duration="" data-delay=""
                                                         class=" qfy-column-27-5d735f3f9f2f4762435 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                                                         data-dw="1/1" data-fixheight="">
                                                        <div style=";position:relative;" class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3f9f5da306"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_header_2wylr-c-av" data-anitime="1"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom qfe_start_animation"
                                                                     style="position: relative; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f9f5da306 .header_title {
                                                                                font-size: 14px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3f9f5da306 .header_subtitle {
                                                                                font-size: 30px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title center mobilecenter">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_title"
                                                                                         style="font-family:arial;font-size:16px;font-weight:bold;font-style:normal;color:#c5a862;display:block;vertical-align:bottom;">
                                                                                        Our Feature
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-size:36px;font-weight:bold;font-style:normal;color:#382924;display:block;vertical-align:bottom;">
                                                                                        专注突显优势 实力铸就极佳性能
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div m-padding="19px 0px 0px 0px" p-padding="19px 0 0 0"
                                                                     css_animation_delay="0.1"
                                                                     qfyuuid="qfy_column_text_qsxkq-c-av"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-11930 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.1 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 19px 0px 0px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="line-height: 20px; text-align: center;"><span
                                                                                    style="font-size: 15px;">


</span><span style="font-size: 15px;">为终端用户浏览带来最佳用户体验.实现无需编程零基础建站,使人们轻松建设企业网站专题网站个人网站</span></div>
                                                                        <div style="line-height: 20px; text-align: center;">
                                                                            <span style="font-size: 15px;">满足企业品牌宣传个人形象展示产品展示营销推广数据收集</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-27-5d735f3f9f2f4762435 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-27-5d735f3f9f2f4762435 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-27-5d735f3f9f2f4762435 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top:;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-27-5d735f3f9f2f4762435 {
                                                            }

                                                            .qfy-column-27-5d735f3f9f2f4762435 > .column_inner > .background-overlay, .qfy-column-27-5d735f3f9f2f4762435 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                </div>
                                            </div>

                                        </section>
                                        <section data-fixheight=""
                                                 class="qfy-row-11-5d735f3f9fc379034 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_bbwgk"
                                                 style="margin-bottom:0;border-radius:0px;color:#382924;">
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-11-5d735f3f9fc379034 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 20px;
                                                        padding-bottom: 15vh;
                                                        margin-top: 0;
                                                    }

                                                    section.section.qfy-row-11-5d735f3f9fc379034 > .container {
                                                        max-width: 1500px;
                                                        margin: 0 auto;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-11-5d735f3f9fc379034 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 20px;
                                                        padding-bottom: 40px;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: #ffffff;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="" data-delay=""
                                                         class=" qfy-column-28-5d735f3f9feaf701958 qfy-column-inner  vc_span12  text-default small-screen-default fullrow"
                                                         data-dw="1/1" data-fixheight="">
                                                        <div style=";position:relative;" class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="tabcontent_5d735f3fa009573523"
                                                                     style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0" qfyuuid="tabcontent_fwpcw"
                                                                     class="qfy-tabcontent vc-contentarea qfy-element  ">
                                                                    <style>#tabcontent_5d735f3fa009573523 .tabcontent-header-menu li.item button.active {
                                                                            background-color: #c5a862 !important;
                                                                            color: #ffffff !important;
                                                                            border-top: 1px solid rgba(197, 168, 98, 1) !important;
                                                                            border-bottom: 1px solid rgba(197, 168, 98, 1) !important;
                                                                            border-left: 1px solid rgba(197, 168, 98, 1) !important;
                                                                            border-right: 1px solid rgba(197, 168, 98, 1) !important;
                                                                            font-weight: bold !important;
                                                                        }

                                                                        #tabcontent_5d735f3fa009573523 .tabcontent-header-menu li.item button:hover {
                                                                            background-color: #c5a862 !important;
                                                                            color: #ffffff !important;
                                                                            border-top: 1px solid rgba(197, 168, 98, 1) !important;
                                                                            border-bottom: 1px solid rgba(197, 168, 98, 1) !important;
                                                                            border-left: 1px solid rgba(197, 168, 98, 1) !important;
                                                                            border-right: 1px solid rgba(197, 168, 98, 1) !important;
                                                                            font-weight: bold !important;
                                                                        }</style>
                                                                    <div class="tabcontent-header-menu">
                                                                        <div class="tabcontent-outner">
                                                                            <div class="tabcontent-inner"
                                                                                 style="border: 1px solid transparent;overflow:hidden;max-width:100%;display: block;padding:0;margin: 0 auto;width: 100%;position:relative; background-color:transparent;border-radius: 0;">
                                                                                <ul style="text-align:center; display: table;width:100%;table-layout:fixed;">
                                                                                    <li style="display: table-cell;"></li>
                                                                                    <li class="item"
                                                                                        onclick="gototab(this)"
                                                                                        style="display: table-cell; position:relative;width:120px;">
                                                                                        <div style="cursor:pointer;">
                                                                                            <button class="active"
                                                                                                    style="width:120px;border-style: solid;font-size:15px; background-color:#ffffff; color:#666666; border-top:1px solid rgba(153,153,153,1); border-bottom:1px solid rgba(153,153,153,1); border-left:1px solid rgba(153,153,153,1); border-right:1px solid rgba(153,153,153,1); padding:8px 0;">
                                                                                                抗风压
                                                                                            </button>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="itemblank"
                                                                                        style="display: table-cell;width:10px"></li>
                                                                                    <li class="item"
                                                                                        onclick="gototab(this)"
                                                                                        style="display: table-cell; position:relative;width:120px;">
                                                                                        <div style="cursor:pointer;">
                                                                                            <button class=""
                                                                                                    style="width:120px;border-style: solid;font-size:15px; background-color:#ffffff; color:#666666; border-top:1px solid rgba(153,153,153,1); border-bottom:1px solid rgba(153,153,153,1); border-left:1px solid rgba(153,153,153,1); border-right:1px solid rgba(153,153,153,1); padding:8px 0;">
                                                                                                隔音性
                                                                                            </button>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="itemblank"
                                                                                        style="display: table-cell;width:10px"></li>
                                                                                    <li class="item"
                                                                                        onclick="gototab(this)"
                                                                                        style="display: table-cell; position:relative;width:120px;">
                                                                                        <div style="cursor:pointer;">
                                                                                            <button class=""
                                                                                                    style="width:120px;border-style: solid;font-size:15px; background-color:#ffffff; color:#666666; border-top:1px solid rgba(153,153,153,1); border-bottom:1px solid rgba(153,153,153,1); border-left:1px solid rgba(153,153,153,1); border-right:1px solid rgba(153,153,153,1); padding:8px 0;">
                                                                                                抗氧化
                                                                                            </button>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="itemblank"
                                                                                        style="display: table-cell;width:10px"></li>
                                                                                    <li class="item"
                                                                                        onclick="gototab(this)"
                                                                                        style="display: table-cell; position:relative;width:120px;">
                                                                                        <div style="cursor:pointer;">
                                                                                            <button class=""
                                                                                                    style="width:120px;border-style: solid;font-size:15px; background-color:#ffffff; color:#666666; border-top:1px solid rgba(153,153,153,1); border-bottom:1px solid rgba(153,153,153,1); border-left:1px solid rgba(153,153,153,1); border-right:1px solid rgba(153,153,153,1); padding:8px 0;">
                                                                                                气密性
                                                                                            </button>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li style="display: table-cell;"></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div style="margin:0 auto;width:100%;max-width:100%;position:relative;overflow:hidden;z-index:3;min-height:px;margin:40px 0 0 0;padding:0px;"
                                                                         class="royalSlider_gallery_new rsDefault tabroya  played rsAutoHeight rsHor rsFade rsWithBullets"
                                                                         transitionspeed="600" transitiontype="fade"
                                                                         disabledclick="true" auto_play="false" delay=""
                                                                         visiblenearby="false"
                                                                         thumbnails_orientation="horizontal"
                                                                         controlnavigation="bullets" arrowsnav="false"
                                                                         showfullscreen="false" g_loop="true"
                                                                         g_height="300" g_width="300"
                                                                         bgcolor="transparent" arrowsnavautohide="true"
                                                                         autoscaleslider="false"
                                                                         slidesorientation="horizontal"
                                                                         imagescalemode="fit-if-smaller">
                                                                        <div class="rsOverflow"
                                                                             style="width: 1259px; height: 540px; transition: height 2ms ease-in-out 0s;">
                                                                            <div class="rsContainer">
                                                                                <div style="z-index:0;"
                                                                                     class="rsSlide  rsActiveSlide">
                                                                                    <section data-fixheight=""
                                                                                             class="qfy-row-12-5d735f3fa0390180472 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                                                             style="margin-bottom: 0px; border-radius: 0px; color: rgb(56, 41, 36); visibility: visible; opacity: 1; transition: opacity 400ms ease-in-out 0s;">
                                                                                        <style class="row_class qfy_style_class">
                                                                                            @media only screen and (min-width: 992px) {
                                                                                                section.section.qfy-row-12-5d735f3fa0390180472 {
                                                                                                    padding-left: 0;
                                                                                                    padding-right: 0;
                                                                                                    padding-top: 0;
                                                                                                    padding-bottom: 0;
                                                                                                    margin-top: 0;
                                                                                                }
                                                                                            }

                                                                                            @media only screen and (max-width: 992px) {
                                                                                                .bit-html section.section.qfy-row-12-5d735f3fa0390180472 {
                                                                                                    padding-left: 15px;
                                                                                                    padding-right: 15px;
                                                                                                    padding-top: 0;
                                                                                                    padding-bottom: 0;
                                                                                                    margin-top: 0;
                                                                                                    min-height: 0;
                                                                                                }
                                                                                            }
                                                                                        </style>
                                                                                        <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                                                             style="background-color: #ffffff;"></div>

                                                                                        <div class="container">
                                                                                            <div class="row qfe_row">
                                                                                                <div data-animaleinbegin="bottom-in-view"
                                                                                                     data-animalename="qfyfadeInUp"
                                                                                                     data-duration="1"
                                                                                                     data-delay=""
                                                                                                     class=" qfy-column-29-5d735f3fa0667954027 qfy-column-inner  vc_span_class  vc_span8  text-Default small-screen-Default notfullrow"
                                                                                                     data-dw="2/3"
                                                                                                     data-fixheight="">
                                                                                                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                                                                         class="column_inner ">
                                                                                                        <div class=" background-overlay grid-overlay-0"
                                                                                                             style="background-color:transparent;width:100%;"></div>
                                                                                                        <div class="column_containter"
                                                                                                             style="z-index:3;position:relative;">
                                                                                                            <div id="vc_img_5d735f3fa0ea582"
                                                                                                                 style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:5px;padding-bottom:5px;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                                                                 m-padding="5px 0px 5px 0px"
                                                                                                                 p-padding="5px 0 5px 0"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_single_image_zldtp"
                                                                                                                 data-anitime="1"
                                                                                                                 data-ani_iteration_count="1"
                                                                                                                 class="qfy-element bitImageControlDiv ">
                                                                                                                <style>@media only screen and (max-width: 768px) {
                                                                                                                        .single_image_text-5d735f3fa0f86350 .head {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }

                                                                                                                        .single_image_text-5d735f3fa0f86350 .content {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }
                                                                                                                    }</style>
                                                                                                                <a class="bitImageAhover  ">
                                                                                                                    <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_left-to-right vc_align_center qfe_start_animation"
                                                                                                                         style="animation-duration: 1s; animation-iteration-count: 1;">
                                                                                                                        <div class="qfe_wrapper">
                                                                                                                            <span></span><img
                                                                                                                                    width="1000"
                                                                                                                                    height="666"
                                                                                                                                    src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy9hZDEzMmYzNDYwMjJkNjZkNjNlMWQzM2Q2Y2VlMWJkZi5qcGc_p_p100_p_3D.jpg"
                                                                                                                                    class="front_image  attachment-large"
                                                                                                                                    alt="home4"
                                                                                                                                    title=""
                                                                                                                                    description=""
                                                                                                                                    data-title="home4"
                                                                                                                                    src-img=""
                                                                                                                                    style="">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <a class="bitImageAhover  "> </a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                                                                        .qfy-column-29-5d735f3fa0667954027 > .column_inner {
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 44px;
                                                                                                            padding-top: 0;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .qfe_row .vc_span_class.qfy-column-29-5d735f3fa0667954027 {
                                                                                                        }

                                                                                                    ;
                                                                                                    }

                                                                                                    @media only screen and (max-width: 992px) {
                                                                                                        .qfy-column-29-5d735f3fa0667954027 > .column_inner {
                                                                                                            margin: 0 auto 0 !important;
                                                                                                            min-height: 0 !important;
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 0;
                                                                                                            padding-top: 0;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .display_entire .qfe_row .vc_span_class.qfy-column-29-5d735f3fa0667954027 {
                                                                                                        }

                                                                                                        .qfy-column-29-5d735f3fa0667954027 > .column_inner > .background-overlay, .qfy-column-29-5d735f3fa0667954027 > .column_inner > .background-media {
                                                                                                            width: 100% !important;
                                                                                                            left: 0 !important;
                                                                                                            right: auto !important;
                                                                                                        }
                                                                                                    }</style>
                                                                                                <div data-animaleinbegin="bottom-in-view"
                                                                                                     data-animalename="qfyfadeInUp"
                                                                                                     data-duration="1"
                                                                                                     data-delay=""
                                                                                                     class=" qfy-column-30-5d735f3fa1169162094 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-Default notfullrow"
                                                                                                     data-dw="1/3"
                                                                                                     data-fixheight="">
                                                                                                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                                                                         class="column_inner ">
                                                                                                        <div class=" background-overlay grid-overlay-0"
                                                                                                             style="background-color:transparent;width:100%;"></div>
                                                                                                        <div class="column_containter"
                                                                                                             style="z-index:3;position:relative;">
                                                                                                            <div m-padding="5px 0px 5px 0px"
                                                                                                                 p-padding="5px 0px 5px 0px"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_column_text_th6tb"
                                                                                                                 class="qfy-element qfy-text qfy-text-46815 qfe_text_column qfe_content_element  "
                                                                                                                 style="position: relative;;;line-height:1.5em;;background-color: transparent;background-repeat: no-repeat;;padding-top:5px;padding-bottom:5px;">
                                                                                                                <div class="qfe_wrapper">
                                                                                                                    <div>
                                                                                                                        <div>
                                                                                                                            <span style="font-size: 20px;"><strong>门窗艺术，让您生活更精彩</strong></span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div id="vc_img_5d735f3fa1ab19"
                                                                                                                 style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:8px;padding-bottom:14px;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                                                                 m-padding="8px 0px 14px 0px"
                                                                                                                 p-padding="8px 0 14px 0"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_single_image_tfwg3"
                                                                                                                 data-anitime="0.7"
                                                                                                                 data-ani_iteration_count="1"
                                                                                                                 class="qfy-element bitImageControlDiv ">
                                                                                                                <style>@media only screen and (max-width: 768px) {
                                                                                                                        .single_image_text-5d735f3fa1b8a157 .head {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }

                                                                                                                        .single_image_text-5d735f3fa1b8a157 .content {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }
                                                                                                                    }</style>
                                                                                                                <a class="bitImageAhover  ">
                                                                                                                    <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_left">
                                                                                                                        <div class="qfe_wrapper">
                                                                                                                            <span></span><img
                                                                                                                                    class="front_image   ag_image"
                                                                                                                                    src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy82YTMxMmRjZTFhNThmOTcwNGM3YmNiMzc2NzIxYjAwYS02NTB4MzMwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                                                    alt="home8"
                                                                                                                                    description=""
                                                                                                                                    data-attach-id="16555"
                                                                                                                                    data-title="home8"
                                                                                                                                    title=""
                                                                                                                                    src-img=""
                                                                                                                                    style="">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <a class="bitImageAhover  "> </a>
                                                                                                            <div m-padding="5px 0px 5px 0px"
                                                                                                                 p-padding="5px 0px 5px 0px"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_column_text_g5sog"
                                                                                                                 class="qfy-element qfy-text qfy-text-12352 qfe_text_column qfe_content_element  "
                                                                                                                 style="position: relative;;;line-height:1.5em;;background-repeat: no-repeat;;padding-top:5px;padding-bottom:5px;">
                                                                                                                <div class="qfe_wrapper">
                                                                                                                    <div>
                                                                                                                        <div><span style="font-size: 14px;">所有型材均为腔体结构,铝材壁厚高于国家标准;所有铝合金型材都经过时效处理,强度远高于普通铝合金门窗。
使人们轻松建设企业网站专题网站个人网站满足企业品牌宣传个人形象展示产品展示营销推广数据收集</span></div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                                                                        .qfy-column-30-5d735f3fa1169162094 > .column_inner {
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 0;
                                                                                                            padding-top: 60px;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .qfe_row .vc_span_class.qfy-column-30-5d735f3fa1169162094 {
                                                                                                        }

                                                                                                    ;
                                                                                                    }

                                                                                                    @media only screen and (max-width: 992px) {
                                                                                                        .qfy-column-30-5d735f3fa1169162094 > .column_inner {
                                                                                                            margin: 0 auto 0 !important;
                                                                                                            min-height: 0 !important;
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 0;
                                                                                                            padding-top: 20px;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .display_entire .qfe_row .vc_span_class.qfy-column-30-5d735f3fa1169162094 {
                                                                                                        }

                                                                                                        .qfy-column-30-5d735f3fa1169162094 > .column_inner > .background-overlay, .qfy-column-30-5d735f3fa1169162094 > .column_inner > .background-media {
                                                                                                            width: 100% !important;
                                                                                                            left: 0 !important;
                                                                                                            right: auto !important;
                                                                                                        }
                                                                                                    }</style>
                                                                                                <div class="wf-mobile-hidden qfy-clumn-clear"
                                                                                                     style="clear:both;"></div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </section>
                                                                                </div>
                                                                                <div style="z-index:0; display:none; opacity:0;"
                                                                                     class="rsSlide ">
                                                                                    <section data-fixheight=""
                                                                                             class="qfy-row-13-5d735f3fa1fc6558789 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                                                             style="margin-bottom: 0px; border-radius: 0px; color: rgb(56, 41, 36); visibility: visible; opacity: 1; transition: opacity 400ms ease-in-out 0s;">
                                                                                        <style class="row_class qfy_style_class">
                                                                                            @media only screen and (min-width: 992px) {
                                                                                                section.section.qfy-row-13-5d735f3fa1fc6558789 {
                                                                                                    padding-left: 0;
                                                                                                    padding-right: 0;
                                                                                                    padding-top: 0;
                                                                                                    padding-bottom: 0;
                                                                                                    margin-top: 0;
                                                                                                }
                                                                                            }

                                                                                            @media only screen and (max-width: 992px) {
                                                                                                .bit-html section.section.qfy-row-13-5d735f3fa1fc6558789 {
                                                                                                    padding-left: 15px;
                                                                                                    padding-right: 15px;
                                                                                                    padding-top: 0;
                                                                                                    padding-bottom: 0;
                                                                                                    margin-top: 0;
                                                                                                    min-height: 0;
                                                                                                }
                                                                                            }
                                                                                        </style>
                                                                                        <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                                                             style="background-color: #ffffff;"></div>

                                                                                        <div class="container">
                                                                                            <div class="row qfe_row">
                                                                                                <div data-animaleinbegin="bottom-in-view"
                                                                                                     data-animalename="qfyfadeInUp"
                                                                                                     data-duration="1"
                                                                                                     data-delay=""
                                                                                                     class=" qfy-column-31-5d735f3fa22cf789707 qfy-column-inner  vc_span_class  vc_span8  text-Default small-screen-Default notfullrow"
                                                                                                     data-dw="2/3"
                                                                                                     data-fixheight="">
                                                                                                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                                                                         class="column_inner ">
                                                                                                        <div class=" background-overlay grid-overlay-0"
                                                                                                             style="background-color:transparent;width:100%;"></div>
                                                                                                        <div class="column_containter"
                                                                                                             style="z-index:3;position:relative;">
                                                                                                            <div id="vc_img_5d735f3fa2b31928"
                                                                                                                 style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:5px;padding-bottom:5px;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                                                                 m-padding="5px 0px 5px 0px"
                                                                                                                 p-padding="5px 0 5px 0"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_single_image_nngyd"
                                                                                                                 data-anitime="0.7"
                                                                                                                 data-ani_iteration_count="1"
                                                                                                                 class="qfy-element bitImageControlDiv ">
                                                                                                                <style>@media only screen and (max-width: 768px) {
                                                                                                                        .single_image_text-5d735f3fa2c24822 .head {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }

                                                                                                                        .single_image_text-5d735f3fa2c24822 .content {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }
                                                                                                                    }</style>
                                                                                                                <a class="bitImageAhover  ">
                                                                                                                    <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_center">
                                                                                                                        <div class="qfe_wrapper">
                                                                                                                            <span></span><img
                                                                                                                                    class="front_image   ag_image"
                                                                                                                                    src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy83MGI1MmU2ZDQ1NzNjYzZhYzk0ZjIzOGEwMGRiYTE4Mi05MDB4NjAwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                                                    alt="products-2"
                                                                                                                                    description=""
                                                                                                                                    data-attach-id="15434"
                                                                                                                                    data-title="products-2"
                                                                                                                                    title=""
                                                                                                                                    src-img=""
                                                                                                                                    style="">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <a class="bitImageAhover  "> </a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                                                                        .qfy-column-31-5d735f3fa22cf789707 > .column_inner {
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 44px;
                                                                                                            padding-top: 0;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .qfe_row .vc_span_class.qfy-column-31-5d735f3fa22cf789707 {
                                                                                                        }

                                                                                                    ;
                                                                                                    }

                                                                                                    @media only screen and (max-width: 992px) {
                                                                                                        .qfy-column-31-5d735f3fa22cf789707 > .column_inner {
                                                                                                            margin: 0 auto 0 !important;
                                                                                                            min-height: 0 !important;
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 0;
                                                                                                            padding-top: 0;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .display_entire .qfe_row .vc_span_class.qfy-column-31-5d735f3fa22cf789707 {
                                                                                                        }

                                                                                                        .qfy-column-31-5d735f3fa22cf789707 > .column_inner > .background-overlay, .qfy-column-31-5d735f3fa22cf789707 > .column_inner > .background-media {
                                                                                                            width: 100% !important;
                                                                                                            left: 0 !important;
                                                                                                            right: auto !important;
                                                                                                        }
                                                                                                    }</style>
                                                                                                <div data-animaleinbegin="bottom-in-view"
                                                                                                     data-animalename="qfyfadeInUp"
                                                                                                     data-duration="1"
                                                                                                     data-delay=""
                                                                                                     class=" qfy-column-32-5d735f3fa2e7c177026 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-Default notfullrow"
                                                                                                     data-dw="1/3"
                                                                                                     data-fixheight="">
                                                                                                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                                                                         class="column_inner ">
                                                                                                        <div class=" background-overlay grid-overlay-0"
                                                                                                             style="background-color:transparent;width:100%;"></div>
                                                                                                        <div class="column_containter"
                                                                                                             style="z-index:3;position:relative;">
                                                                                                            <div m-padding="5px 0px 5px 0px"
                                                                                                                 p-padding="5px 0px 5px 0px"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_column_text_ym254"
                                                                                                                 class="qfy-element qfy-text qfy-text-94072 qfe_text_column qfe_content_element  "
                                                                                                                 style="position: relative;;;line-height:1.5em;;background-color: transparent;background-repeat: no-repeat;;padding-top:5px;padding-bottom:5px;">
                                                                                                                <div class="qfe_wrapper">
                                                                                                                    <div>
                                                                                                                        <div>
                                                                                                                            <span style="font-size: 20px;"><strong>遮风挡雨，为您缔造安逸空间</strong></span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div id="vc_img_5d735f3fa3828577"
                                                                                                                 style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:29px;padding-bottom:14px;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                                                                 m-padding="29px 0px 14px 0px"
                                                                                                                 p-padding="29px 0 14px 0"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_single_image_ccucp"
                                                                                                                 data-anitime="0.7"
                                                                                                                 data-ani_iteration_count="1"
                                                                                                                 class="qfy-element bitImageControlDiv ">
                                                                                                                <style>@media only screen and (max-width: 768px) {
                                                                                                                        .single_image_text-5d735f3fa3904434 .head {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }

                                                                                                                        .single_image_text-5d735f3fa3904434 .content {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }
                                                                                                                    }</style>
                                                                                                                <a class="bitImageAhover  ">
                                                                                                                    <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_left">
                                                                                                                        <div class="qfe_wrapper">
                                                                                                                            <span></span><img
                                                                                                                                    class="front_image   ag_image"
                                                                                                                                    src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8yMDM4YzVmNzc3MzdhYTE1NjkyNWNhOTRkZjg2NGQ2Yi0yNzZ4MzMwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                                                    alt="home9"
                                                                                                                                    description=""
                                                                                                                                    data-attach-id="16578"
                                                                                                                                    data-title="home9"
                                                                                                                                    title=""
                                                                                                                                    src-img=""
                                                                                                                                    style="">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <a class="bitImageAhover  "> </a>
                                                                                                            <div m-padding="5px 0px 5px 0px"
                                                                                                                 p-padding="5px 0px 5px 0px"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_column_text_w8347"
                                                                                                                 class="qfy-element qfy-text qfy-text-72503 qfe_text_column qfe_content_element  "
                                                                                                                 style="position: relative;;;line-height:1.5em;;background-repeat: no-repeat;;padding-top:5px;padding-bottom:5px;">
                                                                                                                <div class="qfe_wrapper">
                                                                                                                    <div>
                                                                                                                        <div><span style="font-size: 14px;">所有型材均为腔体结构,铝材壁厚高于国家标准;所有铝合金型材都经过时效处理,强度远高于普通铝合金门窗。
使人们轻松建设企业网站专题网站个人网站满足企业品牌宣传个人形象展示产品展示营销推广数据收集</span></div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                                                                        .qfy-column-32-5d735f3fa2e7c177026 > .column_inner {
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 0;
                                                                                                            padding-top: 60px;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .qfe_row .vc_span_class.qfy-column-32-5d735f3fa2e7c177026 {
                                                                                                        }

                                                                                                    ;
                                                                                                    }

                                                                                                    @media only screen and (max-width: 992px) {
                                                                                                        .qfy-column-32-5d735f3fa2e7c177026 > .column_inner {
                                                                                                            margin: 0 auto 0 !important;
                                                                                                            min-height: 0 !important;
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 0;
                                                                                                            padding-top: 20px;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .display_entire .qfe_row .vc_span_class.qfy-column-32-5d735f3fa2e7c177026 {
                                                                                                        }

                                                                                                        .qfy-column-32-5d735f3fa2e7c177026 > .column_inner > .background-overlay, .qfy-column-32-5d735f3fa2e7c177026 > .column_inner > .background-media {
                                                                                                            width: 100% !important;
                                                                                                            left: 0 !important;
                                                                                                            right: auto !important;
                                                                                                        }
                                                                                                    }</style>
                                                                                                <div class="wf-mobile-hidden qfy-clumn-clear"
                                                                                                     style="clear:both;"></div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </section>
                                                                                </div>
                                                                                <div style="z-index:0; display:none; opacity:0;"
                                                                                     class="rsSlide ">
                                                                                    <section data-fixheight=""
                                                                                             class="qfy-row-15-5d735f3fa5273652525 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                                                             style="margin-bottom:0;border-radius:0px;color:#382924;">
                                                                                        <style class="row_class qfy_style_class">
                                                                                            @media only screen and (min-width: 992px) {
                                                                                                section.section.qfy-row-15-5d735f3fa5273652525 {
                                                                                                    padding-left: 0;
                                                                                                    padding-right: 0;
                                                                                                    padding-top: 0;
                                                                                                    padding-bottom: 0;
                                                                                                    margin-top: 0;
                                                                                                }
                                                                                            }

                                                                                            @media only screen and (max-width: 992px) {
                                                                                                .bit-html section.section.qfy-row-15-5d735f3fa5273652525 {
                                                                                                    padding-left: 15px;
                                                                                                    padding-right: 15px;
                                                                                                    padding-top: 0;
                                                                                                    padding-bottom: 0;
                                                                                                    margin-top: 0;
                                                                                                    min-height: 0;
                                                                                                }
                                                                                            }
                                                                                        </style>
                                                                                        <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                                                             style="background-color: #ffffff;"></div>

                                                                                        <div class="container">
                                                                                            <div class="row qfe_row">
                                                                                                <div data-animaleinbegin="bottom-in-view"
                                                                                                     data-animalename="qfyfadeInUp"
                                                                                                     data-duration="1"
                                                                                                     data-delay=""
                                                                                                     class=" qfy-column-35-5d735f3fa559c82988 qfy-column-inner  vc_span_class  vc_span8  text-Default small-screen-Default notfullrow"
                                                                                                     data-dw="2/3"
                                                                                                     data-fixheight="">
                                                                                                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                                                                         class="column_inner ">
                                                                                                        <div class=" background-overlay grid-overlay-0"
                                                                                                             style="background-color:transparent;width:100%;"></div>
                                                                                                        <div class="column_containter"
                                                                                                             style="z-index:3;position:relative;">
                                                                                                            <div id="vc_img_5d735f3fa5936753"
                                                                                                                 style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:5px;padding-bottom:5px;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                                                                 m-padding="5px 0px 5px 0px"
                                                                                                                 p-padding="5px 0 5px 0"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_single_image_yspht"
                                                                                                                 data-anitime="0.7"
                                                                                                                 data-ani_iteration_count="1"
                                                                                                                 class="qfy-element bitImageControlDiv ">
                                                                                                                <style>@media only screen and (max-width: 768px) {
                                                                                                                        .single_image_text-5d735f3fa5a0f726 .head {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }

                                                                                                                        .single_image_text-5d735f3fa5a0f726 .content {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }
                                                                                                                    }</style>
                                                                                                                <a class="bitImageAhover  ">
                                                                                                                    <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_center">
                                                                                                                        <div class="qfe_wrapper">
                                                                                                                            <span></span><img
                                                                                                                                    class="front_image   ag_image"
                                                                                                                                    src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy83MGI1MmU2ZDQ1NzNjYzZhYzk0ZjIzOGEwMGRiYTE4Mi05MDB4NjAwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                                                    alt="products-2"
                                                                                                                                    description=""
                                                                                                                                    data-attach-id="15434"
                                                                                                                                    data-title="products-2"
                                                                                                                                    title=""
                                                                                                                                    src-img=""
                                                                                                                                    style="">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <a class="bitImageAhover  "> </a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                                                                        .qfy-column-35-5d735f3fa559c82988 > .column_inner {
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 44px;
                                                                                                            padding-top: 0;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .qfe_row .vc_span_class.qfy-column-35-5d735f3fa559c82988 {
                                                                                                        }

                                                                                                    ;
                                                                                                    }

                                                                                                    @media only screen and (max-width: 992px) {
                                                                                                        .qfy-column-35-5d735f3fa559c82988 > .column_inner {
                                                                                                            margin: 0 auto 0 !important;
                                                                                                            min-height: 0 !important;
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 0;
                                                                                                            padding-top: 0;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .display_entire .qfe_row .vc_span_class.qfy-column-35-5d735f3fa559c82988 {
                                                                                                        }

                                                                                                        .qfy-column-35-5d735f3fa559c82988 > .column_inner > .background-overlay, .qfy-column-35-5d735f3fa559c82988 > .column_inner > .background-media {
                                                                                                            width: 100% !important;
                                                                                                            left: 0 !important;
                                                                                                            right: auto !important;
                                                                                                        }
                                                                                                    }</style>
                                                                                                <div data-animaleinbegin="bottom-in-view"
                                                                                                     data-animalename="qfyfadeInUp"
                                                                                                     data-duration="1"
                                                                                                     data-delay=""
                                                                                                     class=" qfy-column-36-5d735f3fa5c01263460 qfy-column-inner  vc_span_class  vc_span4  text-Default small-screen-Default notfullrow"
                                                                                                     data-dw="1/3"
                                                                                                     data-fixheight="">
                                                                                                    <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                                                                         class="column_inner ">
                                                                                                        <div class=" background-overlay grid-overlay-0"
                                                                                                             style="background-color:transparent;width:100%;"></div>
                                                                                                        <div class="column_containter"
                                                                                                             style="z-index:3;position:relative;">
                                                                                                            <div m-padding="5px 0px 5px 0px"
                                                                                                                 p-padding="5px 0px 5px 0px"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_column_text_7b8vi"
                                                                                                                 class="qfy-element qfy-text qfy-text-70666 qfe_text_column qfe_content_element  "
                                                                                                                 style="position: relative;;;line-height:1.5em;;background-color: transparent;background-repeat: no-repeat;;padding-top:5px;padding-bottom:5px;">
                                                                                                                <div class="qfe_wrapper">
                                                                                                                    <div>
                                                                                                                        <div>
                                                                                                                            <span style="font-size: 20px;"><strong>遮风挡雨，为您缔造安逸空间</strong></span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div id="vc_img_5d735f3fa6199809"
                                                                                                                 style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:29px;padding-bottom:14px;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                                                                 m-padding="29px 0px 14px 0px"
                                                                                                                 p-padding="29px 0 14px 0"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_single_image_yby9g"
                                                                                                                 data-anitime="0.7"
                                                                                                                 data-ani_iteration_count="1"
                                                                                                                 class="qfy-element bitImageControlDiv ">
                                                                                                                <style>@media only screen and (max-width: 768px) {
                                                                                                                        .single_image_text-5d735f3fa6271614 .head {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }

                                                                                                                        .single_image_text-5d735f3fa6271614 .content {
                                                                                                                            font-size: 16px !important;
                                                                                                                        }
                                                                                                                    }</style>
                                                                                                                <a class="bitImageAhover  ">
                                                                                                                    <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_left">
                                                                                                                        <div class="qfe_wrapper">
                                                                                                                            <span></span><img
                                                                                                                                    class="front_image   ag_image"
                                                                                                                                    src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy8yMDM4YzVmNzc3MzdhYTE1NjkyNWNhOTRkZjg2NGQ2Yi0yNzZ4MzMwLmpwZw_p_p100_p_3D_p_p100_p_3D.jpg"
                                                                                                                                    alt="home9"
                                                                                                                                    description=""
                                                                                                                                    data-attach-id="16578"
                                                                                                                                    data-title="home9"
                                                                                                                                    title=""
                                                                                                                                    src-img=""
                                                                                                                                    style="">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <a class="bitImageAhover  "> </a>
                                                                                                            <div m-padding="5px 0px 5px 0px"
                                                                                                                 p-padding="5px 0px 5px 0px"
                                                                                                                 css_animation_delay="0"
                                                                                                                 qfyuuid="qfy_column_text_4nkfp"
                                                                                                                 class="qfy-element qfy-text qfy-text-86875 qfe_text_column qfe_content_element  "
                                                                                                                 style="position: relative;;;line-height:1.5em;;background-repeat: no-repeat;;padding-top:5px;padding-bottom:5px;">
                                                                                                                <div class="qfe_wrapper">
                                                                                                                    <div>
                                                                                                                        <div><span style="font-size: 14px;">所有型材均为腔体结构,铝材壁厚高于国家标准;所有铝合金型材都经过时效处理,强度远高于普通铝合金门窗。
使人们轻松建设企业网站专题网站个人网站满足企业品牌宣传个人形象展示产品展示营销推广数据收集</span></div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                                                                        .qfy-column-36-5d735f3fa5c01263460 > .column_inner {
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 0;
                                                                                                            padding-top: 60px;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .qfe_row .vc_span_class.qfy-column-36-5d735f3fa5c01263460 {
                                                                                                        }

                                                                                                    ;
                                                                                                    }

                                                                                                    @media only screen and (max-width: 992px) {
                                                                                                        .qfy-column-36-5d735f3fa5c01263460 > .column_inner {
                                                                                                            margin: 0 auto 0 !important;
                                                                                                            min-height: 0 !important;
                                                                                                            padding-left: 0;
                                                                                                            padding-right: 0;
                                                                                                            padding-top: 20px;
                                                                                                            padding-bottom: 0;
                                                                                                        }

                                                                                                        .display_entire .qfe_row .vc_span_class.qfy-column-36-5d735f3fa5c01263460 {
                                                                                                        }

                                                                                                        .qfy-column-36-5d735f3fa5c01263460 > .column_inner > .background-overlay, .qfy-column-36-5d735f3fa5c01263460 > .column_inner > .background-media {
                                                                                                            width: 100% !important;
                                                                                                            left: 0 !important;
                                                                                                            right: auto !important;
                                                                                                        }
                                                                                                    }</style>
                                                                                                <div class="wf-mobile-hidden qfy-clumn-clear"
                                                                                                     style="clear:both;"></div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </section>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="rsNav rsBullets">
                                                                            <div class="rsNavItem rsBullet rsNavSelected">
                                                                                <span></span></div>
                                                                            <div class="rsNavItem rsBullet">
                                                                                <span></span></div>
                                                                            <div class="rsNavItem rsBullet">
                                                                                <span></span></div>
                                                                            <div class="rsNavItem rsBullet">
                                                                                <span></span></div>
                                                                        </div>
                                                                        <div style="clear:both; float: none;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-28-5d735f3f9feaf701958 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-28-5d735f3f9feaf701958 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-28-5d735f3f9feaf701958 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top:;
                                                                padding-bottom:;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-28-5d735f3f9feaf701958 {
                                                            }

                                                            .qfy-column-28-5d735f3f9feaf701958 > .column_inner > .background-overlay, .qfy-column-28-5d735f3f9feaf701958 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                </div>
                                            </div>

                                        </section>
                                        <section data-fixheight=""
                                                 class="qfy-row-16-5d735f3fa67cc276878 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_gl5ng"
                                                 style="margin-bottom:0;border-radius:0px;color:#ffffff;">
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-16-5d735f3fa67cc276878 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 0;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-16-5d735f3fa67cc276878 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 0;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div data-time="3" data-imagebgs="" class="background-media   "
                                                 backgroundsize="true"
                                                 style="background-image: url(&#39;https://ccdn.goodq.top/caches/b15bd6c05a6cc12948141d4271734ac7/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNy81ODI5NmY5YzE4NjUxNmNhNzkzYzFjN2I4Yzk0ZWRkNC5qcGc_p_p100_p_3D.jpg&#39;); background-repeat:no-repeat; background-size:cover; background-attachment:fixed; background-position:center center;">
                                            </div>

                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: #382924;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="bottom-in-view"
                                                         data-animalename="qfyfadeInDown" data-duration="1"
                                                         data-delay=""
                                                         class=" qfy-column-37-5d735f3fa6cdd622874 qfy-column-inner  vc_span_class  vc_span6  text-Default small-screen-Default column_middle notfullrow"
                                                         data-dw="1/2" data-fixheight=""
                                                         style="margin-top: 59px; margin-bottom: 0px;">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:10px;z-index:4;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:#382924;width:100%;border-radius:10px;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;border-radius:10px;">
                                                                <div id="vc_header_5d735f3fa701f25"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0" qfyuuid="qfy_header_7ece2"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom qfe_start_animation"
                                                                     style="position: relative; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 10px; padding: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3fa701f25 .header_title {
                                                                                font-size: 16px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3fa701f25 .header_subtitle {
                                                                                font-size: 30px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title left mobileleft">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_title"
                                                                                         style="font-family:微软雅黑;font-size:16px;font-weight:normal;font-style:normal;color:#c5a862;padding:0 0 10px 0;display:block;padding:0 0 10px 0;vertical-align:bottom;">
                                                                                        高端隔音门窗
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-family:微软雅黑;font-size:36px;font-weight:bold;font-style:normal;color:#ffffff;display:block;vertical-align:bottom;">
                                                                                        门窗定制服务，请填写以下信息
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div m-padding="0px 0px 20px 0px" p-padding="0 0 20px 0"
                                                                     css_animation_delay="0.2"
                                                                     qfyuuid="qfy_column_text_obopa-c-6q"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-40040 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.2 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 0px 0px 20px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="letter-spacing: 1px;"><span
                                                                                    style="font-size: 14px;"><em>您可以通过留言的方式,来咨询产品,我们工作人员看到后会第一时间和您联系</em></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-tag="iphorm_vc" data-id="68"
                                                                     m-padding="5px 0px 0px 0px" p-padding="5px 0 0 0"
                                                                     css_animation_delay="0" qfyuuid="iphorm_vc_zk3gy"
                                                                     class="qfy-element qfe_layerslider_element qfe_content_element"
                                                                     style="margin-top:0;margin-bottom:0;padding-top:5px;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <div id="iphorm-outer-5d735f3fa9a9a"
                                                                         class="bitWidgetFrame iphorm-outer iphorm-outer-68 iphorm-uniform-theme-default">

                                                                        <form id="iphorm-5d735f3fa9a9a"
                                                                              class="iphorm iphorm-form-68"
                                                                              action="http://www.yzhpps.com/#iphorm-5d735f3fa9a9a"
                                                                              method="post"
                                                                              enctype="multipart/form-data"
                                                                              data-check="">
                                                                            <div class="iphorm-inner iphorm-inner-68">
                                                                                <input type="hidden" name="iphorm_id"
                                                                                       value="68">
                                                                                <input type="hidden" name="iphorm_uid"
                                                                                       value="5d735f3fa9a9a">
                                                                                <input type="hidden" name="form_url"
                                                                                       value="http://www.yzhpps.com/">
                                                                                <input type="hidden"
                                                                                       name="referring_url"
                                                                                       value="http://www.yzhpps.com/?page_id=11626">
                                                                                <input type="hidden" name="post_id"
                                                                                       value="11965">
                                                                                <input type="hidden" name="post_title"
                                                                                       value="首页">

                                                                                <div class="iphorm-success-message iphorm-hidden"></div>
                                                                                <div class="iphorm-error-message iphorm-hidden"></div>
                                                                                <input type="hidden" class="popaction"
                                                                                       value="">
                                                                                <div class="iphorm-elements iphorm-elements-68 iphorm-clearfix">
                                                                                    <div class="iphorm-group-wrap iphorm_68_6-group-wrap iphorm-clearfix iphorm-labels-inside iphorm-group-style-plain iphorm-group-alignment-proportional">
                                                                                        <div class="iphorm-group-elements">
                                                                                            <div class="iphorm-group-row iphorm-clearfix iphorm-group-row-2cols">
                                                                                                <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_68_1-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-required">
                                                                                                    <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_68_1-element-spacer">
                                                                                                        <label for="iphorm_68_1_5d735f3fa9a9a"
                                                                                                               style="color: rgb(204, 204, 204);font-size: 14px;line-height:60px;margin-top:0;;">
                                                                                                            &nbsp;&nbsp;姓名
                                                                                                            <span class="iphorm-required"
                                                                                                                  style="color: #c72c2c;font-size: 14px;;">*</span>
                                                                                                        </label>
                                                                                                        <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_68_1-input-wrap">
                                                                                                            <input class="iphorm-element-text  iphorm_68_1"
                                                                                                                   type="text"
                                                                                                                   name="username"
                                                                                                                   value=""
                                                                                                                   placeholder=""
                                                                                                                   style="background-color: rgb(255, 255, 255);border: 1px solid rgb(214, 214, 214);color: rgb(115, 113, 115);height: 60px;font-size: 14px;">
                                                                                                        </div>
                                                                                                        <div class="iphorm-errors-wrap iphorm-hidden">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_68_8-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-required last-child">
                                                                                                    <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_68_8-element-spacer">
                                                                                                        <label for="iphorm_68_8_5d735f3fa9a9a"
                                                                                                               style="color: rgb(204, 204, 204);font-size: 14px;line-height:60px;margin-top:0;;">
                                                                                                            &nbsp;&nbsp;电话
                                                                                                            <span class="iphorm-required"
                                                                                                                  style="color: #c72c2c;font-size: 14px;;">*</span>
                                                                                                        </label>
                                                                                                        <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_68_8-input-wrap">
                                                                                                            <input class="iphorm-element-text  iphorm_68_8"
                                                                                                                   id="iphorm_68_8_5d735f3fa9a9a"
                                                                                                                   type="text"
                                                                                                                   name="tel"
                                                                                                                   value=""
                                                                                                                   placeholder=""
                                                                                                                   style="background-color: rgb(255, 255, 255);border: 1px solid rgb(214, 214, 214);color: rgb(115, 113, 115);height: 60px;font-size: 14px;">
                                                                                                        </div>
                                                                                                        <div class="iphorm-errors-wrap iphorm-hidden">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="iphorm-group-row iphorm-clearfix iphorm-group-row-2cols last-child">
                                                                                                <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_68_9-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-optional">
                                                                                                    <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_68_9-element-spacer">
                                                                                                        <label for="iphorm_68_9_5d735f3fa9a9a"
                                                                                                               style="color: rgb(204, 204, 204);font-size: 14px;line-height:60px;margin-top:0;;">
                                                                                                            &nbsp;&nbsp;邮箱 </label>
                                                                                                        <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_68_9-input-wrap">
                                                                                                            <input class="iphorm-element-text  iphorm_68_9"
                                                                                                                   id="iphorm_68_9_5d735f3fa9a9a"
                                                                                                                   type="text"
                                                                                                                   name="email"
                                                                                                                   value=""
                                                                                                                   placeholder=""
                                                                                                                   style="background-color: rgb(255, 255, 255);border: 1px solid rgb(214, 214, 214);color: rgb(115, 113, 115);height: 60px;font-size: 14px;">
                                                                                                        </div>
                                                                                                        <div class="iphorm-errors-wrap iphorm-hidden">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_68_10-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-optional last-child">
                                                                                                    <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_68_10-element-spacer">
                                                                                                        <label for="iphorm_68_10_5d735f3fa9a9a"
                                                                                                               style="color: rgb(204, 204, 204);font-size: 14px;line-height:60px;margin-top:0;;">
                                                                                                            &nbsp;&nbsp;咨询产品名称 </label>
                                                                                                        <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_68_10-input-wrap">
                                                                                                            <input class="iphorm-element-text  iphorm_68_10"
                                                                                                                   id="iphorm_68_10_5d735f3fa9a9a"
                                                                                                                   type="text"
                                                                                                                   name="product"
                                                                                                                   value=""
                                                                                                                   placeholder=""
                                                                                                                   style="background-color: rgb(255, 255, 255);border: 1px solid rgb(214, 214, 214);color: rgb(115, 113, 115);height: 60px;font-size: 14px;">
                                                                                                        </div>
                                                                                                        <div class="iphorm-errors-wrap iphorm-hidden">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <!-- /.iphorm-group-row -->
                                                                                        </div>
                                                                                        <!-- /.iphorm-group-elements -->
                                                                                    </div> <!-- /.iphorm-group-wrap -->
                                                                                    <div class="iphorm-element-wrap iphorm-element-wrap-textarea iphorm_68_11-element-wrap iphorm-clearfix iphorm-labels-inside iphorm-element-optional">

                                                                                        <div class="iphorm-element-spacer iphorm-element-spacer-textarea iphorm_68_11-element-spacer">
                                                                                            <label for="iphorm_68_11_5d735f3fa9a9a"
                                                                                                   style="color: rgb(204, 204, 204);font-size: 14px;line-height:14px;">
                                                                                                &nbsp;&nbsp;备注 </label>
                                                                                            <div class="iphorm-input-wrap iphorm-input-wrap-textarea iphorm_68_11-input-wrap">
                                                                                                <textarea placeholder=""
                                                                                                          class="iphorm-element-textarea  iphorm_68_11"
                                                                                                          id="iphorm_68_11_5d735f3fa9a9a"
                                                                                                          name="remark"
                                                                                                          style="background-color: rgb(255, 255, 255);border: 1px solid rgb(214, 214, 214);color: rgb(115, 113, 115);font-size: 14px;"
                                                                                                          rows="3"
                                                                                                          cols="25"></textarea>
                                                                                            </div>
                                                                                            <div class="iphorm-errors-wrap iphorm-hidden">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="iphorm-hidden">
                                                                                        <label>这个输入框应该是留空的<input
                                                                                                    type="text"
                                                                                                    name="iphorm_68_0"></label>
                                                                                    </div>
                                                                                    <div class="iphorm-submit-wrap iphorm-submit-wrap-68 iphorm-clearfix">

                                                                                        <div class="iphorm-submit-input-wrap iphorm-submit-input-wrap-68">

                                                                                            <button class="iphorm-submit-element"
                                                                                                    style="width:100%;"
                                                                                                    type="submit"
                                                                                                    name="iphorm_submit">
                                                                                                <span style="font-size: 15px;line-height:15px;;padding-left: 50px !important;;background-color: #c5a762;border: 1px solid #c5a762;border-radius: 10px;overflow: hidden;"><em
                                                                                                            style="color: #ffffff;font-size: 15px;line-height:15px;padding:20px 50px 20px 0;;background-color: #c5a762;">立 即 提 交 →</em></span>
                                                                                            </button>

                                                                                            <div style="display:inline-block;">
                                                                                                <div class="iphorm-loading-wrap">
                                                                                                    <span class="iphorm-loading">请稍候</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                    </div>
                                                                                    <style>.iphorm-elements-68, .iphorm-elements-68 button, .iphorm-elements-68 input, .iphorm-elements-68 select, .iphorm-elements-68 textarea {
                                                                                            font-family: 微软雅黑;
                                                                                            font-style: normal
                                                                                        }

                                                                                        .iphorm-submit-wrap-68 button.iphorm-submit-element:hover span, .iphorm-submit-wrap-68 button.iphorm-submit-element:hover em {
                                                                                            background-color: #c5a762 !important;
                                                                                        }

                                                                                        .iphorm-submit-wrap-68 button.iphorm-submit-element:hover span, .iphorm-submit-wrap-68 button.iphorm-submit-element:hover em {
                                                                                            border-color: #c5a762 !important;
                                                                                        }

                                                                                        .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap-text input, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap-captcha input, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap-password input, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap select, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap textarea {
                                                                                            width: 100%;
                                                                                            max-width: 100% !important;
                                                                                        }

                                                                                        .iphorm-elements.iphorm-elements-68 .iphorm-group-wrap, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap-text, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap-captcha, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap-password, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap {
                                                                                            width: 100% !important;
                                                                                            max-width: 100% !important;
                                                                                        }

                                                                                        .iphorm-submit-wrap-68 .iphorm-submit-input-wrap {
                                                                                            width: 100% !important;
                                                                                            max-width: 100% !important;
                                                                                        }

                                                                                        .iphorm-submit-wrap-68 .iphorm-submit-input-wrap button {
                                                                                            margin: 0;
                                                                                        }

                                                                                        .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap-text input, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap-captcha input, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap-password input, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap select, .iphorm-elements.iphorm-elements-68 .iphorm-element-wrap textarea {
                                                                                            border-radius: 10px;
                                                                                        }

                                                                                        .iphorm-elements-68 .iphorm-element-wrap-text input,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap-captcha input,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap-password input,
                                                                                        .iphorm-elements-68 .iphorm-input-wrap-select select,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap select, .iphorm-elements .iphorm-element-wrap textarea {
                                                                                            box-shadow: none !important;
                                                                                        }

                                                                                        .iphorm-elements-68 .iphorm-element-wrap-text input,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap-captcha input,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap-password input,
                                                                                        .iphorm-elements-68 .iphorm-input-wrap-select select,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap select,
                                                                                        .iphorm-elements-68 .iphorm-input-wrap-select select,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap textarea {
                                                                                            background-color: rgba(255, 255, 255, 0.95) !important;
                                                                                        }

                                                                                        .iphorm-elements-68 .iphorm-element-wrap-text input,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap-captcha input,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap-password input,
                                                                                        .iphorm-elements-68 .iphorm-input-wrap-select select,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap select,
                                                                                        .iphorm-elements-68 .iphorm-input-wrap-select select,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap textarea,
                                                                                        .iphorm-elements-68 .iphorm-input-li > label,
                                                                                        .iphorm-elements-68 .iphorm-datepicker-icon,
                                                                                        .iphorm-elements-68 .iphorm-input-wrap > .iphorm-element-file-inner > div.uploader > span {
                                                                                            color: #737173;
                                                                                        }

                                                                                        .iphorm-elements-68 .iphorm-input-li > label {
                                                                                            font-size: 14px;
                                                                                        }

                                                                                        .iphorm-elements-68 .iphorm-element-spacer {
                                                                                            padding-bottom: 10px !important;
                                                                                        }

                                                                                        .iphorm-elements-68 .iphorm-element-wrap-text input,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap-captcha input,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap-password input,
                                                                                        .iphorm-elements-68 .iphorm-input-wrap-select select,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap select,
                                                                                        .iphorm-elements-68 .iphorm-input-wrap-select select,
                                                                                        .iphorm-elements-68 .iphorm-element-wrap textarea {
                                                                                            border-width: 1px !important;
                                                                                            border-color: #d6d6d6 !important;

                                                                                        }

                                                                                        .iphorm-elements-68 .iphorm-submit-input-wrap {
                                                                                            float: none !important;
                                                                                            text-align: center !important;
                                                                                        }</style>
                                                                                </div>

                                                                            </div>
                                                                        </form>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-37-5d735f3fa6cdd622874 > .column_inner {
                                                                padding-left: 10vw;
                                                                padding-right: 10vw;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-37-5d735f3fa6cdd622874 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-37-5d735f3fa6cdd622874 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 40px;
                                                                padding-right: 40px;
                                                                padding-top: 40px;
                                                                padding-bottom: 40px;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-37-5d735f3fa6cdd622874 {
                                                            }

                                                            .qfy-column-37-5d735f3fa6cdd622874 > .column_inner > .background-overlay, .qfy-column-37-5d735f3fa6cdd622874 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                    <div data-animaleinbegin="bottom-in-view"
                                                         data-animalename="qfyfadeInUp" data-duration="1" data-delay=""
                                                         class=" qfy-column-38-5d735f3fab044181663 qfy-column-inner  vc_span_class  vc_span6  text-Default small-screen-Default notfullrow"
                                                         data-dw="1/2" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_img_5d735f3fab864816"
                                                                     style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_single_image_cyel1" data-anitime="0.7"
                                                                     data-ani_iteration_count="1"
                                                                     class="qfy-element bitImageControlDiv ">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            .single_image_text-5d735f3fab939328 .head {
                                                                                font-size: 16px !important;
                                                                            }

                                                                            .single_image_text-5d735f3fab939328 .content {
                                                                                font-size: 16px !important;
                                                                            }
                                                                        }</style>
                                                                    <a class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element widthfull vc_align_center">
                                                                            <div class="qfe_wrapper"><span></span><img
                                                                                        width="1172" height="1127"
                                                                                        src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wOC9kNjBkYzRjYzk1MjM5MzczNjE0MjkyYTZlZDk5N2YyMC5qcGc_p_p100_p_3D.jpg"
                                                                                        class="front_image  attachment-widthfull"
                                                                                        alt="banner5" title=""
                                                                                        description=""
                                                                                        data-title="banner5" src-img=""
                                                                                        style=""></div>
                                                                        </div>
                                                                    </a></div>
                                                                <a class="bitImageAhover  "> </a></div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-38-5d735f3fab044181663 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-38-5d735f3fab044181663 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-38-5d735f3fab044181663 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 34px;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-38-5d735f3fab044181663 {
                                                            }

                                                            .qfy-column-38-5d735f3fab044181663 > .column_inner > .background-overlay, .qfy-column-38-5d735f3fab044181663 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                    <div class="wf-mobile-hidden qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                </div>
                                            </div>

                                        </section>
                                        <section data-fixheight=""
                                                 class="qfy-row-17-5d735f3fabb693509 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                                 id="bit_ywegu"
                                                 style="margin-bottom:0;border-radius:0px;color:#382924;">
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-17-5d735f3fabb693509 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 15vh;
                                                        padding-bottom: 15vh;
                                                        margin-top: 0;
                                                    }

                                                    section.section.qfy-row-17-5d735f3fabb693509 > .container {
                                                        max-width: 1600px;
                                                        margin: 0 auto;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-17-5d735f3fabb693509 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 40px;
                                                        padding-bottom: 40px;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: #ffffff;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="bottom-in-view"
                                                         data-animalename="qfyfadeInUp" data-duration="" data-delay=""
                                                         class=" qfy-column-39-5d735f3fabdee992661 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                                                         data-dw="1/1" data-fixheight="">
                                                        <div style=";position:relative;" class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div id="vc_header_5d735f3fac0e4269"
                                                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0" qfyuuid="qfy_header_2wylr"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element minheigh1px qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom qfe_start_animation"
                                                                     style="position: relative; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3fac0e4269 .header_title {
                                                                                font-size: 14px !important;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 768px) {
                                                                            #vc_header_5d735f3fac0e4269 .header_subtitle {
                                                                                font-size: 30px !important;
                                                                            }
                                                                        }</style>
                                                                    <div class="qfe_wrapper">
                                                                        <div class="qfy_title center mobilecenter">
                                                                            <div class="qfy_title_image_warpper"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="qfy_title_inner"
                                                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                                                    <div class="header_title"
                                                                                         style="font-family:arial;font-size:16px;font-weight:bold;font-style:normal;color:#c5a862;display:block;vertical-align:bottom;">
                                                                                        Latest News
                                                                                    </div>
                                                                                    <div class="header_subtitle"
                                                                                         style="font-size:36px;font-weight:bold;font-style:normal;color:#382924;display:block;vertical-align:bottom;">
                                                                                        最新动态
                                                                                    </div>
                                                                                </div>
                                                                                <style></style>
                                                                                <div class="button_wrapper"
                                                                                     style="display:none;">
                                                                                    <div class="button "
                                                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                        + 查看更多
                                                                                    </div>
                                                                                </div>
                                                                                <span style="clear:both;"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div m-padding="19px 0px 0px 0px" p-padding="19px 0 0 0"
                                                                     css_animation_delay="0.2"
                                                                     qfyuuid="qfy_column_text_qsxkq-c-6y"
                                                                     data-anitime="1" data-ani_iteration_count="1"
                                                                     class="qfy-element qfy-text qfy-text-58092 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_top-to-bottom delay0.2 qfe_start_animation"
                                                                     style="position: relative; line-height: 1.5em; background-position: left top; background-repeat: no-repeat; margin-top: 0px; margin-bottom: 0px; padding: 19px 0px 0px; border-radius: 0px; animation-duration: 1s; animation-iteration-count: 1;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="line-height: 20px; text-align: center; letter-spacing: 1px;"><span
                                                                                    style="font-size: 15px;">


</span><span style="font-size: 14px;">为终端用户浏览带来最佳用户体验.实现无需编程零基础建站,使人们轻松建设企业网站专题网站个人网站</span></div>
                                                                        <div style="line-height: 20px; text-align: center; letter-spacing: 1px;">
                                                                            <span style="font-size: 14px;">满足企业品牌宣传个人形象展示产品展示营销推广数据收集</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-loading="" data-loading-w="" data-open=""
                                                                     data-post="post" data-cate=" "
                                                                     m-padding="24.6333px 0 0 0" p-padding="8vh 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="advanced_list_u6qd4-c-tg"
                                                                     class="qfy-element advanced-list-5d735f3fada31994 qfe_teaser_grid qfe_content_element  qfe_grid columns_count_3 columns_count_3 qfe_teaser_grid_post  advanced_list "
                                                                     style="margin-top:0;margin-bottom:0;padding-top:8vh;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <style>
                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .item_wrapper .title {
                                                                            color: #382924 !important;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .item_wrapper .subtitle {
                                                                            color: #ffffff !important;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .item_wrapper .year {
                                                                            color: #382924 !important;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .item_wrapper .md {
                                                                            color: #382924 !important;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .item_wrapper .cate span {
                                                                            color: #cccccc !important;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .item_wrapper .description {
                                                                            color: #333333 !important;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .price_warp .amount, .advanced-list-5d735f3fada31994 .item_block:hover .price_warp del {
                                                                            color: #cccccc !important;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .price_warp ins .amount {
                                                                            color: #cccccc !important;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block .price_warp .amount, .advanced-list-5d735f3fada31994 .item_block .price_warp del {
                                                                            color: #757575;
                                                                            font-size: 22px;
                                                                            font-family: Raleway;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block .price_warp ins .amount {
                                                                            color: #757575;
                                                                            font-size: 22px;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block .price_warp {
                                                                            padding-top: 0px;
                                                                            padding-bottom: 0px;
                                                                            padding-left: 0px;
                                                                            padding-right: 0px;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block .item_wrapper {
                                                                            background: rgba(244, 244, 244, 1);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .item_wrapper {
                                                                            background: rgba(244, 244, 244, 1);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .item_block:hover .item_img img {
                                                                            transform: scale(1.06);
                                                                            -moz-transform: scale(1.06, 1.06);
                                                                            -webkit-transform: scale(1.06, 1.06);
                                                                            -o-transform: scale(1.06, 1.06);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-left {
                                                                            background-color: transparent;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-right {
                                                                            background-color: transparent;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-left:hover {
                                                                            background-color: transparent;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-right:hover {
                                                                            background-color: transparent;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-left:before {
                                                                            color: #382924;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-right:before {
                                                                            color: #382924;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-left:hover:before {
                                                                            color: #382924;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-right:hover:before {
                                                                            color: #382924;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-left {
                                                                            opacity: 0.7;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-right {
                                                                            opacity: 0.7;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-left:hover {
                                                                            opacity: 1;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-right:hover {
                                                                            opacity: 1;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-left:hover {
                                                                            border-color: #cccccc;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-right:hover {
                                                                            border-color: #cccccc;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .vc-carousel .vc-cbtn:before {
                                                                            font-size: 28px;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .title::after {
                                                                            border-color:;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .item_block .item_box > .item_wrapper {
                                                                            padding-left: 50px;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .item_block .item_box > .item_wrapper .item_des {
                                                                            max-height: 450px;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .item_box > .item_a_link::after {
                                                                            background: #c5a862;
                                                                            color: #ffffff;
                                                                            content: "阅读正文 →"
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .text_wrap {
                                                                            width: 450px;
                                                                            max-width: 100%
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .vc-right.leftbottom {
                                                                            left: 50%;
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .new1 .vc-left.leftbottom {
                                                                            left: calc(50% - 100px);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .new8 .vc-left.leftbottom {
                                                                            left: calc(50% - 70px);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .new2 .vc-left.leftbottom {
                                                                            left: calc(50% - 40px);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .new3 .vc-left.leftbottom {
                                                                            left: calc(50% - 26px);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .new4 .vc-left.leftbottom {
                                                                            left: calc(50% - 60px);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .new5 .vc-left.leftbottom {
                                                                            left: calc(50% - 30px);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .new6 .vc-left.leftbottom {
                                                                            left: calc(50% - 60px);
                                                                        }

                                                                        .advanced-list-5d735f3fada31994 .list-style6 .new7 .vc-left.leftbottom {
                                                                            left: calc(50% - 30px);
                                                                        }

                                                                        @media only screen and (max-width: 992px) {
                                                                            .advanced-list-5d735f3fada31994 .item_block {
                                                                                padding: 0px !important;
                                                                                position: relative !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_wrapper {
                                                                                position: relative !important;
                                                                                top: 0 !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_mask {
                                                                                display: none !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_wrapper {
                                                                                background: rgba(244, 244, 244, 1) !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_wrapper .title {
                                                                                color: #382924 !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_wrapper .subtitle {
                                                                                color: #ffffff !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_wrapper .year {
                                                                                color: #382924 !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_wrapper .md {
                                                                                color: #382924 !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_wrapper .cate span {
                                                                                color: #cccccc !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_wrapper .description {
                                                                                color: #333333 !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .price_warp .amount, .advanced-list-5d735f3fada31994 .item_block .price_warp del {
                                                                                color: #cccccc !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .price_warp ins .amount {
                                                                                color: #cccccc !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .item_block .item_wrapper .item_info .subtitle, .advanced-list-5d735f3fada31994 .item_block .item_wrapper .item_info .date_wrap, .advanced-list-5d735f3fada31994 .item_block .item_wrapper .item_info .cate, .advanced-list-5d735f3fada31994 .item_block .item_wrapper .item_info .item_des {
                                                                                opacity: 1 !important;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .list-style6 .item_block .item_box > .item_a_link, .list-style6 .item_block .item_box > .item_wrapper {
                                                                                display: block;
                                                                                width: 100% !important;
                                                                                padding: 0 !important
                                                                            }

                                                                        ;.advanced-list-5d735f3fada31994 .list-style6 .text_wrap {
                                                                             padding-right: 0px;
                                                                         }

                                                                            .advanced-list-5d735f3fada31994 .list-style6 .item_a_link img {
                                                                                margin-left: 0px;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .list-style6 .item_box > .item_a_link::after {
                                                                                display: none;
                                                                            }

                                                                            .advanced-list-5d735f3fada31994 .list-style6 .vc_slide_item {
                                                                                height: auto !important;
                                                                            }
                                                                        }
                                                                    </style>
                                                                    <div class="qfe_wrapper list-style6">
                                                                        <div style=";clear:both;" data-type="post"
                                                                             data-cate=" " data-pcate="">
                                                                            <div id="advanced-carousel-5d735f3fad9e9427"
                                                                                 data-ride="vc-carousel"
                                                                                 data-wrap="true" data-viewnum="1"
                                                                                 data-interval="5000"
                                                                                 data-auto-height="true"
                                                                                 data-mode="horizontal"
                                                                                 data-partial="false" data-per-view="1"
                                                                                 data-hide-on-end="false"
                                                                                 class="itempcbody  vc-carousel vc-slide new2 vc-build"
                                                                                 style="">
                                                                                <!-- Indicators -->
                                                                                <ol class="vc-carousel-indicators wf-mobile-hidden"
                                                                                    style="display:none;visibility:hidden;">
                                                                                    <li data-target="#advanced-carousel-5d735f3fad9e9427"
                                                                                        data-slide-to="0" class=""></li>
                                                                                    <li data-target="#advanced-carousel-5d735f3fad9e9427"
                                                                                        data-slide-to="1" class=""></li>
                                                                                    <li data-target="#advanced-carousel-5d735f3fad9e9427"
                                                                                        data-slide-to="2"
                                                                                        class="vc-active"></li>
                                                                                    <li data-target="#advanced-carousel-5d735f3fad9e9427"
                                                                                        data-slide-to="3" class=""></li>
                                                                                    <li data-target="#advanced-carousel-5d735f3fad9e9427"
                                                                                        data-slide-to="4" class=""></li>
                                                                                </ol>


                                                                                <a class="vc-left vc-cbtn wf-mobile-hidden  lr"
                                                                                   href="http://www.yzhpps.com/#advanced-carousel-5d735f3fad9e9427"
                                                                                   data-slide="prev"></a>
                                                                                <a class="vc-right vc-cbtn wf-mobile-hidden  lr"
                                                                                   href="http://www.yzhpps.com/#advanced-carousel-5d735f3fad9e9427"
                                                                                   data-slide="next"></a>

                                                                                <section class="vc-carousel-inner ">
                                                                                    <div class="vc-carousel-slideline"
                                                                                         style="width: 6295px;">
                                                                                        <div class="vc-carousel-slideline-inner"
                                                                                             style="left: -40%;">
                                                                                            @foreach($articles as $art)
                                                                                                <div class="vc-item vc_slide_item list-item"
                                                                                                     style="width: 20%; height: 400px;">
                                                                                                    <div class="vc-inner "
                                                                                                         style="margin:0;margin-bottom:10px;">
                                                                                                        <div class="vc_ca_post_id qfy_item_post"
                                                                                                             data-postid="13320"
                                                                                                             data-animaleinbegin="90%"
                                                                                                             data-animalename="qfyfadeInUp"
                                                                                                             data-delay="0"
                                                                                                             data-duration="1"
                                                                                                             style="">
                                                                                                            <div id="item_block"
                                                                                                                 class="qfy_item_block item_block "
                                                                                                                 style=" position: relative;">
                                                                                                                <div class="item_box ">
                                                                                                                    <a class="item_a_link"
                                                                                                                       href="/article/{{ $art->id }}"
                                                                                                                       title="{{ $art->title }}"
                                                                                                                       target="_self">
                                                                                                                        <div class="item_img"
                                                                                                                             target="_blank">
                                                                                                                            <img data-t-id="15436"
                                                                                                                                 style=""
                                                                                                                                 class="ag_image"
                                                                                                                                 src="{{ $art->cover }}"
                                                                                                                                 alt="products-4"
                                                                                                                                 description=""
                                                                                                                                 data-attach-id="15436"
                                                                                                                                 data-title="products-4"
                                                                                                                                 title=""
                                                                                                                                 src-img="">
                                                                                                                            <div class="item_mask"></div>
                                                                                                                        </div>
                                                                                                                    </a>
                                                                                                                    <div class="item_wrapper clearfix">
                                                                                                                        <div class="item_info clearfix">
                                                                                                                            <div class="text_wrap">
                                                                                                                                <a class="item_a_link"
                                                                                                                                   href="/article/{{ $art->id }}"
                                                                                                                                   title="{{ $art->title }}"
                                                                                                                                   target="_self">
                                                                                                                                    <div class="title ellipsis"
                                                                                                                                         data-title="true"
                                                                                                                                         style="color:#382924;font-size:18px;font-family:微软雅黑;font-weight:700;padding-bottom:20px;">
                                                                                                                                        {{ $art->title }}
                                                                                                                                    </div>
                                                                                                                                </a>
                                                                                                                                <div class="subtitle ellipsis hide"
                                                                                                                                     style="color:#999999;font-size:12px;display:none;"></div>
                                                                                                                                <div class="date_wrap show"
                                                                                                                                     style="padding:20px 0 0 0">
                                                                                                                                    <span class="year"
                                                                                                                                          style="color:#382924;font-size:14px;">{{ $art->created_at }}</span>
                                                                                                                                </div>


                                                                                                                                <div class="item_des show"
                                                                                                                                     style="max-width:px;">
                                                                                                                                    <div class="description post_excerpt"
                                                                                                                                         style="color:#333333;font-size:12px;max-width:px;word-break:break-all;line-height:20px;padding-top:10px;">
                                                                                                                                        <p>
                                                                                                                                            {{ $art->description }}</p>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="price_warp ellipsis hide">
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <a href="/article/{{ $art->id }}"
                                                                                                                   title="{{ $art->title }}"
                                                                                                                   target="_self"
                                                                                                                   class="details item_link"
                                                                                                                   style="color:#cccccc;font-size:16px;">阅读正文
                                                                                                                    →<i class="fa fa-angle-right"
                                                                                                                        aria-hidden="true"></i></a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endforeach
                                                                                        </div>

                                                                                    </div>
                                                                                </section>


                                                                            </div>

                                                                        </div>


                                                                    </div>
                                                                    <div class="clear"></div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-39-5d735f3fabdee992661 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-39-5d735f3fabdee992661 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-39-5d735f3fabdee992661 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top:;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-39-5d735f3fabdee992661 {
                                                            }

                                                            .qfy-column-39-5d735f3fabdee992661 > .column_inner > .background-overlay, .qfy-column-39-5d735f3fabdee992661 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                </div>
                                            </div>

                                        </section>
                                    </div>


                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div><!-- END .page-wrapper -->
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>


                </div>

            </div><!-- .wf-container -->
        </div><!-- .wf-wrap -->
    </div><!-- #main -->
    @component('./layouts/yzh/footer')
    @endcomponent


    <a href="http://www.yzhpps.com/#" class="scroll-top displaynone off"></a>

</div><!-- #page -->

<script type="text/javascript">
    /* <![CDATA[ */
    /* iphorm-plugin */
    var iphormL10n = {
        "error_submitting_form": "\u5728\u63d0\u4ea4\u8868\u5355\u65f6\u6709\u9519\u8bef",
        "swfupload_flash_url": "http:\/\/5d1c71c9850fc.t73.qifeiye.com\/qfy-includes\/js\/swfupload\/swfupload.swf",
        "swfupload_upload_url": "http:\/\/5d1c71c9850fc.t73.qifeiye.com\/?iphorm_swfupload=1",
        "swfupload_too_many": "\u961f\u5217\u4e2d\u7684\u6587\u4ef6\u592a\u591a\u4e86",
        "swfupload_file_too_big": "\u6587\u4ef6\u592a\u5927\u4e86",
        "swfupload_file_empty": "\u4e0d\u80fd\u4e0a\u4f20\u7a7a\u6587\u4ef6",
        "swfupload_file_type_not_allowed": "\u8be5\u6587\u4ef6\u7684\u7c7b\u578b\u662f\u4e0d\u5141\u8bb8\u4e0a\u4f20\u7684",
        "swfupload_unknown_queue_error": "\u672a\u77e5\u961f\u5217\u9519\u8bef\uff0c\u8bf7\u7a0d\u5019\u518d\u8bd5",
        "swfupload_upload_error": "\u4e0a\u4f20\u9519\u8bef",
        "swfupload_upload_failed": "\u4e0a\u4f20\u5931\u8d25",
        "swfupload_server_io": "\u670d\u52a1\u5668IO\u9519\u8bef",
        "swfupload_security_error": "\u5b89\u5168\u9519\u8bef",
        "swfupload_limit_exceeded": "\u4e0a\u4f20\u8d85\u8fc7\u9650\u5236",
        "swfupload_validation_failed": "\u9a8c\u8bc1\u5931\u8d25",
        "swfupload_upload_stopped": "\u4e0a\u4f20\u88ab\u7ec8\u6b62",
        "swfupload_unknown_upload_error": "\u6587\u4ef6\u4e0a\u4f20\u88ab\u610f\u5916\u7ec8\u6b62",
        "plugin_url": "http:\/\/5d1c71c9850fc.t73.qifeiye.com\/qfy-content\/plugins\/qfy_form",
        "preview_no_submit": "\u5728\u9884\u89c8\u6a21\u5f0f\u4e0b\u4e0d\u80fd\u63d0\u4ea4\u8868\u5355",
        "iphorm_required": "\u8fd9\u662f\u4e00\u4e2a\u5fc5\u586b\u9879\u54e6"
    };

    /* ]]> */
</script>
<script type="text/javascript" src="/js/yzh/index.js">/*Cache!*/</script>
<link rel="stylesheet" href="/css/yzh/index.css" type="text/css" media="all">
<!-- Cache! -->
<style type="text/css">

</style>
<div style="display:none;"></div>        <!-- 起飞页/猫头鹰 -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function () {
        var u = "//t2.qifeiye.com/";
        _paq.push(['setTrackerUrl', u + 'js/tracker.php']);
        _paq.push(['setSiteId', '1']);
        var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.async = true;
        g.defer = true;
        g.src = u + 'js/tracker.php';
        s.parentNode.insertBefore(g, s);
    })();
</script>
<!-- End 起飞页/猫头鹰 Code -->


<div id="phantom" class="" style="animation-duration: 0.4s;">
    <div class="ph-wrap with-logo">
        <div class="ph-wrap-content" style="max-width: 1600px;">
            <div class="ph-wrap-inner">
                <div class="logo-box">
                    <div style="height:48px;vertical-align: middle;display: table-cell;"><a
                                href="http://www.yzhpps.com/"><img
                                    src="/images/yzh/logo.png"
                                    height="88" width="299" style="max-height: 32px;"></a></div>
                </div>
                <div class="menu-box"></div>
                <div class="menu-info-box align_right">
                    <div class="main-nav-slide-inner" data-class="align_right">
                        <div class="floatmenu-bar-right bit_widget_more" bitdatamarker="bitHeader-3"
                             bitdataaction="site_fix_container" bitdatacolor="white">
                            <div id="simplepage-4" style="margin-top:0;margin-bottom:0;"
                                 class="headerWidget_1 simplepage site_tooler">
                                <style class="style_simplepage-4">#simplepage-4 .widget-title {
                                        padding: 0 0 0 10px;
                                        height: 28px;
                                        line-height: 28px;
                                        background-color: transparent;
                                        margin: 0px;
                                        font-family:;
                                        font-size: px;
                                        font-weight: normal;
                                        font-style: normal;
                                        text-decoration: none;
                                        color: #ffffff;
                                        border-top: 1px solid transparent;
                                        border-left: 1px solid transparent;
                                        border-right: 1px solid transparent;
                                        border-bottom: 0px solid transparent;
                                        background-image: none;
                                        -webkit-border-top-left-radius: 4px;
                                        -webkit-border-top-right-radius: 4px;
                                        -moz-border-radius-topleft: 4px;
                                        -moz-border-radius-topright: 4px;
                                        border-top-left-radius: 4px;
                                        border-top-right-radius: 4px;
                                    }

                                    #simplepage-4 .widget-title {
                                        border-top: 0;
                                        border-left: 0;
                                        border-right: 0;
                                    }

                                    #simplepage-4 .bitWidgetFrame {
                                        border-bottom: 0;
                                        border-top: 0;
                                        border-left: 0;
                                        border-right: 0;
                                        padding: 4px 10px 4px 10px;
                                    }

                                    #simplepage-4 {
                                        -webkit-box-shadow: none;
                                        box-shadow: none;
                                    }

                                    #simplepage-4 .bitWidgetFrame {
                                        background-color: transparent;
                                        background-image: none;
                                        -webkit-border-bottom-left-radius: 4px;
                                        border-bottom-left-radius: 4px;
                                        -webkit-border-bottom-right-radius: 4px;
                                        border-bottom-right-radius: 4px;
                                    }

                                    #simplepage-4 .bitWidgetFrame {
                                        padding-left: 0px;
                                        padding-right: 0px;
                                    }

                                    body #simplepage-4 .bitWidgetFrame {
                                        padding-top: 0px !important;
                                        padding-bottom: 0px !important;
                                    }</style>
                                <div class="simplepage_container bitWidgetFrame" data-post_id="15366">
                                    <section data-fixheight=""
                                             class="qfy-row-1-5d735f3f7f1c6321376 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                                             id="bit_38bwq" style="margin-bottom:0;border-radius:0px;color:#382924;">
                                        <style class="row_class qfy_style_class">
                                            @media only screen and (min-width: 992px) {
                                                section.section.qfy-row-1-5d735f3f7f1c6321376 {
                                                    padding-left: 0;
                                                    padding-right: 0;
                                                    padding-top: 0;
                                                    padding-bottom: 0;
                                                    margin-top: 0;
                                                }
                                            }

                                            @media only screen and (max-width: 992px) {
                                                .bit-html section.section.qfy-row-1-5d735f3f7f1c6321376 {
                                                    padding-left: 15px;
                                                    padding-right: 15px;
                                                    padding-top: 0;
                                                    padding-bottom: 0;
                                                    margin-top: 0;
                                                    min-height: 0;
                                                }
                                            }
                                        </style>
                                        <div class="section-background-overlay background-overlay grid-overlay-0 "
                                             style="background-color: #ffffff;"></div>

                                        <div class="container">
                                            <div class="row qfe_row">
                                                <div data-animaleinbegin="bottom-in-view" data-animalename="qfyfadeInUp"
                                                     data-duration="" data-delay=""
                                                     class=" qfy-column-1-5d735f3f7f450490518 qfy-column-inner  vc_span12  text-default small-screen-default fullrow"
                                                     data-dw="1/1" data-fixheight="">
                                                    <div style=";position:relative;" class="column_inner ">
                                                        <div class=" background-overlay grid-overlay-"
                                                             style="background-color:transparent;width:100%;"></div>
                                                        <div class="column_containter"
                                                             style="z-index:3;position:relative;">
                                                            <div id="vc_header_5d735f3f7f7bc831"
                                                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                 css_animation_delay="0" qfyuuid="qfy_header_hy33n-c-y3"
                                                                 data-anitime="0.7" data-ani_iteration_count="1"
                                                                 class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                 style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                <style>#vc_header_5d735f3f7f7bc831 .qfy_title_inner, #vc_header_5d735f3f7f7bc831 .qfy_title_image_inner {
                                                                        vertical-align: middle !important;
                                                                    }

                                                                    @media only screen and (max-width: 768px) {
                                                                        #vc_header_5d735f3f7f7bc831 .header_title {
                                                                            font-size: 16px !important;
                                                                        }
                                                                    }

                                                                    @media only screen and (max-width: 768px) {
                                                                        #vc_header_5d735f3f7f7bc831 .header_subtitle {
                                                                            font-size: 12px !important;
                                                                        }
                                                                    }</style>
                                                                <div class="qfe_wrapper">
                                                                    <div class="qfy_title left mobileleft">
                                                                        <div class="qfy_title_image_warpper"
                                                                             style="display:inline-block;position:relative;max-width:100%;">
                                                                            <div class="qfy_title_image_inner"
                                                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                                                <div class="header_image "
                                                                                     style="padding:5px 0 0 0;width:40px;">
                                                                                    <img class=" ag_image"
                                                                                         src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi8yNDdhMTdlNmRkOGY1ZDE5YjU2YWFmNTQzYjBjMGJhOC00MHg0MC5qcGc_p_p100_p_3D.jpg"
                                                                                         alt="top" description=""
                                                                                         data-attach-id="15369"
                                                                                         data-title="top" title=""
                                                                                         src-img=""></div>
                                                                            </div>
                                                                            <div class="qfy_title_inner"
                                                                                 style="display:inline-block;position:relative;max-width:calc(100% - 40px);">
                                                                                <div class="header_title"
                                                                                     style="font-family:微软雅黑;font-size:16px;font-weight:bold;font-style:normal;color:#382924;padding:0 0 0 10px;display:block;padding:0 0 0 10px;vertical-align:bottom;">
                                                                                    +86（010）6296 296
                                                                                </div>
                                                                                <div class="header_subtitle"
                                                                                     style="font-size:12px;font-weight:normal;font-style:normal;color:#c5a862;display:block;padding:2px 0 0 10PX;vertical-align:bottom;">
                                                                                    周一-周六：早上10:00-下午2:00
                                                                                </div>
                                                                            </div>
                                                                            <style></style>
                                                                            <div class="button_wrapper"
                                                                                 style="display:none;">
                                                                                <div class="button "
                                                                                     style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                    + 查看更多
                                                                                </div>
                                                                            </div>
                                                                            <span style="clear:both;"></span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                        .qfy-column-1-5d735f3f7f450490518 > .column_inner {
                                                            padding-left: 0;
                                                            padding-right: 0;
                                                            padding-top: 0;
                                                            padding-bottom: 0;
                                                        }

                                                        .qfe_row .vc_span_class.qfy-column-1-5d735f3f7f450490518 {
                                                        }

                                                    ;
                                                    }

                                                    @media only screen and (max-width: 992px) {
                                                        .qfy-column-1-5d735f3f7f450490518 > .column_inner {
                                                            margin: 0 auto 0 !important;
                                                            padding-left: 0;
                                                            padding-right: 0;
                                                            padding-top:;
                                                            padding-bottom:;
                                                        }

                                                        .display_entire .qfe_row .vc_span_class.qfy-column-1-5d735f3f7f450490518 {
                                                        }

                                                        .qfy-column-1-5d735f3f7f450490518 > .column_inner > .background-overlay, .qfy-column-1-5d735f3f7f450490518 > .column_inner > .background-media {
                                                            width: 100% !important;
                                                            left: 0 !important;
                                                            right: auto !important;
                                                        }
                                                    }</style>
                                            </div>
                                        </div>

                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{ asset('static/layuiadmin/layui/css/layui.css') }}" media="all">
<script src="{{ asset('static/layuiadmin/layui/layui.js') }}"></script>
<script>
    var layer = null;
    layui.use(['layer', 'form'], function () {
        layer = layui.layer
            , form = layui.form;
    });

    $('.iphorm-submit-element').click(function () {
        var username = $("input[name='username']").val();
        var tel = $("input[name='tel']").val();
        var email = $("input[name='email']").val();
        var product = $("input[name='product']").val();
        var remark = $("textarea[name='remark']").val();
        if (username == '') {
            layer.msg('请填写姓名', {icon: 5});
            return false;
        }
        if (tel == '') {
            layer.msg('请填写电话', {icon: 5});
            return false;
        }
        if (!(/^1[3456789]\d{9}$/.test(tel))) {
            layer.msg('手机号码有误，请重填', {icon: 5});
            return false;
        }
        if (email == '') {
            layer.msg('请填写邮箱', {icon: 5});
            return false;
        }
        var reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
        if (!reg.test(email)) {
            layer.msg('请填写正确的邮箱格式', {icon: 5});
            return false;
        }
        if (product == '') {
            layer.msg('请填写想要咨询的产品', {icon: 5});
            return false;
        }
        $.ajax({
            type: "POST",
            url: "/message",
            data: {
                username: username,
                tel: tel,
                email: email,
                product: product,
                remark: remark,
                _token: "{{ csrf_token() }}"
            },
            dataType: "json",
            success: function (data) {
                if (data.status == 0) {
                    layer.msg(data.msg, {icon: 6});
                } else {
                    layer.msg(data.msg, {icon: 5});
                }
            }
        });
        return false;
    });
</script>
</body>
</html>