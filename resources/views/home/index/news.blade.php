@extends('layouts.yzh')
@section('header')

@endsection
@section('page_title', '新闻资讯')
@section('content')
    @include('./layouts/yzh/back')
    <section data-fixheight=""
             class="qfy-row-4-5d7355d421c9c184885 section     no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
             id="bit_oe8tb"
             style="margin-bottom:0;border-radius:0px;border-top:0px solid rgba(255,255,255,1);border-bottom:1px solid rgba(232,232,232,1);border-left:0px solid rgba(255,255,255,1);border-right:0px solid rgba(255,255,255,1);color:#382924;">
        <style class="row_class qfy_style_class">
            @media only screen and (min-width: 992px) {
                section.section.qfy-row-4-5d7355d421c9c184885 {
                    padding-left: 0;
                    padding-right: 0;
                    padding-top: 10vh;
                    padding-bottom: 10vh;
                    margin-top: 0;
                }

                section.section.qfy-row-4-5d7355d421c9c184885 > .container {
                    max-width: 1640px;
                    margin: 0 auto;
                }
            }

            @media only screen and (max-width: 992px) {
                .bit-html section.section.qfy-row-4-5d7355d421c9c184885 {
                    padding-left: 15px;
                    padding-right: 15px;
                    padding-top: 20px;
                    padding-bottom: 40px;
                    margin-top: 0;
                    min-height: 0;
                }
            }
        </style>
        <div class="section-background-overlay background-overlay grid-overlay-0 "
             style="background-color: #ffffff;"></div>

        <div class="container">
            <div class="row qfe_row">
                <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                     data-duration="" data-delay=""
                     class=" qfy-column-4-5d7355d421ff1928475 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                     data-dw="1/1" data-fixheight="">
                    <div style=";position:relative;" class="column_inner ">
                        <div class=" background-overlay grid-overlay-"
                             style="background-color:transparent;width:100%;"></div>
                        <div class="column_containter"
                             style="z-index:3;position:relative;">
                            <div id="" data-loading="" data-loading-w=""
                                 data-open="" data-post="post" data-cate=" "
                                 m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                 css_animation_delay="0"
                                 qfyuuid="qfy_posts_grid_gt74s"
                                 class="qfy-element  UUID-POSTS-5d7355d42976a693614 qfe_teaser_grid qfe_content_element navAjax qfe_grid columns_count_4 columns_count_4 qfe_teaser_grid_post  "
                                 style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <style>.UUID-POSTS-5d7355d42976a693614 li:hover .post-title > i.glyphicon {
                                        color: #cccccc !important;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 li:hover .post-title > a, .UUID-POSTS-5d7355d42976a693614 li:hover .post-title > div > a, .UUID-POSTS-5d7355d42976a693614 li:hover .post-title > span {
                                        color: #382924 !important;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 li:hover .post-title > span.toEditor {
                                        color: #fff !important;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 li:hover .subtitle {
                                        color: #cccccc !important;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 li:hover .post-comment {
                                        padding-left: 15px !important;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 li:hover .vc_read_more {
                                    }

                                    @media only screen and (max-width: 992px) {
                                        .UUID-POSTS-5d7355d42976a693614 .vc_span_mobile {
                                            float: left !important;
                                            max-width: 99.8% !important;
                                        }

                                        .UUID-POSTS-5d7355d42976a693614 .post_blog .vc_span_mobile .blog-media {
                                            width: auto !important;
                                        }

                                        .thumbnail_text-5d7355d429722709 .head {
                                            font-size: 16px !important;
                                        }

                                        .thumbnail_text-5d7355d429722709 .content {
                                            font-size: 16px !important;
                                        }
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 .qfy_item_post .price_warp .amount, .UUID-POSTS-5d7355d42976a693614 .qfy_item_post .price_warp del {
                                        color: #cccccc;
                                        font-size: 12px;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 .qfy_item_post .price_warp ins .amount {
                                        color: #cccccc;
                                        font-size: 12px;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 .qfy_item_post .price_warp {
                                        padding-top: 0px;
                                        padding-bottom: 0px;
                                        padding-left: 0px;
                                        padding-right: 0px;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 .qfy_item_post:hover .price_warp .amount, .UUID-POSTS-5d7355d42976a693614 .qfy_item_post:hover .price_warp del {
                                        color: #cccccc !important;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 .qfy_item_post:hover .price_warp ins .amount {
                                        color: #cccccc !important;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 li:hover .post-title {
                                        border-color: transparent !important;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 li:hover {
                                        transition: all .6s ease;
                                    }

                                    .UUID-POSTS-5d7355d42976a693614 li {
                                        transition: all .6s ease;
                                    }</style>
                                <div class="qfe_wrapper">
                                    <div class="teaser_grid_container noanimale"
                                         style=";clear:both;" data-type="post"
                                         data-cate=" " data-pcate="">
                                        <ul style="min-height:60px;" class="qfe_thumbnails qfe_thumbnails-fluid vc_clearfix post_grid" data-layout-mode="fitRows">
                                            @foreach($articles as $art)
                                                <li data-postid="{{ $art->id }}"
                                                    data-animaleinbegin="90%"
                                                    data-animalename="qfyfadeInUp"
                                                    data-delay="0.1" data-duration=""
                                                    class="isotope-item qfy_item_post vc_span3 vc_span_mobile vc_span_mobile12 grid-cat-1 grid-cat-2" style="max-width:24.95%;margin-bottom:2px;padding-bottom:2px;padding:20px;">
                                                    <div class="itembody  itempcbody "
                                                         style="">
                                                        <div class="post-thumb blog-media wf-td" style="overflow:hidden;display:block;width: 100%;text-align: center;padding-bottom:10px;vertical-align:middle;">
                                                            <a href="/article/{{ $art->id }}"
                                                               class="bitImageAhover link_image"
                                                               title="{{ $art->title }}">
                                                                <img data-t-id="{{ $art->title }}" style="border-radius: 10px;" class="vc_box_rounded ag_image" src="{{ $art->cover }}" alt="products-1" description="" data-attach-id="{{ $art->id }}" data-title="products-{{ $art->id }}" title="{{ $art->title }}" src-img=""><i></i></a>
                                                        </div>
                                                        <div class="post-title" style="padding-top:15px;padding-bottom:10px;color:#382924;line-height:20px; vertical-align: top; text-align:left;width:100%; ">
                                                            <a data-title="true" style="color:#382924;font-size:20px;font-family:微软雅黑;line-height:26px" href="/article/{{ $art->id }}"  class="bitImageAhover link_title" title="{{ $art->title }}">{{ $art->title }}</a>
                                                        </div>
                                                        <div class="entry-content post_excerpt"
                                                             style="color:#898989;font-size:12px;font-family:微软雅黑;line-height:25px;padding-top:3px;padding-bottom:10px;">
                                                            <p>{{ $art->description }}</p>
                                                            <br>
                                                        </div>
                                                        <span style="clear:both"></span>
                                                        <div class="vc_read_more_wrapper">
                                                            <a style=" color:#c5a862;font-size:13px;font-family:微软雅黑;"
                                                               href="/article/{{ $art->id }}"
                                                               class="vc_read_more "
                                                               title="/article/{{ $art->title }}">阅读正文
                                                                →</a>
                                                        </div>
                                                        <div style="clear:both"></div>
                                                    </div>
                                                </li>
                                                @if($loop->iteration % 4 ==0)
                                                    <div class="wf-mobile-hidden" style="clear:both"></div>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                        .qfy-column-4-5d7355d421ff1928475 > .column_inner {
                            padding-left: 0;
                            padding-right: 0;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .qfe_row .vc_span_class.qfy-column-4-5d7355d421ff1928475 {
                        }

                    ;
                    }

                    @media only screen and (max-width: 992px) {
                        .qfy-column-4-5d7355d421ff1928475 > .column_inner {
                            margin: 0 auto 0 !important;
                            padding-left: 0;
                            padding-right: 0;
                            padding-top:;
                            padding-bottom:;
                        }

                        .display_entire .qfe_row .vc_span_class.qfy-column-4-5d7355d421ff1928475 {
                        }

                        .qfy-column-4-5d7355d421ff1928475 > .column_inner > .background-overlay, .qfy-column-4-5d7355d421ff1928475 > .column_inner > .background-media {
                            width: 100% !important;
                            left: 0 !important;
                            right: auto !important;
                        }
                    }</style>
            </div>
        </div>

    </section>
@endsection