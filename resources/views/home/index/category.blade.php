@extends('layouts.yzh')
@section('header')
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 992px) {
            section.section.qfy-row-4-5d73531865b1f911681 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 10vh;
                padding-bottom: 10vh;
                margin-top: 0;
            }

            section.section.qfy-row-4-5d73531865b1f911681 > .container {
                max-width: 1600px;
                margin: 0 auto;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-4-5d73531865b1f911681 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 40px;
                padding-bottom: 40px;
                margin-top: 0;
                min-height: 0;
            }
        }
        @media only screen and (max-width: 768px) {
            .single_image_text-5d73531866ac7956 .head {
                font-size: 16px !important;
            }

            .single_image_text-5d73531866ac7956 .content {
                font-size: 16px !important;
            }
        }
        @media only screen and (max-width: 768px) {
            #vc_header_5d73531867166875 .header_title {
                font-size: 24px !important;
            }
        }

        @media only screen and (max-width: 768px) {
            #vc_header_5d73531867166875 .header_subtitle {
                font-size: 12px !important;
            }
        }
        @media only screen and (max-width: 768px) {
            .single_image_text-5d73531867dcd359 .head {
                font-size: 16px !important;
            }

            .single_image_text-5d73531867dcd359 .content {
                font-size: 16px !important;
            }
        }
    </style>
    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
            .qfy-column-4-5d73531865e86760715 > .column_inner {
                padding-left: 0;
                padding-right: 0;
                padding-top: 15px;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-4-5d73531865e86760715 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-4-5d73531865e86760715 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-4-5d73531865e86760715 {
            }

            .qfy-column-4-5d73531865e86760715 > .column_inner > .background-overlay, .qfy-column-4-5d73531865e86760715 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }</style>
    <style>.advanced-list-5d73531869457369 .item_block:hover .item_wrapper .title {
            color: #382924 !important;
        }

        .advanced-list-5d73531869457369 .item_block:hover .item_wrapper .subtitle {
            color: #999999 !important;
        }

        .advanced-list-5d73531869457369 .item_block:hover .item_wrapper .year {
            color: #c4c4c4 !important;
        }

        .advanced-list-5d73531869457369 .item_block:hover .item_wrapper .md {
            color: #c4c4c4 !important;
        }

        .advanced-list-5d73531869457369 .item_block:hover .item_wrapper .cate span {
            color: #cccccc !important;
        }

        .advanced-list-5d73531869457369 .item_block:hover .item_wrapper .description {
            color: #333333 !important;
        }

        .advanced-list-5d73531869457369 .item_block:hover .price_warp .amount, .advanced-list-5d73531869457369 .item_block:hover .price_warp del {
            color: #cccccc !important;
        }

        .advanced-list-5d73531869457369 .item_block:hover .price_warp ins .amount {
            color: #cccccc !important;
        }

        .advanced-list-5d73531869457369 .item_block .price_warp .amount, .advanced-list-5d73531869457369 .item_block .price_warp del {
            color: #757575;
            font-size: 22px;
            font-family: Raleway;
        }

        .advanced-list-5d73531869457369 .item_block .price_warp ins .amount {
            color: #757575;
            font-size: 22px;
        }

        .advanced-list-5d73531869457369 .item_block .price_warp {
            padding-top: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            padding-right: 0px;
        }

        .advanced-list-5d73531869457369 .item_block .item_wrapper {
            background: rgba(255, 255, 255, 1);
        }

        .advanced-list-5d73531869457369 .item_block:hover .item_wrapper {
            background: rgba(255, 255, 255, 1);
        }

        .advanced-list-5d73531869457369 .item_block:hover .item_img img {
            transform: none;
        }

        .advanced-list-5d73531869457369 .vc-cbtn.vc-left {
            margin-left: -40px;
        }

        .advanced-list-5d73531869457369 .vc-cbtn.vc-right {
            margin-right: -40px;
        }

        .advanced-list-5d73531869457369 .vc-cbtn {
            top: calc(50% - 20px);
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-left {
            background-color: transparent;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-right {
            background-color: transparent;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-left:hover {
            background-color: transparent;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-right:hover {
            background-color: transparent;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-left:before {
            color: #382924;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-right:before {
            color: #382924;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-left:hover:before {
            color: #382924;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-right:hover:before {
            color: #382924;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-left {
            opacity: 0.7;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-right {
            opacity: 0.7;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-left:hover {
            opacity: 1;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-right:hover {
            opacity: 1;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-left:hover {
            border-color: #cccccc;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-right:hover {
            border-color: #cccccc;
        }

        .advanced-list-5d73531869457369 .vc-carousel .vc-cbtn:before {
            font-size: 28px;
        }

        .advanced-list-5d73531869457369 .list-style4 .item_block .item_wrapper {
            height: 110px;
            overflow: hidden;
        }

        .advanced-list-5d73531869457369 .list-style4 .item_block .item_wrapper .item_info .text_wrap {
            text-align: center
        }

        @media only screen and (max-width: 992px) {
            .advanced-list-5d73531869457369 .item_block .item_wrapper {
                height: 100px !important;
            }

            .advanced-list-5d73531869457369 .item_block {
                padding: 0px !important;
                position: relative !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_wrapper {
                position: relative !important;
                top: 0 !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_mask {
                display: none !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_wrapper {
                background: rgba(255, 255, 255, 1) !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_wrapper .title {
                color: #382924 !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_wrapper .subtitle {
                color: #999999 !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_wrapper .year {
                color: #c4c4c4 !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_wrapper .md {
                color: #c4c4c4 !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_wrapper .cate span {
                color: #cccccc !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_wrapper .description {
                color: #333333 !important;
            }

            .advanced-list-5d73531869457369 .item_block .price_warp .amount, .advanced-list-5d73531869457369 .item_block .price_warp del {
                color: #cccccc !important;
            }

            .advanced-list-5d73531869457369 .item_block .price_warp ins .amount {
                color: #cccccc !important;
            }

            .advanced-list-5d73531869457369 .item_block .item_wrapper .item_info .subtitle, .advanced-list-5d73531869457369 .item_block .item_wrapper .item_info .date_wrap, .advanced-list-5d73531869457369 .item_block .item_wrapper .item_info .cate, .advanced-list-5d73531869457369 .item_block .item_wrapper .item_info .item_des {
                opacity: 1 !important;
            }
        }</style>
    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
            .qfy-column-5-5d73531867fcc842665 > .column_inner {
                padding-left: 10px;
                padding-right: 0;
                padding-top: 0;
                padding-bottom: 0;
            }

            .qfe_row .vc_span_class.qfy-column-5-5d73531867fcc842665 {
            }

        ;
        }

        @media only screen and (max-width: 992px) {
            .qfy-column-5-5d73531867fcc842665 > .column_inner {
                margin: 0 auto 0 !important;
                min-height: 0 !important;
                padding-left: 0;
                padding-right: 0;
                padding-top: 22px;
                padding-bottom: 0;
            }

            .display_entire .qfe_row .vc_span_class.qfy-column-5-5d73531867fcc842665 {
            }

            .qfy-column-5-5d73531867fcc842665 > .column_inner > .background-overlay, .qfy-column-5-5d73531867fcc842665 > .column_inner > .background-media {
                width: 100% !important;
                left: 0 !important;
                right: auto !important;
            }
        }
        .section-content{
            padding-left: 0;
            padding-right: 0;
            padding-top: 10vh;
            padding-bottom: 10vh;
            margin-top: 0;
        }
    </style>
@endsection
@section('page_title', '产品中心')
@section('content')
    @include('./layouts/yzh/back')
    @foreach($categoryList as $cate)
        <section data-fixheight=""
                 class="qfy-row-4-5d73531865b1f911681 section  section-content   no  section-text-no-shadow section-inner-no-shadow section-normal section-orgi"
                 id="bit_l1l7q"
                 style="margin-bottom:0;border-radius:0px;color:#382924;">
            <div class="section-background-overlay background-overlay grid-overlay-0 "
                 style="background-color: transparent;"></div>
            <div class="container">
                <div class="row qfe_row">
                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                         data-duration="1" data-delay=""
                         class=" qfy-column-4-5d73531865e86760715 qfy-column-inner  vc_span_class  vc_span2_4  text-Default small-screen-undefined notfullrow"
                         data-dw="1/5" data-fixheight="">
                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:20px;border-radius:10px;;position:relative;"
                             class="column_inner ">
                            <div class=" background-overlay grid-overlay-0"
                                 style="background-color:transparent;width:100%;border-radius:10px;"></div>
                            <div class="column_containter"
                                 style="z-index:3;position:relative;border-radius:10px;">
                                <div id="vc_img_5d735318669f3872"
                                     style="padding:0px;margin:0px;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;"
                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                     css_animation_delay="0"
                                     qfyuuid="qfy_single_image_txkn4-c-n6"
                                     data-anitime="0.7" data-ani_iteration_count="1"
                                     class="qfy-element bitImageControlDiv ">
                                    <a class="bitImageAhover  ">
                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_left">
                                            <div class="qfe_wrapper"><span></span><img src="" class="front_image  attachment-medium"  alt="{{ $cate->name }}" title="" description="" data-title="{{ $cate->keywords }}" src-img="" style="height:190px;width: 165px;display: block"></div>
                                        </div>
                                    </a></div>
                                <a class="bitImageAhover  "> </a>
                                <div id="vc_header_5d73531867166875" m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"  css_animation_delay="0" qfyuuid="qfy_header_6vi2r-c-n6" data-anitime="0.7" data-ani_iteration_count="1" class="qfy-element minheigh1px qfe_text_column qfe_content_element "
   style="position: relative;;;background-repeat: no-repeat;;margin-top:30px;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">

                                    <div class="qfe_wrapper">
                                        <div class="qfy_title left mobileleft">
                                            <div class="qfy_title_image_warpper"
                                                 style="display:inline-block;position:relative;max-width:100%;">
                                                <div class="qfy_title_inner"
                                                     style="display:inline-block;position:relative;max-width:100%;">
                                                    <div class="header_title"
                                                         style="font-family:微软雅黑;font-size:26px;font-weight:bold;font-style:normal;color:#382924;display:block;vertical-align:bottom;">
                                                        {{ $cate->name }}
                                                    </div>
                                                    <div class="header_subtitle"
                                                         style="font-size:13px;font-weight:normal;font-style:normal;color:#bababa;display:block;vertical-align:bottom;">
                                                        {{ $cate->keywords }}
                                                    </div>
                                                </div>
                                                <style></style>
                                                <div class="button_wrapper"
                                                     style="display:none;">
                                                    <div class="button "
                                                         style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                        + 查看更多
                                                    </div>
                                                </div>
                                                <span style="clear:both;"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="vc_img_5d73531867ce9285" style="padding:0px;margin:0px;clear:both;position:relative;margin-top:10px;margin-bottom:0;padding-top:13px;padding-bottom:0;padding-right:0;padding-left:0;background-position:0 0;background-repeat: no-repeat;" m-padding="13px 0px 0px 0px" p-padding="13px 0 0 0" css_animation_delay="0" qfyuuid="qfy_single_image_txkn4-c-n6" data-anitime="0.7" data-ani_iteration_count="1" class="qfy-element bitImageControlDiv ">
                                    <a class="bitImageAhover  ">
                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element vc_align_left">
                                            <div class="qfe_wrapper"><span></span><img
                                                        class="front_image   ag_image"
                                                        src="/images/yzh/aHR0cDovLzVkMWM3MWM5ODUwZmMudDczLnFpZmVpeWUuY29tL3FmeS1jb250ZW50L3VwbG9hZHMvMjAxOS8wNi80M2JlYjBiY2YyNzFlNTU3MmNiM2YzNWE5NzQ2Nzg0NC0zOHg0MC5wbmc_p_p100_p_3D.png"
                                                        alt="left-tips" description=""
                                                        data-attach-id="15389"
                                                        data-title="left-tips" title=""
                                                        src-img="" style=""></div>
                                        </div>
                                    </a></div>
                                <a class="bitImageAhover  "> </a></div>
                        </div>
                    </div>

                    <div data-animaleinbegin="bottom-in-view" data-animalename="qfyfadeInUp" data-duration="1" data-delay="" class=" qfy-column-5-5d73531867fcc842665 qfy-column-inner  vc_span_class  vc_span9_6 text-Default small-screen-Default notfullrow" data-dw="4/5" data-fixheight="">
                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                             class="column_inner ">
                            <div class=" background-overlay grid-overlay-0"
                                 style="background-color:transparent;width:100%;"></div>
                            <div class="column_containter"
                                 style="z-index:3;position:relative;">
                                <div data-loading="" data-loading-w="" data-open=""
                                     data-post="products" data-cate="47"
                                     m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                     css_animation_delay="0"
                                     qfyuuid="advanced_list_ku2wl-c-n6"
                                     class="qfy-element advanced-list-5d73531869457369 qfe_teaser_grid qfe_content_element  qfe_grid columns_count_4 columns_count_4 qfe_teaser_grid_products  advanced_list "
                                     style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">

                                    <div class="qfe_wrapper list-style4">
                                        <div style=";clear:both;" data-type="products"
                                             data-cate="1" data-pcate="47">
                                            <div id="advanced-carousel-5d73531869412366"
                                                 data-wrap="true" data-viewnum="4"
                                                 data-interval="0"
                                                 data-auto-height="true"
                                                 data-mode="horizontal"
                                                 data-partial="false" data-per-view="4"
                                                 data-hide-on-end="true"
                                                 class=" itempcbody  normal-list"
                                                 style="">
                                                <!-- Indicators -->
                                                <section class="vc-carousel-inner ">
                                                    <div class="vc-carousel-slideline">
                                                        <div class="vc-carousel-slideline-inner">
                                                            @foreach($categoryArt[$cate->id] as  $art)
                                                                <div class="item_container list-item vc_span3" style="float:left;margin-bottom:0px;padding:0 10px;">
                                                                    <div class="vc_ca_post_id qfy_item_pos"
                                                                         data-postid="13415"
                                                                         data-animaleinbegin="90%"
                                                                         data-animalename="qfyfadeInUp"
                                                                         data-delay="0"
                                                                         data-duration="1"
                                                                         style="">
                                                                        <div id="item_block"
                                                                             class="qfy_item_block item_block "
                                                                             style=" position: relative;">
                                                                            <div class="item_box ">
                                                                                <a class="item_a_link"
                                                                                   href="/product/{{ $art->id }}"
                                                                                   title="产品名称标题一"
                                                                                   target="_self">
                                                                                    <div class="item_img"
                                                                                         target="_blank">
                                                                                        <img data-t-id="15433"
                                                                                             style="border-radius: 4px;"
                                                                                             class="vc_box_rounded  ag_image"
                                                                                             src="{{ $art->cover }}"
                                                                                             alt="products-1"
                                                                                             description=""
                                                                                             data-attach-id="15433"
                                                                                             data-title="products-1"
                                                                                             title=""
                                                                                             src-img="">
                                                                                        <div class="item_mask"></div>
                                                                                    </div>
                                                                                </a>
                                                                                <div class="item_wrapper clearfix">
                                                                                    <div class="item_info clearfix">
                                                                                        <div class="text_wrap">
                                                                                            <a class="item_a_link"
                                                                                               href="/product/{{ $art->id }}"
                                                                                               title="{{ $art->title }}"
                                                                                               target="_self">
                                                                                                <div class="title ellipsis"
                                                                                                     data-title="true"
                                                                                                     style="color:#382924;font-size:18px;font-weight:700;padding-top:15px;padding-bottom:5px;padding-left:10px;padding-right:10px;">
                                                                                                   {{ $art->title }}
                                                                                                </div>
                                                                                            </a>
                                                                                            <div class="subtitle ellipsis hide"
                                                                                                 style="color:#999999;font-size:12px;display:none;"></div>
                                                                                            <div class="date_wrap hide"
                                                                                                 style="">
                                                                                                                <span class="year"
                                                                                                                      style="color:#c4c4c4;font-size:16px;font-family:Raleway;;">2017</span>
                                                                                                <span class="md post_date"
                                                                                                      style="color:#c4c4c4;font-size:15px;font-family:Raleway;;">06-01</span>
                                                                                            </div>


                                                                                            <div class="item_des show"
                                                                                                 style="max-width:px;">
                                                                                                <div class="description post_excerpt"
                                                                                                     style="color:#333333;font-size:12px;max-width:px;word-break:break-all;">
                                                                                                    <p>{{ $art->description }}</p>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="price_warp ellipsis hide">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <a href="/product/{{ $art->id }}" title="{{ $art->title }}" target="_self" class="details item_link" style="color:#cccccc;font-size:16px;"><i class="fa fa-angle-right" aria-hidden="true"></i></a>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @if($loop->iteration%4==0)
                                                                    <p class="wf-mobile-hidden"
                                                                       style="clear:both;padding:0;"></p>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wf-mobile-hidden qfy-clumn-clear"
                         style="clear:both;"></div>
                </div>
            </div>
        </section>
    @endforeach
@endsection