<?php

namespace App\Http\Controllers\Home;

use App\Http\Traits\TraitFront;
use App\Models\Article;
use App\Models\Category;
use App\Models\Chat;
use App\Models\FriendLink;
use App\Models\Message;
use App\Models\SystemConfig;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Vijay\Curl\Curl;
use App\Models\Nav;

class IndexController extends Controller
{
    use TraitFront;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $navList = Nav::getMenuTree(Nav::orderBy('sort', 'asc')->get()->toArray());
        $configs = SystemConfig::where('status', 1)->pluck('value', 'key');
        View::share([
            'nav_list' => $navList,
            'configs' => $configs
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/8/4
     * Time: 16:30
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $categories = Category::where('pid', 3)->select('name', 'id')->get();
        $articles = Article::where([
            ['status', '=', 1],
            ['category_id', '=', 2]
        ])
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();
        return view('home.index.index', [
            'articles' => $articles,
            'categories' => $categories,
            'title' => '网站首页',
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/9/8
     * Time: 22:55
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function news()
    {
        $where = [
            ['status', '=', '1'],
            ['category_id', '=', 2]
        ];
        $articles = Article::where($where)
            ->orderBy('updated_at', 'desc')
            ->paginate(40);
        return view('home.index.news', [
            'title' => '新闻资讯',
            'articles' => $articles
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/8/3
     * Time: 14:29
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function article($id)
    {
        $info = Article::find($id);
        if (empty($info) || $info->status == 0) {
            return redirect('/');
        }
        $info->click += 1;
        $info->save();

        $infoTags = [];
        if (!empty($info->keywords)) {
            $infoTags = Tag::whereIn('name', explode(',', $info->keywords))
                ->select('id', 'name')
                ->get()->toArray();
        }
        $artList = Article::where('category_id', 2)
            ->select('id', 'title', 'created_at')
            ->limit(9)
            ->get();
        $pre = Article::where('category_id', 2)
            ->select('id', 'title')
            ->find($id - 1);
        $next = Article::where('category_id', 2)
            ->select('id', 'title')
            ->find($id + 1);
        return view('home.index.article', [
            'info' => $info,
            'info_tags' => $infoTags,
            'pre' => $pre,
            'next' => $next,
            'artList' => $artList
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/9/8
     * Time: 22:52
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function product($id)
    {
        $info = Article::find($id);
        if (empty($info) || $info->status == 0) {
            return redirect('/');
        }
        $info->click += 1;
        $info->save();
        $infoTags = [];
        if (!empty($info->keywords)) {
            $infoTags = Tag::whereIn('name', explode(',', $info->keywords))
                ->select('id', 'name')
                ->get()->toArray();
        }
        return view('home.index.product', [
            'info' => $info,
            'info_tags' => $infoTags,
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/9/8
     * Time: 22:52
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        $info = Article::find(1);
        return view('home.index.about', [
            'info' => $info,
            'title' => '关于我们',
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/9/8
     * Time: 22:52
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        $info = Article::find(2);
        return view('home.index.contact', [
            'title' => '联系我们',
            'info' => $info,
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/8/11
     * Time: 15:41
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category(Category $category)
    {
        $categoryList = Category::where('pid', $category->id)->select('name', 'id')->get();
        $ids = $categoryList->pluck('id');
        if (!$categoryList->count()) {
            $categoryList = [$category->toArray()];
            $ids = $category->id;
        }
        $query = Article::query();
        $query->where('status', 1);
        $query->whereIn('category_id', $ids);
        $articles = $query->orderBy('id', 'asc')
            ->get();
        $categoryArt = [];
        foreach ($articles as $art) {
            $categoryArt[$art->category_id][] = $art;
        }
        return view('home.index.category', [
            'info' => $category,
            'categoryList' => $categoryList,
            'categoryArt' => $categoryArt,
            'title' => '产品中心',
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/8/11
     * Time: 17:53
     * @param Tag $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tag(Tag $tag)
    {
        $where = [
            ['status', '=', '1'],
            ['keywords', 'like', '%' . $tag->name . '%']
        ];
        $articles = Article::where($where)
            ->orderBy('updated_at', 'desc')
            ->paginate(9);
        $count = count($articles);
        $data = [];
        $j = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($i % 3 == 0) {
                $j++;
            }
            $data[$j][] = $articles[$i]->toArray();
        }
        return view('home.index.tag', [
            'info' => $tag,
            'articles' => $articles,
            'data' => $data
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/8/11
     * Time: 17:53
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function chat()
    {
        $chats = Chat::orderBy('id', 'desc')
            ->paginate(30);
        return view('home.index.chat', [
            'chats' => $chats
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/7/31
     * Time: 13:51
     * @param Request $request
     * @return array|bool|mixed
     */
    public function history(Request $request)
    {
        $curl = new Curl();
        $url = 'http://www.jiahengfei.cn:33550/port/history?dispose=detail&key=jiahengfei&month=' . date('m') . '&day=' . date('d');
        $res = $curl->get($url);
        return $res;
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/8/3
     * Time: 14:04
     * @param Request $request
     * @return mixed
     */
    public function clickArticle(Request $request)
    {
        $clickArticle = Article::where([
            ['status', '=', '1']
        ])->select('id', 'title', 'cover', 'description')
            ->orderBy('click', 'desc')
            ->limit(6)
            ->get();
        return $this->resJson('0', '获取成功', $clickArticle);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/8/3
     * Time: 14:21
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function friendLinks(Request $request)
    {
        $links = FriendLink::select('name', 'url')->get();
        return $this->resJson('0', '获取成功', $links);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/8/3
     * Time: 14:26
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function ajaxTags(Request $request)
    {
        $tags = Tag::select('id', 'name')->get();
        return $this->resJson('0', '获取成功', $tags);
    }

    /**
     * Description:搜索
     * User: Vijay
     * Date: 2019/9/13
     * Time: 14:46
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $keyword = $request->input('search', '');
        $articles = Article::where([
            ['title', 'like', '%' . $keyword . '%'],
            ['status', '=', '1']
        ])->get();

        return view('home.index.search', [
            'title' => '搜索结果',
            'articles' => $articles
        ]);
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2019/9/13
     * Time: 15:15
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function message(Request $request)
    {
        if ($request->isMethod('post')) {
            $model = new Message();
            try {
                $model::create($request->input());
                return $this->resJson(0, '操作成功');
            } catch (\Exception $e) {
                return $this->resJson(1, $e->getMessage());
            }
        } else {
            return $this->resJson(1, '提交方式不正确');
        }
    }
}
